#! /usr/bin/sh

# scan URLs
cat BBJ.md | grep -E '\[.+\]\(.+\)' | awk -F '[)(]' '{print $2}' | sort | uniq -d

# scan names
cat BBJ.md | grep -E '\[.+\]\(.+\)' | awk -F '[][]' '{print $2}' | sort | uniq -d