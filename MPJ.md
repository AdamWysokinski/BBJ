# MATLAB–Python–Julia cheatsheet

## Creating Vectors

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Row vector: size (1, n) | `A = [1 2 3]` | `A = np.array([1, 2, 3]).reshape(1, 3)` | `A = [1 2 3]` |
| Column vector: size (n, 1) | `A = [1; 2; 3]` | `A = np.array([1, 2, 3]).reshape(3, 1)` | `A = [1 2 3]'` |
| 1d array: size (n, ) | Not possible | `A = np.array([1, 2, 3])` | <pre><code>A = [1; 2; 3]</code><br># or<br><code>A = [1, 2, 3]</code></pre> |
| Integers from j to n with step size k | `A = j:k:n` | `A = np.arange(j, n+1, k)` | `A = j:k:n` |
| Linearly spaced vector of k points | `A = linspace(1, 5, k)` | `A = np.linspace(1, 5, k)` | `A = range(1, 5, length = k)` |

## Creating matrices

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Create a matrix | `A = [1 2; 3 4]` | `A = np.array([[1, 2], [3, 4]])` | `A = [1 2; 3 4]` |
| 2 x 2 matrix of zeros | `A = zeros(2, 2)` | `A = np.zeros((2, 2))` | `A = zeros(2, 2)` |
| 2 x 2 matrix of ones | `A = ones(2, 2)` | `A = np.ones((2, 2))` | `A = ones(2, 2)` |
| 2 x 2 identity matrix | `A = eye(2, 2)` | `A = np.eye(2)` | `A = I(2)` |
| Diagonal matrix | `A = diag([1 2 3])` | `A = np.diag([1, 2, 3])` | `A = Diagonal([1, 2, 3])` |
| Uniform random numbers | `A = rand(2, 2)` | `A = np.random.rand(2, 2)` | `A = rand(2, 2)` |
| Normal random numbers | `A = randn(2, 2)` | `A = np.random.randn(2, 2)` | `A = randn(2, 2)` |
| Sparse Matrices | <pre><code>A = sparse(2, 2)</code><br><code>A(1, 2) = 4</code><br><code>A(2, 2) = 1</code></pre> | <pre><code>from scipy.sparse import coo_matrix</code><br><code>A = coo_matrix(([4, 1], ([0, 1], [1, 1])), shape=(2, 2))</code></pre> | <pre><code>using SparseArrays</code><br><code>A = spzeros(2, 2)</code><br><code>A[1, 2] = 4</code><br><code>A[2, 2] = 1</code></pre> |
| Tridiagonal Matrices | <pre><code>A = [1 2 3 4; 1 2 3 4; 1 2 3 4]</code><br><code>spdiags(A',[-1 0 1], 4, 4)</code></pre> | <pre><code>import sp.sparse as sp</code><br><code>diagonals = [[4, 5, 6, 7], [1, 2, 3], [8, 9, 10]]</code><br><code>sp.diags(diagonals, [0, -1, 2]).toarray()</code></pre> | <pre><code>x = [1, 2, 3]</code><br><code>y = [4, 5, 6, 7]</code><br><code>z = [8, 9, 10]</code><br><code>Tridiagonal(x, y, z)</code></pre> |

## Manipulating Vectors and Matrices

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Transpose | `A.'` | `A.T` | `transpose(A)` |
| Complex conjugate transpose<br>(Adjoint) | `A'` | `A.conj()` | `A'` |
| Concatenate horizontally | <pre><code>A = [[1 2] [1 2]]</code><br># or<br><code>A = horzcat([1 2], [1 2])</code></pre> | <pre><code>B = np.array([1, 2])</code><br><code>A = np.hstack((B, B))</code></pre> | <pre><code>A = [[1 2] [1 2]]</code><br># or<br><code>A = hcat([1 2], [1 2])</code></pre> |
| Concatenate vertically | <pre><code>A = [[1 2] [1 2]]</code><br># or<br><code>A = vertcat([1 2], [1 2])</code></pre> | <pre><code>B = np.array([1, 2])</code><br><code>A = np.vstack((B, B))</code></pre> | <pre><code>A = [[1 2]; [1 2]]</code><br># or<br><code>A = vcat([1 2], [1 2])</code></pre> |
| Reshape (to 5 rows, 2 columns) | `A = reshape(1:10, 5, 2)` | `A = A.reshape(5, 2)` | `A = reshape(1:10, 5, 2)` |
| Convert matrix to vector | `A(:)` | `A = A.flatten()` | `A[:]` |
| Flip left/right | `fliplr(A)` | `np.fliplr(A)` | `reverse(A, dims = 2)` |
| Flip up/down | `flipud(A)` | `np.flipud(A)` | `reverse(A, dims = 1)` |
| Repeat matrix<br>(3 times in the row dimension,<br>4 times in the column dimension) | `repmat(A, 3, 4)` | `np.tile(A, (4, 3))` | `repeat(A, 3, 4)` |
| Preallocating/Similar | <pre><code>x = rand(10)</code><br><code>y = zeros(size(x, 1), size(x, 2))</code><br><code>N/A similar type</code></pre> | <pre><code>x = np.random.rand(3, 3)</code><br><code>y = np.empty_like(x)</code><br># new dims<br><code>y = np.empty((2, 3))</code></pre> | <pre><code>x = rand(3, 3)</code><br><code>y = similar(x)</code><br># new dims<br><code>y = similar(x, 2, 2)</code></pre> |
| Broadcast a function over a collection/matrix/vector | <pre><code>f = @(x) x.^2</code><br><code>g = @(x, y) x + 2 + y.^2</code><br><code>x = 1:10</code><br><code>y = 2:11</code><br><code>f(x)</code><br><code>g(x, y)</code><br><code>Functions broadcast directly</code></pre> | <pre><code>def f(x):</code><br><code>    return x\*\*2</code><br><code>def g(x, y):</code><br><code>    return x + 2 + y\*\*2</code><br><code>x = np.arange(1, 10, 1)</code><br><code>y = np.arange(2, 11, 1)</code><br><code>f(x)</code><br><code>g(x, y)</code><br><code>Functions broadcast directly</code></pre> | <pre><code>f(x) = x^2</code><br><code>g(x, y) = x + 2 + y^2</code><br><code>x = 1:10</code><br><code>y = 2:11</code><br><code>f.(x)</code><br><code>g.(x, y)</code></pre> |

## Accessing Vector/Matrix Elements

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Access one element | `A(2, 2)` | `A[1, 1]` | `A[2, 2]` |
| Access specific rows | `A(1:4, :)` | `A[0:4, :]` | `A[1:4, :]` |
| Access specific columns | `A(:, 1:4)` | `A[:, 0:4]` | `A[:, 1:4]` |
| Remove a row | `A([1 2 4], :)` | `A[[0, 1, 3], :]` | `A[[1, 2, 4], :]` |
| Diagonals of matrix | `diag(A)` | `np.diag(A)` | `diag(A)` |
| Get dimensions of matrix | `[nrow ncol] = size(A)` | `nrow, ncol = np.shape(A)` | `nrow, ncol = size(A)` |

## Mathematical Operations

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Dot product | `dot(A, B)` | <pre><code>np.dot(A, B)</code><br># or<br><code>A @ B`</code></pre> | `A ⋅ B # \cdot<TAB>` |
| Matrix multiplication | `A * B` | `A @ B` | `A * B` |
| Inplace matrix multiplication | Not possible | <pre><code>x = np.array([1, 2]).reshape(2, 1)</code><br><code>A = np.array(([1, 2], [3, 4]))</code><br><code>y = np.empty_like(x)</code><br><code>np.matmul(A, x, y)</code></pre> | <pre><code>x = [1, 2]</code><br><code>A = [1 2; 3 4]</code><br><code>y = similar(x)</code><br><code>mul!(y, A, x)</code></pre> |
| Element-wise multiplication | `A .* B` | `A * B` | `A .* B` |
| Matrix to a power | `A^2` | `np.linalg.matrix_power(A, 2)` | `A^2` |
| Matrix to a power, elementwise | `A.^2` | `A**2` | `A.^2` |
| Inverse | <pre><code>inv(A)</code><br># or<br><code>A^(-1)</code></pre> | `np.linalg.inv(A)` | <pre><code>inv(A)</code><br># or<br><code>A^(-1)</code></pre> |
| Determinant | `det(A)` | `np.linalg.det(A)` | `det(A)` |
| Eigenvalues and eigenvectors | `[vec, val] = eig(A)` | `val, vec = np.linalg.eig(A)` | `val, vec = eigen(A)` |
| Euclidean norm | `norm(A)` | `np.linalg.norm(A)` | `norm(A)` |
| Solve linear system `Ax=b`<br>when A is square | `A\b` | `np.linalg.solve(A, b)` | `A\b` |
| Solve least squares problem `Ax=b`<br>when A is rectangular | `A\b` | `np.linalg.lstsq(A, b)` | `A\b` |

## Sum / max / min

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Sum / max / min of each column | <pre><code>sum(A, 1)</code><br><code>max(A, [], 1)</code><br><code>min(A, [], 1)</code></pre> | <pre><code>sum(A, 0)</code><br><code>np.amax(A, 0)</code><br><code>np.amin(A, 0)</code></pre> | <pre><code>sum(A, dims = 1)</code><br><code>maximum(A, dims = 1)</code><br><code>minimum(A, dims = 1)</code></pre> |
| Sum / max / min of each row | <pre><code>sum(A, 2)</code><br><code>max(A, [], 2)</code><br><code>min(A, [], 2)</code></pre> | <pre><code>sum(A, 1)</code><br><code>np.amax(A, 1)</code><br><code>np.amin(A, 1)</code></pre> | <pre><code>sum(A, dims = 2)</code><br><code>maximum(A, dims = 2)</code><br><code>minimum(A, dims = 2)</code></pre> |
| Sum / max / min of entire matrix | <pre><code>sum(A(:))</code><br><code>max(A(:))</code><br><code>min(A(:))</code></pre> | <pre><code>np.sum(A)</code><br><code>np.amax(A)</code><br><code>np.amin(A)</code></pre> | <pre><code>sum(A)</code><br><code>maximum(A)</code><br><code>minimum(A)</code></pre> |
| Cumulative sum / max / min by row | <pre><code>cumsum(A, 1)</code><br><code>cummax(A, 1)</code><br><code>cummin(A, 1)</code></pre> | <pre><code>np.cumsum(A, 0)</code><br><code>np.maximum.accumulate(A, 0)</code><br><code>np.minimum.accumulate(A, 0)</code></pre> | <pre><code>cumsum(A, dims = 1)</code><br><code>accumulate(max, A, dims = 1)</code><br><code>accumulate(min, A, dims = 1)</code></pre> |
| Cumulative sum / max / min by column | <pre><code>cumsum(A, 2)</code><br><code>cummax(A, 2)</code><br><code>cummin(A, 2)</code></pre> | <pre><code>np.cumsum(A, 1)</code><br><code>np.maximum.accumulate(A, 1)</code><br><code>np.minimum.accumulate(A, 1)</code></pre> | <pre><code>cumsum(A, dims = 2)</code><br><code>accumulate(max, A, dims = 2)</code><br><code>accumulate(min, A, dims = 2)</code></pre> |

## Programming

|              | MATLAB | Python | Julia |
|--------------|--------|--------|-------|
| Comment one line | `% This is a comment` | `# This is a comment` | `# This is a comment` |
| Comment block | <pre><code>%{</code><br><code>Comment block</code><br><code>%}</code></pre> | <pre><code># Block</code><br><code># comment</code><br><code># following PEP8</code></pre> | <pre><code>`#=`</code><br><code>Comment block</code><br><code>`=#`</code></pre> |
| For loop | <pre><code>for i = 1:N</code><br><code>   % do something</code><br><code>end</code></pre> | <pre><code>for i in range(n):</code><br><code>    # do something</code></pre> | <pre><code>for i in 1:N</code><br><code>   # do something</code><br><code>end</code></pre> |
| While loop | <pre><code>while i <= N</code><br><code>   % do something</code><br><code>end</code></pre> | <pre><code>while i <= N:</code><br><code>    # do something</code></pre> | <pre><code>while i <= N</code><br><code>   # do something</code><br><code>end</code></pre> |
| If | <pre><code>if i <= N</code><br><code>   % do something</code><br><code>end</code></pre> | <pre><code>if i <= N:</code><br><code>   # do something</code></pre> | <pre><code>if i <= N</code><br><code>   # do something</code><br><code>end</code></pre> |
| If / else | <pre><code>if i <= N</code><br><code>   % do something</code><br><code>else</code><br><code>   % do something else</code><br><code>end</code></pre> | <pre><code>if i <= N:</code><br><code>    # do something</code><br><code>else:</code><br><code>    # so something else</code></pre> | <pre><code>if i <= N</code><br><code>   # do something</code><br><code>else</code><br><code>   # do something else</code><br><code>end</code></pre> |
| Print text and variable | <pre><code>x = 10</code><br><code>fprintf('x = %d \n', x)</code></pre> | <pre><code>x = 10</code><br><code>print(f'x = {x}')</code></pre> | <pre><code>x = 10</code><br><code>println("x = $x")</code></pre> |
| Function: anonymous | `f = @(x) x^2` | `f = lambda x: x**2` | <pre><code>f = x -> x^2</code><br><code># can be rebound</code></pre> |
| Function | <pre><code>function out  = f(x)</code><br><code>   out = x^2</code><br><code>end</code></pre> | <pre><code>def f(x):</code><br><code>    return x\*\*2</code><br><br><code>x = np.random.rand(10)</code><br><code>f(x)</code></pre> | <pre><code>function f(x)</code><br><code>   return x^2</code><br><code>end</code><br><br><code>f(x) = x^2 # not anon!</code></pre> |
| Tuples | <pre><code>t = {1 2.0 "test"}</code><br><code>t{1}</code><br><code># can use cells but watch performance</code></pre> | <pre><code>t = (1, 2.0, "test")</code><br><code>t[0]</code></pre> | <pre><code>t = (1, 2.0, "test")</code><br><code>t[1]</code></pre> |
| Named Tuples/ Anonymous Structures | <pre><code>m.x = 1</code><br><code>m.y = 2</code><br><br><code>m.x</code></pre> | <pre><code>from collections import namedtuple</code><br><br><code>mdef = namedtuple('m', 'x y')</code><br><code>m = mdef(1, 2)</code><br><br><code>m.x</code></pre> | <pre># vanilla<br><code>m = (x = 1, y = 2)</code><br><code>m.x</code><br># constructor<br><code>using Parameters</code><br><code>mdef = @with_kw (x=1, y=2)</code><br><code>m = mdef() # same as above</code><br><code>m = mdef(x = 3)</code></pre> |
| Closures | <pre><code>a = 2.0</code><br><code>f = @(x) a + x</code><br><code>f(1.0)</code></pre> | <pre><code>a = 2.0</code><br><code>def f(x):</code><br><code>    return a + x</code><br><br><code>f(1.0)</code></pre> | <pre><code>a = 2.0</code><br><code>f(x) = a + x</code><br><code>f(1.0)</code></pre> |
| Inplace Modification | No consistent or simple syntax to achieve this | <pre><code>def f(x):</code><br><code>    x **= 2</code><br><code>    return</code><br><br><code>x = np.random.rand(10)</code><br><code>f(x)</code></pre> | <pre><code>function f!(out, x)</code><br><code>    out .= x.^2</code><br><code>end</code><br><code>x = rand(10)</code><br><code>y = similar(x)</code><br><code>f!(y, x)</code></pre> |
