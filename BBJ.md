## Language documentation

[Julia Documentation](https://docs.julialang.org/en/v1/)

This is the official Julia documentation.

---

## Videos

[The Julia Programming Language](https://www.youtube.com/@TheJuliaLanguage)

Alan Edelman, Viral B. Shah [Julia Lightning Round](https://www.youtube.com/watch?v=37L1OMk_3FU)

Keno Fischer [Networking in Julia](https://www.youtube.com/watch?v=qYjHYTn7r2w)

Jeff Bezanson [Parallel and Distributed Computing with Julia](https://www.youtube.com/watch?v=JoRn4ryMclc)

Stefan Karpinski [Metaprogramming and Macros in Julia](https://www.youtube.com/watch?v=EpNeNCGmyZE)

Miles Lubin, Iain Dunning [Numerical Optimization in Julia](https://www.youtube.com/watch?v=O1icUP6sajU)

Steve Johnson [Fast Fourier Transforms in Julia](https://www.youtube.com/watch?v=1iBLaHGL1AM)

Doug Bates [Statistical Models in Julia](https://www.youtube.com/watch?v=v9Io-p_iymI)

John Myles White [Data Analysis in Julia with Data Frames](https://www.youtube.com/watch?v=XRClA5YLiIc)

Jeff Bezanson, Stefan Karpinski [Rationale Behind Julia and the Vision](https://www.youtube.com/watch?v=02U9AJMEWx0)

[Julia for Talented Amateurs](https://www.youtube.com/c/juliafortalentedamateurs/videos)

Alan Edelman [Matrix Methods in Data Analysis, Signal Processing, and Machine Learning](https://www.youtube.com/watch?v=rZS2LGiurKY)

Alan Edelman [A programming language to heal the planet together: Julia](https://www.youtube.com/watch?v=qGW0GT1rCvs)

[Intro to Julia tutorial (version 1.0)](https://www.youtube.com/watch?v=8h8rQyEpiZA)

[Daniel Malafaia](https://www.youtube.com/@d.malafaia)

[Julia in 100 Seconds](https://www.youtube.com/watch?v=JYs_94znYy0)

[Julia Tutorial](https://www.youtube.com/watch?v=sE67bP2PnOo)

[Talk Julia](https://www.youtube.com/@TalkJulia)

[JuliaHub](https://www.youtube.com/@JuliaHubInc)

[Julia Journey: Juggling Jargons Joyfully](https://www.youtube.com/playlist?list=PLOOY0eChA1uwYXUGnF0FZBuiYQjgQffPu)

---

## Julia Academy Courses

[JuliaAcademy](https://juliaacademy.com/courses)

---

## Blogs / Forums

[Julia Computing](https://juliacomputing.com/blog/)

[Julia Forem](https://forem.julialang.org/)

[Discourse](https://discourse.julialang.org/)

[Zulip](https://julialang.zulipchat.com)

[Discord](https://humansofjulia.org/)

[Slack](https://julialang.slack.com)

[Gitter](https://gitter.im/JuliaLang/julia)

[Solver Max](https://www.solvermax.com/component/tags/tag/julia)

---

## Packages

[JuliaHub](https://juliahub.com/ui/Packages)

Searchable list of all registered Julia packages.

[Pkg.jl](https://pkgdocs.julialang.org/v1/)

Documentation for Pkg, Julia's package manager.

[PkgDev.jl](https://github.com/JuliaLang/PkgDev.jl)

PkgDev provides tools for Julia package developers.

[PkgServer.jl](https://github.com/JuliaPackaging/PkgServer.jl)

Reference implementation of a Julia Pkg server, providing advanced package serving and caching capabilities.

[PkgTemplates.jl](https://github.com/JuliaCI/PkgTemplates.jl)

PkgTemplates creates new Julia packages in an easy, repeatable, and customizable way.

[PkgEval.jl](https://github.com/JuliaCI/PkgEval.jl)

PkgEval.jl is a package to test one or more Julia versions against the Julia package ecosystem, and is used by Nanosoldier.jl for keeping track of package compatibility of upcoming Julia versions.

[Nanosoldier.jl](https://github.com/JuliaCI/Nanosoldier.jl)

This package contains the infrastructure powering the @nanosoldier CI bot used by the Julia language.

[BinaryProvider.jl](https://github.com/JuliaPackaging/BinaryProvider.jl)

A reliable binary provider for Julia.

[Scratch.jl](https://github.com/JuliaPackaging/Scratch.jl)

This repository implements the scratch spaces API for package-specific mutable containers of data. 

[RelocatableFolders.jl](https://github.com/JuliaPackaging/RelocatableFolders.jl)

An alternative to the `@__DIR__` macro. Packages that wish to reference paths in their project directory run into issues with relocatability when used in conjunction with PackageCompiler. The `@path` macro provided by this package overcomes this limitation.

[WinRPM.jl](https://github.com/JuliaPackaging/WinRPM.jl)

WinRPM is an installer for RPM packages provided by an RPM-md build system.

[LazyArtifacts.jl](https://github.com/JuliaPackaging/LazyArtifacts.jl)

This is a wrapper package meant to bridge the gap for packages that want to use the LazyArtifacts stdlib as a dependency within packages that still support Julia versions older than 1.6.

[Artifacts.jl](https://github.com/JuliaPackaging/Artifacts.jl)

This is a wrapper package meant to bridge the gap for packages that want to use the Artifacts as a dependency within packages that still support Julia versions older than 1.6.

[CMake.jl](https://github.com/JuliaPackaging/CMake.jl)

This package is designed to make it easier for Julia packages to build binary dependencies that use CMake. 

[CMakeWrapper.jl](https://github.com/JuliaPackaging/CMakeWrapper.jl)

This package provides a `BinDeps.jl`-compatible CMakeProcess class for automatically building CMake dependencies.

[BinDeps.jl](https://github.com/JuliaPackaging/BinDeps.jl)

Easily build binary dependencies for Julia packages.

[MetadataTools.jl](https://github.com/JuliaPackaging/MetadataTools.jl)

Functionality to analyze the structure of Julia's METADATA repository.

[CondaBinDeps.jl](https://github.com/JuliaPackaging/CondaBinDeps.jl)

This package, which builds on the Conda.jl package allows one to use conda as a BinDeps binary provider for Julia. CondaBinDeps.jl is a cross-platform alternative. It can also be used without administrator rights, in contrast to the current Linux-based providers.

[CMakeBuilder](https://github.com/JuliaPackaging/CMakeBuilder)

This repository builds compiled binaries of cmake for the Julia packages that require this program to build.

[VersionCheck.jl](https://github.com/GenieFramework/VersionCheck.jl)

Utility package for checking if a new version of a Julia package is available. It uses the current `Project.toml` file and a special `CHANGELOG.html` file to determine the latest versions.

[autodocs.jl](https://github.com/Clemapfel/autodocs.jl)

Provides the `@autodocs` macro, which, if invoked at the start of a module declaration, will generate basic documentation for all declared Julia objects (functions, structs, macros, etc.) in that module. If an object already has documentation, it will be expanded (but not overwritten).

[PkgImages.jl](https://github.com/timholy/PkgImages.jl)

This "package" is a prototype for various operations needed for a next-generation package cache in Julia, targeted to be merged in the Julia 1.9 and/or 1.10 timeframe.

[PkgCacheInspector.jl](https://github.com/timholy/PkgCacheInspector.jl)

This package provides insight about what's stored in Julia's package precompile files. This works only on Julia 1.9 and above, as it targets the new pkgimage format.

---

### Julia programming

[BenchmarkTools.jl](https://github.com/JuliaCI/BenchmarkTools.jl)

BenchmarkTools makes performance tracking of Julia code easy by supplying a framework for writing and running groups of benchmarks as well as comparing benchmark results.

[BaseBenchmarks.jl](https://github.com/JuliaCI/BaseBenchmarks.jl)

This package is a collection of Julia benchmarks used to track the performance of the Julia language.

[PkgBenchmark.jl](https://github.com/JuliaCI/PkgBenchmark.jl)

PkgBenchmark provides an interface for Julia package developers to track performance changes of their packages.

[Coverage.jl](https://github.com/JuliaCI/Coverage.jl)

Take Julia code coverage and memory allocation results, do useful things with them.

[CoverageTools.jl](https://github.com/JuliaCI/CoverageTools.jl)

CoverageTools.jl provides the core functionality for processing code coverage and memory allocation results.

[CoverageBase.jl](https://github.com/JuliaCI/CoverageBase.jl)

A package for measuring the internal test coverage of the Julia programming language.

[LocalCoverage.jl](https://github.com/JuliaCI/LocalCoverage.jl)

This is a collection of trivial functions to facilitate generating and exploring test coverage information for Julia packages locally, without using any remote/cloud services.

[TravisTest.jl](https://github.com/JuliaCI/TravisTest.jl)

Repository for testing Julia support at the Travis Continuous Integration (CI) service.

[Appveyor.jl](https://github.com/JuliaCI/Appveyor.jl)

This contains a "universal" Appveyor script for Julia repositories, making it easier to set up matrix builds and keep URLs up-to-date.

[CILogAnalysis.jl](https://github.com/JuliaCI/CILogAnalysis.jl)

Log analysis pipeline for CI errors.

[DepotCompactor.jl](https://github.com/JuliaCI/DepotCompactor.jl)

This package is intended to make managing a shared depot used by multiple workers easier. It was designed for running Yggdrasil, where many large artifacts are downloaded by individual agents on a single machine, each agent writes to its own depot, but many artifacts are duplicated across each agent depot. This package allows for transparent "compaction" of depots by inspecting resoures contained within depots and moving shared resources into a shared, read-only depot stored higher on the `DEPOT_PATH` by each agent.

[BuildkiteUtils.jl](https://github.com/JuliaCI/BuildkiteUtils.jl)

This is a collection of utility functions when running Julia jobs on Buildkite.

[Preferences.jl](https://github.com/JuliaPackaging/Preferences.jl)

The Preferences package provides a convenient, integrated way for packages to store configuration switches to persistent TOML files, and use those pieces of information at both run time and compile time in Julia v1.6+.

[Requires.jl](https://github.com/JuliaPackaging/Requires.jl)

Requires is a Julia package that will magically make loading packages faster, maybe.

[Archspec.jl](https://juliapackaging.github.io/Archspec.jl/dev/)

Archspec is a Julia package to get access to the information provided by `archspec-json`, part of the Archspec project.

[OutputCollectors.jl](https://github.com/JuliaPackaging/OutputCollectors.jl)

This package lets you capture subprocess `stdout` and `stderr` streams independently, resynthesizing and colorizing the streams appropriately.

[FileIO.jl](https://juliaio.github.io/FileIO.jl/stable/)

FileIO aims to provide a common framework for detecting file formats and dispatching to appropriate readers/writers.

[PackageCompiler.jl](https://github.com/JuliaLang/PackageCompiler.jl)

Julia is, in general, a "just-barely-ahead-of-time" compiled language. When you call a function for the first time, Julia compiles it for precisely the types of the arguments given. This can take some time. All subsequent calls within that same session use this fast compiled function, but if you restart Julia you lose all the compiled work. PackageCompiler allows you to do this work upfront — further ahead of time — and store the results for a lower latency startup.

[PrecompileTools.jl](https://github.com/JuliaLang/PrecompileTools.jl)

PrecompileTools allows you to reduce the latency of the first execution of Julia code. It is applicable for package developers and for "ordinary users" in their personal workflows.

[Pluto.jl](https://github.com/fonsp/Pluto.jl)

Writing a notebook is not just about writing the final document — Pluto empowers the experiments and discoveries that are essential to getting there.

[PlutoUI.jl](https://github.com/JuliaPluto/PlutoUI.jl)

A tiny package to make html `"<input>"` a bit more Julian. Use it with the `@bind` macro in Pluto.

[IJulia.jl](https://julialang.github.io/IJulia.jl/stable/)

IJulia is a Julia-language backend combined with the Jupyter interactive environment (also used by IPython).

[Revise.jl](https://github.com/timholy/Revise.jl)

Revise.jl allows you to modify code and use the changes without restarting Julia. With Revise, you can be in the middle of a session and then update packages, switch git branches, and/or edit the source code in the editor of your choice; any changes will typically be incorporated into the very next command you issue from the REPL. This can save you the overhead of restarting Julia, loading packages, and waiting for code to JIT-compile.

[Bonito.jl](https://github.com/SimonDanisch/Bonito.jl)

Easy way of building interactive applications from Julia. Uses Hyperscript to create HTML descriptions, and allows to execute Javascript & building of widgets. It also supports an offline mode, that exports your interactive app to a folder, and optionally records a statemap for all UI elements, so that a running Julia process isn't necessary anymore.

[Documenter.jl](https://juliadocs.github.io/Documenter.jl/stable/)

A documentation generator for Julia. A package for building documentation from docstrings and markdown files.

[DocumenterTools.jl](https://github.com/JuliaDocs/DocumenterTools.jl)

This package contains utilities for setting up documentation generation with Documenter.jl.

[DocumenterMarkdown.jl](https://github.com/JuliaDocs/DocumenterMarkdown.jl)

This package enables the Markdown / MkDocs backend of Documenter.jl.

[DocumenterMermaid.jl](https://github.com/JuliaDocs/DocumenterMermaid.jl)

A Documenter plugin package that enables native support for mermaid.js diagrams in Documenter documents (in HTML builds only).

[DocumenterVitepress.jl](https://github.com/LuxDL/DocumenterVitepress.jl)

This package provides a Markdown / MkDocs backend to Documenter.jl.

[DocServer.jl](https://github.com/JuliaGizmos/DocServer.jl)

Serve docs generated by Documenter.

[DocStringExtensions.jl](https://github.com/JuliaDocs/DocStringExtensions.jl)

Extensions for Julia's docsystem.

[DocumentationGenerator.jl](https://github.com/JuliaDocs/DocumentationGenerator.jl)

Generate documentation for all the packages.

[IOCapture.jl](https://github.com/JuliaDocs/IOCapture.jl)

Provides the `IOCapture.capture(f)` function which evaluates the function `f`, captures the standard output and standard error, and returns it as a string, together with the return value.

[Highlights.jl](https://github.com/JuliaDocs/Highlights.jl)

A source code highlighter for Julia.

[ANSIColoredPrinters.jl](https://github.com/JuliaDocs/ANSIColoredPrinters.jl)

ANSIColoredPrinters convert a text qualified by ANSI escape codes to another format.

[Spark.jl](https://dfdx.github.io/Spark.jl/dev/)

Spark.jl provides an interface to Apache Spark™ platform, including SQL / DataFrame and Structured Streaming. It closely follows the PySpark API, making it easy to translate existing Python code to Julia.

[ThreadsX.jl](https://tkf.github.io/ThreadsX.jl/dev/)

Parallelized Base functions.

[Git.jl](https://github.com/JuliaVersionControl/Git.jl)

Git.jl allows you to use command-line Git in your Julia packages. You do not need to have Git installed on your computer, and neither do the users of your packages!

[GitREPL.jl](https://github.com/JuliaVersionControl/GitREPL.jl)

Git REPL mode for Julia.

[JET.jl](https://aviatesk.github.io/JET.jl/stable/)

An experimental code analyzer for Julia, no need for additional type annotations. JET.jl employs Julia's type inference to detect potential bugs.

[Registrator.jl](https://github.com/JuliaComputing/Registrator.jl)

Registrator is a GitHub app that automates creation of registration pull requests for your julia packages to the General registry.

[LocalRegistry.jl](https://github.com/GunnarFarneback/LocalRegistry.jl)

Create and maintain local registries for Julia packages. This package is intended to provide a simple workflow for maintaining local registries (private or public) without making any assumptions about how the registry or the packages are hosted.

[LoopVectorization.jl](https://juliasimd.github.io/LoopVectorization.jl/latest/)

This library provides the `@turbo` macro, which may be used to prefix a for loop or broadcast statement. It then tries to vectorize the loop to improve runtime performance.

[VectorizationBase.jl](https://github.com/JuliaSIMD/VectorizationBase.jl)

This is a library providing basic SIMD support in Julia.

[FLoops.jl](https://juliafolds.github.io/FLoops.jl/dev/)

FLoops.jl provides a macro `@floop`. It can be used to generate a fast generic sequential and parallel iteration over complex collections.

[FoldsCUDA.jl](https://juliafolds.github.io/FoldsCUDA.jl/dev/)

FoldsCUDA.jl provides Transducers.jl-compatible fold (reduce) implemented using CUDA.jl. This brings the transducers and reducing function combinators implemented in Transducers.jl to GPU. Furthermore, using FLoops.jl, you can write parallel for loops that run on GPU.

[Pipe.jl](https://github.com/oxinabox/Pipe.jl)

Place `@pipe` at the start of the line for which you want "advanced piping functionality" to work. This works the same as Julia piping, except if you place a underscore in the right-hand expression, it will be replaced with the result of the left-hand expression.

[NetworkOptions.jl](https://github.com/JuliaLang/NetworkOptions.jl)

The `NetworkOptions` package acts as a mediator between ways of configuring network transport mechanisms (SSL/TLS, SSH, proxies, etc.) and Julia packages that provide access to transport mechanisms. This allows the a common interface to configuring things like TLS and SSH host verification and proxies via environment variables (currently) and other configuration mechanisms (in the future), while packages that need to configure these mechanisms can simply ask `NetworkOptions` what to do in specific situations without worrying about how that configuration is expressed.

[Tokenize.jl](https://github.com/JuliaLang/Tokenize.jl)

Tokenize is a Julia package that serves a similar purpose and API as the tokenize module in Python but for Julia.

[JuliaSyntax.jl](https://github.com/JuliaLang/JuliaSyntax.jl)

A Julia frontend, written in Julia.

[BinaryBuilder.jl](https://docs.binarybuilder.org/stable/)

The purpose of the BinaryBuilder.jl Julia package is to provide a system for compiling 3rd-party binary dependencies that should work anywhere the official Julia distribution does.

[Yggdrasil](https://github.com/JuliaPackaging/Yggdrasil)

This repository contains recipes for building binaries for Julia packages using BinaryBuilder.jl.

[JLLPrefixes.jl](https://github.com/JuliaPackaging/JLLPrefixes.jl)

Collect, symlink and copy around prefixes of JLL packages! This package makes it easy to use prefixes of JLL packages outside of Julia, either by symlinking or copying them to a stable prefix.

[Gen.jl](https://www.gen.dev/stable/)

A general-purpose probabilistic programming system with programmable inference, embedded in Julia

[Soss.jl](https://cscherrer.github.io/Soss.jl/stable/)

Soss is a library for probabilistic programming.

[Tilde.jl]ttps://github.com/cscherrer/Tilde.jl)

WIP, successor to Soss.jl.

[AutoSysimages.jl](https://petvana.github.io/AutoSysimages.jl/stable/)

This package automates building of user-specific system images (sysimages) for the specific project.

[NBInclude.jl](https://github.com/stevengj/NBInclude.jl)

NBInclude is a package for the Julia language which allows you to include and execute IJulia (Julia-language Jupyter) notebook files just as you would include an ordinary Julia file.

[SoftGlobalScope.jl](https://github.com/stevengj/SoftGlobalScope.jl)

SoftGlobalScope is a package for the Julia language that simplifies the variable scoping rules for code in global scope. It is intended for interactive shells (the REPL, IJulia, etcetera) to make it easier to work interactively with Julia, especially for beginners.

[OhMyREPL.jl](https://github.com/KristofferC/OhMyREPL.jl)

A package that hooks into the Julia REPL and gives it syntax highlighting, bracket highlighting, rainbow brackets and other goodies.

[TimerOutputs.jl](https://github.com/KristofferC/TimerOutputs.jl)

TimerOutputs is a small Julia package that is used to generate formatted output from timings made in different sections of a program. It's main functionality is the `@timeit` macro, similar to the `@time` macro in Base except one also assigns a label to the code section being timed. Multiple calls to code sections with the same label (and in the same "scope") will accumulate the data for that label. After the program has executed, it is possible to print a nicely formatted table presenting how much time, allocations and number of calls were made in each section. The output can be customized as to only show the things you are interested in.

[ArgParse.jl](https://github.com/carlobaldassi/ArgParse.jl)

ArgParse.jl is a package for parsing command-line arguments to Julia programs.

[IonCLI.jl](https://github.com/Roger-luo/IonCLI.jl)

A CLI package manager for Julia.

[IonBase.jl](https://github.com/Roger-luo/IonBase.jl)

The Base package for IonCLI. This includes all the builtin functionalities in IonCLI. It can be extended by developers by overloading some of the interfaces.

[RegistryCompatTools.jl](https://github.com/KristofferC/RegistryCompatTools.jl)

Collecting packages you have commit access to.

[Rewrite.jl](https://github.com/HarrisonGrodin/Rewrite.jl)

Rewrite.jl is an efficient symbolic term rewriting engine.

[RewriteRepl.jl](https://github.com/MasonProtter/RewriteRepl.jl)

This package provides a repl mode for the Julia term rewriting system Rewrite.jl.

[Currier.jl](https://github.com/MasonProtter/Currier.jl)

Julia's multiple dispatch offers a lot of expressive power which is a strict superset of things like currying in Haskell. However, sometimes it's convenient to have currying and one doesn't want to write out the methods themselves. 

[SpecializeVarargs.jl](https://github.com/MasonProtter/SpecializeVarargs.jl)

SpecializeVarargs.jl does one thing: force to julia to create and specialize on a given number of varadic arguments. This is likely only useful to people doing very complicated codegen in high performance situations, e.g. in Cassette overdub methods like those used in ForwardDiff2.jl.

[ExprTools.jl](https://github.com/invenia/ExprTools.jl)

ExprTools provides tooling for working with Julia expressions during metaprogramming. This package aims to provide light-weight performant tooling without requiring additional package dependencies.

[MacroTools.jl](https://github.com/FluxML/MacroTools.jl)

MacroTools provides a library of tools for working with Julia code and expressions. This includes a powerful template-matching system and code-walking tools that let you do deep transformations of code in a few lines.

[InfixFunctions.jl](https://github.com/MasonProtter/InfixFunctions.jl)

Julia infix function hack, based on this Python hack: http://code.activestate.com/recipes/384122-infix-operators

[Spec.jl](https://github.com/MasonProtter/Spec.jl)

Spec.jl is an experimental package trying to incorportate ideas from Clojure's spec. The idea in Spec.jl is that we define `@pre_spec` and/or `@post_spec` functions which are run before and/or after a given function when in a validation context.

[BootlegCassette.jl](https://github.com/MasonProtter/BootlegCassette.jl)

BootlegCassette.jl is a quick and dirty package that tries to mimic the interface of Cassette.jl using IRTools.jl under the hood. This isn't a great implementation, but provided you do not use tagging and only use `@context`, `ovderdub`, `prehook`, `posthook` and `recurse`, BootlegCassette.jl should work as a drop-in replacement for Cassette.jl.

[LegibleLambdas.jl](https://github.com/MasonProtter/LegibleLambdas.jl)

Legible Lambdas for Julia.

[LambdaFn.jl](https://github.com/haberdashPI/LambdaFn.jl)

This small package provides an alternative syntax for writing anonymous functions. It allows the use of three types of function arguments within the body of a call to `@λ` or `@lf`.

[Underscores.jl](https://github.com/c42f/Underscores.jl)

Underscores provides a macro `@_` for passing closures to functions by interpreting `_` placeholders as anonymous function arguments. For example `@_ map(_+1, xs)` means `map(x->x+1, xs)`.

[Lazy.jl](https://github.com/MikeInnes/Lazy.jl)

Lazy.jl provides Julia with the cornerstones of functional programming - lazily-evaluated lists and a large library of functions for working with them.

[ToggleableAsserts.jl](https://github.com/MasonProtter/ToggleableAsserts.jl)

Assertions that can be turned on or off with a switch, with no runtime penalty when they're off.

[NaturallyUnitful.jl](https://github.com/MasonProtter/NaturallyUnitful.jl)

This package reexports Unitful.jl alongside two extra functions: 1) `natural`, a function for converting a given quantity to the Physicist's so-called "natural units", 2) `unnatural`, a function for converting from natural units to a given unnatural unit such as meters.

[StaticModules.jl](https://github.com/MasonProtter/StaticModules.jl)

A StaticModule is basically a little, statically sized and typed namespace you can use for enclosing Julia code and variables without runtime overhead and useable in either the global or local scopes. StaticModules are not a replacement modules, but may be complementary.

[ReplMaker.jl](https://github.com/MasonProtter/ReplMaker.jl)

The idea behind ReplMaker.jl is to make a tool for building (domain specific) languages in Julia.

[Gaius.jl](https://github.com/MasonProtter/Gaius.jl)

Gaius.jl is a multi-threaded BLAS-like library using a divide-and-conquer strategy to parallelism, and built on top of the fantastic LoopVectorization.jl. Gaius spawns threads using Julia's depth first parallel task runtime and so Gaius's routines may be fearlessly nested inside multi-threaded Julia programs.

[CompTime.jl](https://github.com/MasonProtter/CompTime.jl)

The goal of this library is to allow for a simplified style of writing `@generated` functions, inspired by zig comptime features.

[Bumper.jl](https://github.com/MasonProtter/Bumper.jl)

Bumper.jl is an experimental package that aims to make working with bump allocators easy and safer (when used right). You can dynamically allocate memory to these bump allocators, and reset them at the end of a code block, just like Julia’s default stack. Allocating to the a `AllocBuffer` with Bumper.jl can be just as efficient as stack allocation.

[Rhea.jl](https://github.com/jkrumbiegel/Rhea.jl)

A Julia port of [Rhea](https://github.com/Nocte-/rhea), an improved C++ implementation of the Cassowary constraint solver.

[FilteredGroupbyMacro.jl](https://github.com/jkrumbiegel/FilteredGroupbyMacro.jl)

FilteredGroupbyMacro.jl offers a macro `@by` with which a concise syntax for filtered split-apply-combine operations can be expressed concisely. It is very similar in nature to the `[i,j,by]` indexing that the well-known package data.table in the R ecosystem uses.

[ClearStacktrace.jl](https://github.com/jkrumbiegel/ClearStacktrace.jl)

ClearStacktrace.jl is an experimental package that replaces `Base.show_backtrace` with a clearer version that uses alignment and colors to reduce visual clutter and indicate module boundaries, and expands base paths so they are clickable.

[Observables.jl](https://github.com/JuliaGizmos/Observables.jl)

Observables are like Refs but you can listen to changes.

[Observables2.jl](https://github.com/jkrumbiegel/Observables2.jl)

A different take on the Observables idea, where listeners and inputs are linked both ways to avoid observable hell.

[SimpleWeave.jl](https://github.com/jkrumbiegel/SimpleWeave.jl)

This package offers a simplified syntax to create documents for use with Weave.jl.

[VersionBenchmarks.jl](https://github.com/jkrumbiegel/VersionBenchmarks.jl)

A package to run benchmarks of different versions, branches, or commits of packages against each other on multiple Julia versions.

[Chain.jl](https://github.com/jkrumbiegel/Chain.jl)

A Julia package for piping a value through a series of transformation expressions using a more convenient syntax than Julia's native piping functionality.

[ReadableRegex.jl](https://github.com/jkrumbiegel/ReadableRegex.jl)

A package for people who don't want to learn or read regexes. ReadableRegex.jl gives you a syntax that is much easier to write and understand than the rather cryptic standard Regex. The syntax is as close as possible to a natural language description of the Regex. Especially for nested and grouped expressions with special characters it is orders of magnitudes easier to understand than a simple regex string. You can also compile the function expression before using it with the @compile macro for lower runtime costs.

[HotTest.jl](https://github.com/jkrumbiegel/HotTest.jl)

HotTest.jl is an experimental package which works in conjunction with Julia's default testing pipeline in Pkg.jl. The tests are run in a sandbox module in the current session, this means that compilation delays only matter for the first run, but each run is still independent from session state. Rerunning the tests afterwards should be quick.

[LanguageServer.jl](https://github.com/julia-vscode/LanguageServer.jl)

This package implements the Microsoft Language Server Protocol for the Julia programming language.

[SymbolServer.jl](https://github.com/julia-vscode/SymbolServer.jl)

SymbolServer is a helper package for LanguageServer.jl that provides information about internal and exported variables of packages (without loading them). A package's symbol information is initially loaded in an external process but then stored on disc for (quick loading) future use.

[JuliaFormatter.jl](https://github.com/domluna/JuliaFormatter.jl)

Width-sensitive formatter for Julia code. Inspired by gofmt, refmt, and black.

[StaticLint.jl](https://github.com/julia-vscode/StaticLint.jl)

Static Code Analysis for Julia.

[CSTParser.jl](https://github.com/julia-vscode/CSTParser.jl)

A parser for Julia using Tokenize that aims to extend the built-in parser by providing additional meta information along with the resultant AST.

[DebugAdapter.jl](https://github.com/julia-vscode/DebugAdapter.jl)

Julia implementation of the Debug Adapter Protocol.

[ChromeProfileFormat.jl](https://github.com/julia-vscode/ChromeProfileFormat.jl)

This package exports Julia profile information into Chrome .cpuprofile files.

[TestItemRunner.jl](https://github.com/julia-vscode/TestItemRunner.jl)

This package runs `@testitem` tests.

[TestItems.jl](https://github.com/julia-vscode/TestItems.jl)

This package provides the `@testitem` macro for the test runner feature in VS Code. Please look at https://github.com/julia-vscode/TestItemRunner.jl for the documentation for this package here.

[TestItemDetection.jl](https://github.com/julia-vscode/TestItemDetection.jl)

This package provides low level features that are used by TestItemRunner.jl and LanguageServer.jl.

[TestProgress.jl](https://github.com/invenia/TestProgress.jl)

Letting you know your tests are making progress, by printing a green dot • every time a test passes, and announcing when a `@testset` is finished.

[JSONRPC.jl](https://github.com/julia-vscode/JSONRPC.jl)

An implementation for JSON RPC 2.0. See the specification for details. Currently, only JSON RPC 2.0 is supported. This package can act as both a client & a server.

[DeferredFutures.jl](https://github.com/invenia/DeferredFutures.jl)

A `DeferredFuture` is like a regular Julia `Future`, but is initialized when put! is called on it. This means that the data in the `DeferredFuture` lives with the process the data was created on. The process the `DeferredFuture` itself lives on never needs to fetch the data to its process. This is useful when there is a lightweight controller process which handles scheduling work on and transferring data between multiple machines.

[Syslogs.jl](https://github.com/invenia/Syslogs.jl)

Julia syslog interface.

[Trace.jl](https://github.com/invenia/Trace.jl)

Trace provides a minimal interface for zero overhead program tracing in Julia.

[ReadWriteLocks.jl](https://github.com/invenia/ReadWriteLocks.jl)

A simple read-write lock for Julia.

[FullNetworkSystems.jl](https://github.com/invenia/FullNetworkSystems.jl)

Definitions of the Julia types for simulating an ISO's market clearing.

[Checkpoints.jl](https://github.com/invenia/Checkpoints.jl)

Checkpoints.jl allows packages to `register` checkpoints which can serialize objects to disk during the execution of an application program, if the application program `config`ures them.

[Mocking.jl](https://github.com/invenia/Mocking.jl)

Allows Julia function calls to be temporarily overloaded for purpose of testing.

[Memento.jl](https://github.com/invenia/Memento.jl)

Memento is flexible hierarchical logging library for Julia.

[CloudWatchLogs.jl](https://github.com/invenia/CloudWatchLogs.jl)

CloudWatchLogs.jl provides easy access to CloudWatch Log Streams, and provides a Memento log handler.

[Parallelism.jl](https://github.com/invenia/Parallelism.jl)

A library for threaded and distributed parallelism.

[RemoteSemaphores.jl](https://github.com/invenia/RemoteSemaphores.jl)

A `RemoteSemaphore` is a counting semaphore that lives on a particular process in order to control access to a resource from multiple processes.

[Julianne.jl](https://github.com/felipenoris/Julianne.jl)

Automated package testing infrastructure and distributed testing for Julia's master branch.

[WhatIsFast.jl](https://github.com/felipenoris/WhatIsFast.jl)

Compares a few implementation possibilities for a certain tasks in Julia to find out what's the fastest solution.

[Lifting.jl](https://github.com/felipenoris/Lifting.jl)

Provides `lift` and `unlift` functions.

[JuliaPackageWithRustDep.jl](https://github.com/felipenoris/JuliaPackageWithRustDep.jl)

This is a set of examples on how to embed a Rust library in a Julia package. Interfacing between Julia and Rust library is done using Rust's FFI: the Rust library is exposed as a C dynamic library, and Julia will call Rust functions using `ccall`.

[OptimizingIR.jl](https://github.com/felipenoris/OptimizingIR.jl)

An Intermediate Representation (IR) on steroids.

[JSONWebTokens.jl](https://github.com/felipenoris/JSONWebTokens.jl)

Secure your Julia APIs with JWT.

[Fontconfig.jl](https://github.com/JuliaGraphics/Fontconfig.jl)

Fontconfig.jl provides basic binding to fontconfig.

[Showoff.jl](https://github.com/JuliaGraphics/Showoff.jl)

Showoff provides an interface for consistently formatting an array of n things, e.g. numbers, dates, unitful values.

[RadeonProRender.jl](https://github.com/JuliaGraphics/RadeonProRender.jl)

Julia wrapper for AMD's RadeonProRender library.

[Cairo.jl](https://github.com/JuliaGraphics/Cairo.jl)

Bindings to the Cairo graphics library.

[Measures.jl](https://github.com/JuliaGraphics/Measures.jl)

This library generalizes and unifies the notion of measures used in Compose, Compose3D, and Escher.

[FreeType.jl](https://github.com/JuliaGraphics/FreeType.jl)

FreeType bindings for Julia.

[IProfile.jl](https://github.com/timholy/IProfile.jl)

This package contains an "instrumenting profiler" for the Julia language. Profilers are mainly used for code optimization, particularly to find bottlenecks.

[MethodAnalysis.jl](https://github.com/timholy/MethodAnalysis.jl)

This package is useful for inspecting Julia's internals, particularly its MethodInstances and their backedges. See the documentation for details.

[ProfileView.jl](https://github.com/timholy/ProfileView.jl)

This package contains tools for visualizing and interacting with profiling data collected with Julia's built-in sampling profiler. It can be helpful for getting a big-picture overview of the major bottlenecks in your code, and optionally highlights lines that trigger garbage collection as potential candidates for optimization.

[FlameGraphs.jl](https://github.com/timholy/FlameGraphs.jl)

FlameGraphs is a package that adds functionality to Julia's Profile standard library. It is directed primarily at the algorithmic side of producing flame graphs, but includes some "format agnostic" rendering code.

[CodeTracking.jl](https://github.com/timholy/CodeTracking.jl)

CodeTracking can be thought of as an extension of Julia's InteractiveUtils library. It provides an interface for obtaining: the strings and expressions of method definitions, the method signatures at a specific file & line number, location information for "dynamic" code that might have moved since it was first loaded, a list of files that comprise a particular package.

[SnoopCompile.jl](https://github.com/timholy/SnoopCompile.jl)

SnoopCompile observes the Julia compiler, causing it to record the functions and argument types it's compiling. From these lists of methods, you can generate lists of `precompile` directives that may reduce the latency between loading packages and using them to do "real work."

[ProgressMeter.jl](https://github.com/timholy/ProgressMeter.jl)

Progress meter for long-running operations in Julia

[CommonMark.jl](https://github.com/MichaelHatherly/CommonMark.jl)

A CommonMark-compliant parser for Julia.

[Reexport.jl](https://github.com/simonster/Reexport.jl)

Maybe you have a module `X` that depends on module `Y` and you want using `X` to pull in all of the symbols from `Y`. Maybe you have an outer module `A` with an inner module `B`, and you want to export all of the symbols in `B` from `A`.

[Debugger.jl](https://github.com/JuliaDebug/Debugger.jl)

A Julia debugger.

[Infiltrator.jl](https://github.com/JuliaDebug/Infiltrator.jl)

This packages provides the `@infiltrate` macro, which acts as a breakpoint with negligible runtime performance overhead.

[CassetteOverlay.jl](https://github.com/JuliaDebug/CassetteOverlay.jl)

An experimental simple method overlay mechanism for Julia.

[Cthulhu.jl](https://github.com/JuliaDebug/Cthulhu.jl)

Cthulhu can help you debug type inference issues by recursively showing the type-inferred code until you find the exact point where inference gave up, messed up, or did something unexpected.

[JuliaInterpreter.jl](https://github.com/JuliaDebug/JuliaInterpreter.jl)

An interpreter for Julia code.

[LoweredCodeUtils.jl](https://github.com/JuliaDebug/LoweredCodeUtils.jl)

Tools for manipulating Julia's lowered code.

[Gallium.jl](https://github.com/JuliaDebug/Gallium.jl)

The Julia debugger (now deprecated).

[Rebugger.jl](https://github.com/timholy/Rebugger.jl)

Rebugger is an expression-level debugger for Julia. It has no ability to interact with or manipulate call stacks (see Gallium), but it can trace execution via the manipulation of Julia expressions.

[CallGraphs.jl](https://github.com/timholy/CallGraphs.jl)

A package for analyzing source-code callgraphs, particularly of Julia's src/ directory.

[ComputationalResources.jl](https://github.com/timholy/ComputationalResources.jl)

A Julia package for choosing resources (hardware, libraries, algorithms) for performing computations. It exports a set of types that allow you to dispatch to different methods depending on the selected resources. It also includes simple facilities that help package authors manage the loading of code in a way that depends on resource availability and user choice.

[DebuggingUtilities.jl](https://github.com/timholy/DebuggingUtilities.jl)

This package contains simple utilities that may help debug julia code.

[HeaderREPLs.jl](https://github.com/timholy/HeaderREPLs.jl)

HeaderREPLs.jl is a Julia package that allows you to extend Julia's amazing REPL to support "mini-applications" that need to communicate more information to the user. They allow you to define a custom "header" type that you can use to print extra information above the prompt.

[TerminalRegressionTests.jl](https://github.com/JuliaDebug/TerminalRegressionTests.jl)

This package builds upon the VT100.jl package to provide automated testing of terminal based application. Both plain text and formatted output is supported.

[BasicBlockRewriter.jl](https://github.com/timholy/BasicBlockRewriter.jl)

This package explores a new approach to latency reduction in Julia. It determines the unique basic blocks present across a swath of Julia code and rewrites functions as calls to "fragments" that represent these blocks.

[MacroExpandJL.jl](https://github.com/timholy/MacroExpandJL.jl)

Save the result of macro-expanded functions to Julia files.

[ThreadedIterables.jl](https://github.com/marekdedic/ThreadedIterables.jl)

This package adds multi-threaded versions of functions operating on collections. Currently, the package contains the functions `foreach`, `map`, `reduce`, `mapreduce`, `mapfoldl`, `mapfoldr`.

[SymbolicLP.jl](https://github.com/timholy/SymbolicLP.jl)

This repository is deprecated; its only purpose is to provide a platform for the experimental Layout.jl which was written on top of this repository. Anyone starting fresh should use JuMP instead.

[FileTrees.jl](https://github.com/shashi/FileTrees.jl)

Easy everyday parallelism with a file tree abstraction.

[Pukeko.jl](https://github.com/IainNZ/Pukeko.jl)

Testing for Julia, simplified.

[InControl.jl](https://github.com/malmaud/InControl.jl)

This package lets you customize the behavior of control statements like `if` and `while` by dispatching to user-defined functions based on the type of the predicate.

[Titan.jl](https://github.com/malmaud/Titan.jl)

Write CUDA kernels as normal Julia functions.

[Autoreload.jl](https://github.com/malmaud/Autoreload.jl)

A package for autoreloading files in Julia. Useful for interactive work. Modeled after IPython's autoreload extension. 

[OptimizationBenchmarks.jl](https://github.com/ahwillia/OptimizationBenchmarks.jl)

Some benchmark optimization problems for testing new algorithms.

[DaemonMode.jl](https://github.com/dmolina/DaemonMode.jl)

Inspired by the daemon-mode of Emacs, this package uses a server/client model. This allows julia to run scripts a lot faster, because the package is maintained in memory between the runs of (to run the same script several times).

[MIMEBundles.jl](https://github.com/JuliaGizmos/MIMEBundles.jl)

Generate Jupyter-style MIME bundles for various types.

[FirstAndLastPlaceValues.jl](https://github.com/JeffreySarnoff/FirstAndLastPlaceValues.jl)

Values of the initial and final bits of a significand, in absolute and relative units.

[TypedDelegation.jl](https://github.com/JeffreySarnoff/TypedDelegation.jl)

Easily apply functions onto fields' values. Use a struct's fields as operands for operations on values of that type.

[Aggregators.jl](https://github.com/JeffreySarnoff/Aggregators.jl)

Characterizing functions applicable to windowed data streams.

[Confirms.jl](https://github.com/JeffreySarnoff/Confirms.jl)

@confirm is an @Assert you can vanish.

[Fjage.jl](https://github.com/org-arl/Fjage.jl)

fjåge provides a lightweight and easy-to-learn framework for agent-oriented software development in Java and Groovy. Fjage.jl is a Julia port of fjåge, which can be independently used, or used in conjunction with a Java/Groovy container in a multi-language application.

[AllocCheck.jl](https://github.com/JuliaLang/AllocCheck.jl)

AllocCheck.jl is a Julia package that statically checks if a function call may allocate by analyzing the generated LLVM IR of it and its callees using LLVM.jl and GPUCompiler.jl

[ConventionalApp.jl](https://github.com/ma-laforge/ConventionalApp.jl)

Provides tools to help deploy/execute conventional applications in Julia.

[Rematch.jl](https://github.com/RelationalAI-oss/Rematch.jl)

Rematch.jl provides a syntax sugar for matching Julia values against syntactic patterns. The `@match` macro expands a pattern-matching syntax into a series of if-elses that check the types and structure of the provided value, allowing you to more simply write checks that describe your intent.

[DefaultApplication.jl](https://github.com/tpapp/DefaultApplication.jl)

Julia library for opening a file with the default application determined by the OS.

[Ansillary.jl](https://github.com/Sean1708/Ansillary.jl)

A Julia package for interacting with ANSI terminals.

[Terming.jl](https://github.com/foldfelis/Terming.jl)

Terming is a toolbox for manipulate terminals written in pure Julia. It offers low-level and elegant APIs to handle information directly from and work around with TTYs.

[DisplayStructure.jl](https://github.com/foldfelis/DisplayStructure.jl)

DisplayArray provides arrays that index character in text width unit. And therefore maintains an immutable display size for terminal emulators.

[TerminalUserInterfaces.jl](https://github.com/kdheepak/TerminalUserInterfaces.jl)

Create TerminalUserInterfaces in Julia.

[Chairmarks.jl](https://github.com/LilithHafner/Chairmarks.jl)

Chairmarks measures performance hundreds of times faster than BenchmarkTools without compromising on accuracy.

[MKL.jl](https://github.com/JuliaLinearAlgebra/MKL.jl)

MKL.jl is a Julia package that allows users to use the Intel MKL library for Julia's underlying BLAS and LAPACK, instead of OpenBLAS, which Julia ships with by default. Julia includes libblastrampoline, which enables picking a BLAS and LAPACK library at runtime.

[AppleAccelerate.jl](https://github.com/JuliaLinearAlgebra/AppleAccelerate.jl)

This provides a Julia interface to some of the macOS Accelerate framework.

[JuliaScript.jl](https://github.com/jolin-io/JuliaScript.jl)

You have a julia script.jl and want to run it fast? Welcome to juliascript! It is build for exactly that purpose.

[About.jl](https://github.com/tecosaur/About.jl)

Sometimes you want to know more about what you’re working with, whether it be a function, type, value, or something else entirely. This package is a utility to help answer that question, it exports a single function `about`, which can be applied to any Julia object.

[TrueRandom.jl](https://github.com/tecosaur/TrueRandom.jl)

Alternate random number generators based on ‘True’ random sources. Approximately a thousand times slower than the default Random.MersenneTwister generator, and relies on a network connection.

[HelpfulMethodErrors.jl](https://github.com/tecosaur/HelpfulMethodErrors.jl)

Julia’s errors are much less helpful than they could (or should, as some would say) be. Let’s try to bridge the gap a bit. This package is a prototype for functionality that I think should be in the Base language. This isn’t a complete solution, but I think it’s a step in the right direction.

[InteractivePrompts.jl](https://github.com/tecosaur/InteractivePrompts.jl)

A flexible foundation for basic user interaction.

[Terminfo.jl](https://github.com/tecosaur/Terminfo.jl)

A pure-Julia parser for (binary/compiled) terminfo files, with the accompanying terminfo data to be practically useful.

[KangarooTwelve.jl](https://github.com/tecosaur/KangarooTwelve.jl)

A pure-Julia implementation of the KangarooTwelve hashing scheme, so named because it consists of 12 rounds of Keccak (TurboSHAKE128) with kangaroo hopping, allowing for parallel tree-ish hashing termed “leaves stapled to a pole”.

[CheckDoc.jl](https://github.com/tecosaur/CheckDoc.jl)

A Julia docstring linter to check for various quality indications, taking into account the method(s) a docstring applies to.

[DaemonConductor.jl](https://github.com/tecosaur/DaemonConductor.jl)

Run a script many times, compile it once.

[DotEnv.jl](https://github.com/tecosaur/DotEnv.jl)

DotEnv.jl is a lightweight package that loads environment variables from .env files into =ENV=. Storing configuration in the environment is based on The Twelve-Factor App methodology.

[RemoteFiles.jl](https://github.com/helgee/RemoteFiles.jl)

Download files from the Internet and keep them up-to-date.

[DataSets.jl](https://github.com/JuliaComputing/DataSets.jl)

DataSets helps make data wrangling code more reusable.

[DataToolkit.jl](https://github.com/tecosaur/DataToolkit.jl)

DataToolkit is a batteries-included family of packages for robustly managing data.

[DataToolkitDocumenter.jl](https://github.com/tecosaur/DataToolkitDocumenter.jl)

This is a tiny package that allows data sets to be documented in the same manner as functions/variables.

[DataDeps.jl](https://github.com/oxinabox/DataDeps.jl)

Reproducible data setup for reproducible science.

[BaseDirs.jl](https://github.com/tecosaur/BaseDirs.jl)

This package exists to help you put and look for files in the appropriate place(s).

[PropertyUtils.jl](https://github.com/joshday/PropertyUtils.jl)

Properties in Julia made easy.

[Notcurses.jl](https://github.com/kdheepak/Notcurses.jl)

Notcurses.jl is a Julia package that provides a wrapper over notcurses, the most blingful TUI library.

[BorrowChecker.jl](https://github.com/MilesCranmer/BorrowChecker.jl)

This package implements a borrow checker in Julia using a macro layer over standard Julia code. This is built to emulate Rust's ownership, lifetime, and borrowing semantics. This tool is mainly to be used in development and testing to flag memory safety issues, and help you design safer code.

---

### Parallel computing

[MPI.jl](https://github.com/JuliaParallel/MPI.jl)

This provides Julia interface to the Message Passing Interface (MPI), roughly inspired by mpi4py.

[Dagger.jl](https://github.com/JuliaParallel/Dagger.jl)

At the core of Dagger.jl is a scheduler heavily inspired by Dask. It can run computations represented as directed-acyclic-graphs (DAGs) efficiently on many Julia worker processes and threads, as well as GPUs via DaggerGPU.jl.

[DaggerGPU.jl](https://github.com/JuliaGPU/DaggerGPU.jl)

GPU integrations for Dagger.jl.

[DTables.jl](https://github.com/JuliaParallel/DTables.jl)

Distributed table structures and data manipulation operations built on top of Dagger.jl

[ClusterManagers.jl](https://github.com/JuliaParallel/ClusterManagers.jl)

Support for different job queue systems commonly used on compute clusters.

[MPIBenchmarks.jl](https://github.com/JuliaParallel/MPIBenchmarks.jl)

MPIBenchmarks.jl is a collection of benchmarks for MPI.jl, the Julia wrapper for the Message Passing Interface (MPI).

[Hwloc.jl](https://github.com/JuliaParallel/Hwloc.jl)

Hwloc.jl is a high-level wrapper of the hwloc library. It examines the current machine's hardware topology (memories, caches, cores, etc.) and provides Julia functions to visualize and access this information conveniently.

[DistributedArrays.jl](https://github.com/JuliaParallel/DistributedArrays.jl)

DistributedArrays.jl uses the stdlib `Distributed` to implement a Global Array interface. A `DArray` is distributed across a set of workers. Each worker can read and write from its local portion of the array and each worker has read-only access to the portions of the array held by other workers.

[Elly.jl](https://github.com/JuliaParallel/Elly.jl)

Elly is a Hadoop HDFS and Yarn client.

[MPIClusterManagers.jl](https://github.com/JuliaParallel/MPIClusterManagers.jl)

In order for MPI calls to be made from a Julia cluster, it requires the use of `MPIManager`, a cluster manager that will start the julia workers using `mpirun`.

[Elemental.jl]ttps://github.com/JuliaParallel/Elemental.jl)

A package for dense and sparse distributed linear algebra and optimization. The underlying functionality is provided by the C++ library Elemental written originally by Jack Poulson and now maintained by LLNL.

[PETSc.jl](https://github.com/JuliaParallel/PETSc.jl)

This package provides a low level interface for PETSc and allows combining julia features (such as automatic differentiation) with the PETSc infrastructure and nonlinear solvers.

[Tullio.jl](https://github.com/mcabbott/Tullio.jl)

Tullio is a very flexible einsum macro. It understands many array operations written in index notation -- not just matrix multiplication and permutations, but also convolutions, stencils, scatter/gather, and broadcasting.

[DaggerArrays.jl](https://github.com/JuliaParallel/DaggerArrays.jl)

This is an attempt to use Tullio.jl to run tensor expressions on Dagger-based distributed arrays.

[MessageUtils.jl](https://github.com/JuliaParallel/MessageUtils.jl)

A collection of utilities for messaging.

[DProfile.jl](https://github.com/JuliaParallel/DProfile.jl)

Simple distributed variant of `Base.Profile`.

[DagScheduler.jl](https://github.com/JuliaParallel/DagScheduler.jl)

Computational problems can often be represented as specific types of graphs known as directed acyclic graphs (DAGs). That allows an execution framework to execute parts in right order, schedule in parallel, and make optimal use of available resources.

[ThreadsX.jl](https://github.com/tkf/ThreadsX.jl)

Add prefix `ThreadsX`. to functions from Base to get some speedup, if supported.

[GPUifyLoops.jl](https://github.com/vchuravy/GPUifyLoops.jl)

Support for writing loop-based code that executes both on CPU and GPU.

---

### Interoperability

[PyRun.jl](https://github.com/cjdoris/PyRun.jl)

Reliable Python-Julia interop.

[PyCall.jl](https://github.com/JuliaPy/PyCall.jl)

This package provides the ability to directly call and fully interoperate with Python from the Julia language. You can import arbitrary Python modules from Julia, call Python functions (with automatic conversion of types between Julia and Python), define Python classes from Julia methods, and share large data structures between Julia and Python without copying them.

[Conda.jl](https://github.com/JuliaPy/Conda.jl)

This package allows one to use conda as a cross-platform binary provider for Julia for other Julia packages, especially to install binaries that have complicated dependencies like Python.

[SymPy.jl](https://github.com/JuliaPy/SymPy.jl)

SymPy is a Python library for symbolic mathematics.

[SciPy.jl](https://atsushisakai.github.io/SciPy.jl/stable/)

A Julia interface for SciPy using PyCall.jl.

[PyPlot.jl](https://github.com/JuliaPy/PyPlot.jl)

This module provides a Julia interface to the Matplotlib plotting library from Python, and specifically to the `matplotlib.pyplot` module.

[PyCallJLD.jl](https://github.com/JuliaPy/PyCallJLD.jl)

PyCallJLD enables saving and loading PyCall's PyObjects using JLD.jl.

[Seaborn.jl](https://github.com/JuliaPy/Seaborn.jl)

A Julia wrapper around the Seaborn data visualization library.

[Pandas.jl](https://github.com/JuliaPy/Pandas.jl)

This package provides a Julia interface to the excellent Pandas package. It sticks closely to the Pandas API. One exception is that integer-based indexing is automatically converted from Python's 0-based indexing to Julia's 1-based indexing.

[RCall.jl](https://juliainterop.github.io/RCall.jl/stable/)

Allows the user to call R packages from within Julia.

[StataCall.jl](https://github.com/jmboehm/StataCall.jl)

Allows Stata operations on Julia DataFrames by exporting it to Stata, running a .do file, and re-importing the result into Julia. Requires a copy of Stata.

[MATLAB.jl](https://github.com/JuliaInterop/MATLAB.jl)

The MATLAB.jl package provides an interface for using MATLAB® from Julia using the MATLAB C api.

[MatlabCompat.jl](https://github.com/MatlabCompat/MatlabCompat.jl)

Julia library aimed at simplifying conversion of legacy MATLAB/Octave code into Julia by providing functions similar to MATLAB/Octave.

[JSExpr.jl](https://github.com/JuliaGizmos/JSExpr.jl)

This package provides two macros that are used to write JavaScript code inside of Julia programs.

[Cxx.jl](https://juliainterop.github.io/Cxx.jl/stable/)

Cxx.jl is a Julia package that provides a C++ interoperability interface for Julia. It also provides an experimental C++ REPL mode for the Julia REPL.

[jluna](https://github.com/Clemapfel/jluna)

Julia <-> C++ Wrapper with Focus on Safety, Elegance, and Ease of Use.

[CxxWrap.jl](https://github.com/JuliaInterop/CxxWrap.jl)

This package aims to provide a Boost.Python-like wrapping for C++ types and functions to Julia. The idea is to write the code for the Julia wrapper in C++, and then use a one-liner on the Julia side to make the wrapped C++ library available there.

[Clang.jl](https://github.com/JuliaInterop/Clang.jl)

This package provides a Julia language wrapper for libclang: the stable, C-exported interface to the LLVM Clang compiler. The libclang API documentation provides background on the functionality available through libclang, and thus through the Julia wrapper. The repository also hosts related tools built on top of libclang functionality.

[OctCall.jl](https://github.com/JuliaInterop/OctCall.jl)

This package allows you to call and interact with GNU Octave, a mostly Matlab-compatible free-software numerical-computing language, from Julia. It works by directly accessing the GNU Octave C++ libraries using Cxx.jl, and hence should have performance comparable to calling Octave functions from within Octave.

[ObjectiveC.jl](https://github.com/JuliaInterop/ObjectiveC.jl)

ObjectiveC.jl is an Objective-C bridge for Julia.

[JavaCall.jl](https://github.com/JuliaInterop/JavaCall.jl)

Call Java programs from Julia.

[MathLink.jl](https://github.com/JuliaInterop/MathLink.jl)

This package provides access to Mathematica/Wolfram Engine via the MathLink library, now renamed to Wolfram Symbolic Transfer Protocol (WSTP).

[DotNET.jl](https://github.com/azurefx/DotNET.jl)

This package provides interoperability between Julia and Common Language Runtime, the execution engine of .NET applications. Many languages run on CLR, including C#, Visual Basic .NET and PowerShell.

[HalideCall.jl](https://github.com/timholy/HalideCall.jl)

Halide is an promising project with an interesting design. I'm quite interested in seeing how some of their ideas work out when implemented in pure Julia, and preliminary experiments suggest that it may be possible to match the performance of Halide using pure Julia. The main purpose of this package, therefore, is for benchmark comparisons.

---

### Import/Export

[HDF5.jl](https://juliaio.github.io/HDF5.jl/stable/)

HDF5 is a file format and library for storing and accessing data, commonly used for scientific data. HDF5 files can be created and read by numerous programming languages. This package provides an interface to the HDF5 library for the Julia language.

[JLD2.jl](https://juliaio.github.io/JLD2.jl/dev/)

JLD2 saves and loads Julia data structures in a format comprising a subset of HDF5, without any dependency on the HDF5 C library.

[RData.jl](https://github.com/JuliaData/RData.jl)

Read R data files (.rda, .RData) and optionally convert the contents into Julia equivalents.

[Parquet.jl](https://github.com/JuliaIO/Parquet.jl)

A parquet file or dataset can be loaded using the `read_parquet` function. A parquet dataset is a directory with multiple parquet files, each of which is a partition belonging to the dataset.

[MAT.jl](https://github.com/JuliaIO/MAT.jl)

Read and write MATLAB files in Julia.

[ReadStatTables.jl](https://github.com/junyuan-chen/ReadStatTables.jl)

Read and write Stata, SAS and SPSS data files with Julia tables.

[ImagineFormat.jl](https://github.com/timholy/ImagineFormat.jl)

Imagine is an acquisition program for light sheet microscopy written by Zhongsheng Guo in Tim Holy's lab. This package implements a loader for the file format for the Julia programming language. Each Imagine "file" consists of two parts (as two separate files): a `*.imagine` file which contains the (ASCII) header, and a `*.cam` file which contains the camera data. The `*.cam` file is a raw byte dump, and is compatible with the NRRD "raw" file.

[BioformatsLoader.jl](https://github.com/ahnlabb/BioformatsLoader.jl)

A julia package to load images using bioformats.

[TiffImages.jl](https://github.com/tlnagy/TiffImages.jl)

This package aims to be a fast, minimal, and correct TIFF reader and writer written in Julia.

[GIFImages.jl](https://github.com/JuliaIO/GIFImages.jl)

GIFImages.jl provides support for decoding and encoding GIF images by wrapping LibGif.

[MzXML.jl](https://github.com/timholy/MzXML.jl)

A Julia package for reading mass spectrometry mzXML files.

[GIFTI.jl](https://github.com/JuliaNeuroscience/GIFTI.jl)

This package includes very basic support for loading GIFTI (.gii) files in Julia.

[NIfTI.jl](https://github.com/JuliaNeuroscience/NIfTI.jl)

Julia module for reading/writing NIfTI MRI files.

[VideoIO.jl](https://github.com/JuliaIO/VideoIO.jl)

Reading and writing of video files in Julia.

[LibPSF.jl](https://github.com/ma-laforge/LibPSF.jl)

The LibPSF.jl module provides a pure-Julia implementation of Henrik Johansson's .psf reader.

[PSFWrite.jl](https://github.com/ma-laforge/PSFWrite.jl)

The PSFWrite.jl module provides a pure-Julia .psf writer.

[SpiceData.jl](https://github.com/ma-laforge/SpiceData.jl)

The SpiceData.jl module provides a pure-Julia SPICE data file reader inspired by Michael H. Perrott's CppSim reader.

[ZipFile.jl](https://github.com/fhs/ZipFile.jl)

This module provides support for reading and writing ZIP archives in Julia.

[ZipArchives.jl](https://github.com/JuliaIO/ZipArchives.jl)

Read and write Zip archives in julia.

[Tar.jl](https://github.com/JuliaIO/Tar.jl)

TAR files: create, list, extract them in pure Julia.

[NPZ.jl](https://github.com/fhs/NPZ.jl)

The NPZ package provides support for reading and writing Numpy .npy and .npz files in Julia.

[MeshIO.jl](https://github.com/JuliaIO/MeshIO.jl)

This package supports loading 3D model file formats: obj, stl, ply, off and 2DM. More 3D model formats will be supported in the future.

[ImageMagick.jl](https://github.com/JuliaIO/ImageMagick.jl)

Thin Wrapper for the library ImageMagick.

[ImageIO.jl](https://github.com/JuliaIO/ImageIO.jl)

FileIO.jl integration for image files.

[QuartzImageIO.jl](https://github.com/JuliaIO/QuartzImageIO.jl)

Exposes macOS's native image IO functionality to Julia.

[MetaImageFormat.jl](https://github.com/JuliaIO/MetaImageFormat.jl)

This package supports the MetaImage file format for the Julia language.

[SerialPorts.jl](https://github.com/JuliaIO/SerialPorts.jl)

SerialPorts.jl lets you work with devices over serial communication with Julia. It is designed to mimic regular file IO as in the Base Julia library.

[LibSerialPort.jl](https://github.com/JuliaIO/LibSerialPort.jl)

libserialport is a small, well-documented C library for general-purpose serial port communication. This is a julia wrapper for the library.

[JpegTurbo.jl](https://github.com/stevengj/JpegTurbo.jl)

This is a work-in-progress Julia package to provide Julia-callable wrappers to the libjpeg-turbo C library.

[CBOR.jl](https://github.com/JuliaIO/CBOR.jl)

CBOR.jl is a Julia package for working with the CBOR data format, providing straightforward encoding and decoding for Julia types.

[Sixel.jl](https://github.com/JuliaIO/Sixel.jl)

Encode the image into a sixel control sequence and vice versa. If your terminal supports this format, then you can get a nice visualization of it.

[FileTypes.jl](https://github.com/JuliaIO/FileTypes.jl)

Small and dependency free Julia package to infer file and MIME type checking the magic numbers signature.

[MultifileArrays.jl](https://github.com/JuliaIO/MultifileArrays.jl)

Create a higher-dimensional array from lazily-loaded slices in separate files.

[IniFile.jl](https://github.com/JuliaIO/IniFile.jl)

Reading and writing Windows-style INI files from Julia.

[Snappy.jl](https://github.com/JuliaIO/Snappy.jl)

Snappy.jl is a Julia wrapper for the snappy library - a compression/decompression library focusing on speed.

[LightXML.jl](https://github.com/JuliaIO/LightXML.jl)

This package is a light-weight Julia wrapper of libxml2.

[AdjustCRC.jl](https://github.com/JuliaIO/AdjustCRC.jl)

This module exports two functions, adjust_crc and adjust_crc!, which allow you to write 4 bytes to a file or array in order to adjust the 32-bit CRC checksum (either CRC32 or CRC32c) to equal any desired value.

[FLAC.jl](https://github.com/JuliaIO/FLAC.jl)

Julia bindings for libFLAC.

[LibExpat.jl](https://github.com/JuliaIO/LibExpat.jl)

Julia interface to the Expat XML parser library.

[Suppressor.jl](https://github.com/JuliaIO/Suppressor.jl)

Julia macros for suppressing and/or capturing output (`stdout`), warnings (`stderr`) or both streams at the same time.

[TranscodingStreams.jl](https://github.com/JuliaIO/TranscodingStreams.jl)

Simple, consistent interfaces for any codec.

[Blosc.jl](https://github.com/JuliaIO/Blosc.jl)

This module provides fast lossless compression for the Julia language by interfacing the Blosc Library.

[Blosc2.jl](https://github.com/JuliaIO/Blosc2.jl)

Julia interface for blosc2 library.

[HexEdit.jl](https://github.com/JuliaIO/HexEdit.jl)

HexEdit is a package for editing and displaying data in binary files in hexadecimal format.

[ImgHdr.jl](https://github.com/JuliaIO/ImgHdr.jl)

Library to Check Type of Image.

[Pcap.jl](https://github.com/JuliaIO/Pcap.jl)

Pcap contains libpcap bindings for Julia as well as logic to parse and extract useful data from packet captures.

[SigMF.jl](https://github.com/JuliaIO/SigMF.jl)

This is a Julia package for working with metadata and storage associated with signals.

[StructIO.jl](https://github.com/JuliaIO/StructIO.jl)

Generates IO methods (`pack`, `unpack`) from structure definitions. Also defines `packed_sizeof` to give the on-disk size of a packed structure, which is smaller than `sizeof` would give, if the struct is marked as `align_packed`.

[Telegram.jl](https://github.com/JuliaIO/Telegram.jl)

Wrapper for the Telegram API in Julia.

[MsgPack.jl](https://github.com/JuliaIO/https://github.com/JuliaIO/MsgPack.jl)

MsgPack.jl is a MessagePack implementation in pure Julia.

[Zarr.jl](https://github.com/JuliaIO/Zarr.jl)

Zarr is a Julia package providing an implementation of chunked, compressed, N-dimensional arrays.

[ProtoBuf.jl](https://github.com/JuliaIO/ProtoBuf.jl)

This is a Julia package that provides a compiler and a codec for Protocol Buffers. Protocol Buffers are a language-neutral, platform-neutral extensible mechanism for serializing structured data.

[FFMPEG.jl](https://github.com/JuliaIO/FFMPEG.jl)

Julia Package for the FFMPEG builder binaries.

[JLD.jl](https://github.com/JuliaIO/JLD.jl)

Save and load variables in Julia Data format (JLD).

[CodecZstd.jl](https://github.com/JuliaIO/CodecZstd.jl)

A zstd codec for TranscodingStreams.jl.

[CodecLz4.jl](https://github.com/JuliaIO/CodecLz4.jl)

Transcoding codecs for compression and decompression with LZ4.

[CodecBzip2.jl](https://github.com/JuliaIO/CodecBzip2.jl)

A bzip2 codec for TranscodingStreams.jl.

[CodecXz.jl](https://github.com/JuliaIO/CodecXz.jl)

An xz codec for TranscodingStreams.jl.

[CodecZlib.jl](https://github.com/JuliaIO/CodecZlib.jl)

zlib codecs for TranscodingStreams.jl.

[CodecBase.jl](https://github.com/JuliaIO/CodecBase.jl)

Base 16/32/64 codecs for TranscodingStreams.jl.

[WavefrontObj.jl](https://github.com/JuliaIO/WavefrontObj.jl)

Wafefront Obj importer.

[ZeissMicroscopyFormat.jl](https://github.com/JuliaIO/ZeissMicroscopyFormat.jl)

This package provides support for the CZI image format for microscopy in the Julia programming language. It is registered with the FileIO package.

[HDF5Plugins.jl](https://github.com/JuliaIO/HDF5Plugins.jl)

This package formerly implemented compression, decompression plugins, and other filter plugins for HDF5.jl. bzip2, lz4, and zstd compression codecs were supported using CodecBzip2.jl, CodecLZ4.jl, and CodecZstd.jl.

[BSDiff.jl](https://github.com/JuliaIO/BSDiff.jl)

The BSDiff package is a pure Julia implementation of the bsdiff tool for computing and applying binary diffs of files.

[NRRD.jl](https://github.com/JuliaIO/NRRD.jl)

Package for reading NRRD files. Implements the FileIO interface.

[Netpbm.jl](https://github.com/JuliaIO/Netpbm.jl)

This package implements the FileIO interface for loading and saving binary Netpbm images. Other packages, such as ImageMagick, also support such formats. One advantage of this package is that it does not have any binary (e.g., external library) dependencies---it is implemented in pure Julia.

[AVSfldIO.jl](https://github.com/JuliaIO/AVSfldIO.jl)

File IO for AVS format "field" data files with extension .fld for the Julia language, in conjunction with the FileIO package. This data format supports N-dimensional arrays of real numbers.

[ArgTools.jl](https://github.com/JuliaIO/ArgTools.jl)

ArgTools provides tools for creating consistent, flexible APIs that work with various kinds of function arguments. In the current version, it helps deal with arguments that are, at their core, IO handles, but which you'd like to allow the user to specify directly as file names, commands, pipelines, or, of course, as raw IO handles. For write arguments, it's also possible to use `nothing` and write to a temporary file whose path is returned.

[BufferedStreams.jl](https://github.com/JuliaIO/BufferedStreams.jl)

BufferedStreams provides buffering for IO operations. It can wrap any IO type automatically making incremental reading and writing faster.

[AndorSIF.jl](https://github.com/JuliaIO/AndorSIF.jl)

This implements support for reading Andor SIF image files in the Julia programming language.

[GoogleDrive.jl](https://github.com/JuliaIO/GoogleDrive.jl)

Download files from Google Drive.

[EzXML.jl](https://github.com/JuliaIO/EzXML.jl)

EzXML.jl is a package to handle XML/HTML documents for primates.

[GZip.jl](https://github.com/JuliaIO/GZip.jl)

This module provides a wrapper for the gzip related functions of zlib, a free, general-purpose, legally unencumbered, lossless data-compression library. These functions allow the reading and writing of gzip files.

[BSON.jl](https://github.com/JuliaIO/BSON.jl)

BSON.jl is a Julia package for working with the Binary JSON serialization format.

[PNGFiles.jl](https://github.com/JuliaIO/PNGFiles.jl)

FileIO.jl integration for PNG files.

[ConfParser.jl](https://github.com/JuliaIO/ConfParser.jl)

ConfParser is a package for parsing, modifying, and writing to configuration files. ConfParser can handle configuration files utilizing multiple syntaxes to include INI, HTTP, and simple.

[WidthLimitedIO.jl](https://github.com/JuliaIO/WidthLimitedIO.jl)

This Julia package exports a type, `TextWidthLimiter <: IO`, which can be used to limit output to no more than a specified number of characters.

[VideoPlayer.jl](https://github.com/JuliaIO/VideoPlayer.jl)

Video player for Julia.

[Toxcore.jl](https://github.com/JuliaIO/Toxcore.jl)

Attempt to make Toxcore accessible in Julia. Toxcore, ToxAv and ToxDNS have been wrapped.

[Org.jl](https://github.com/tecosaur/Org.jl)

A library for working with Org files.

[Firestore.jl](https://github.com/joshday/Firestore.jl)

This package provides utilities for reading & writing Google Firestore documents with Julia using the REST API.

[NewsAPI.jl](https://github.com/joshday/NewsAPI.jl)

Access newsapi.org from Julia.

[IOStructs.jl](https://github.com/joshday/IOStructs.jl)

IOStructs is a Julia package that helps write structs which represent (part of) a file format.

---

### Databases

[Accumulo.jl](https://github.com/JuliaDatabases/Accumulo.jl)

Accumulo.jl is a client library for Apache Accumulo, built using the Accumulo Thrift Proxy API.

[Hive.jl](https://github.com/JuliaDatabases/Hive.jl)

A client for distributed SQL engines that provide a HiveServer2 interface. E.g.: Hive, Spark SQL, Impala.

[PostgreSQL.jl](https://github.com/JuliaDatabases/PostgreSQL.jl)

An interface to PostgreSQL from Julia. Uses libpq (the C PostgreSQL API) and obeys the DBI.jl protocol.

[LibPQ.jl](https://github.com/invenia/LibPQ.jl)

LibPQ.jl is a Julia wrapper for the PostgreSQL libpq C library.

[Redis.jl](https://github.com/JuliaDatabases/Redis.jl)

Redis.jl is a fully-featured Redis client for the Julia programming language. The implementation is an attempt at an easy to understand, minimalistic API that mirrors actual Redis commands as closely as possible.

[DBInterface.jl](https://github.com/JuliaDatabases/DBInterface.jl)

DBInterface.jl provides interface definitions to allow common database operations to be implemented consistently across various database packages.

[JDBC.jl](https://github.com/JuliaDatabases/JDBC.jl)

This package enables the use of Java JDBC drivers to access databases from within Julia. It uses the JavaCall.jl package to call into Java in order to use the JDBC drivers.

[ODBC.jl](https://github.com/JuliaDatabases/ODBC.jl)

A Julia library for interacting with the ODBC API.

[ClickHouse.jl](https://github.com/JuliaDatabases/ClickHouse.jl)

Pure Julia Lang implementation of a client for the ClickHouse TCP native API.

[MySQL.jl](https://github.com/JuliaDatabases/MySQL.jl)

Package for interfacing with MySQL databases from Julia via the MariaDB C connector library, version 3.1.6.

[SQLite.jl](https://github.com/JuliaDatabases/SQLite.jl)

A Julia interface to the sqlite library.

[Mongoc.jl](https://github.com/felipenoris/Mongoc.jl)

Mongoc.jl is a MongoDB driver for the Julia Language.

[Oracle.jl](https://github.com/felipenoris/Oracle.jl)

This package provides a driver to access Oracle databases using the Julia language, based on ODPI-C bindings.

[JuliaDB.jl](https://juliadb.juliadata.org/stable/)

JuliaDB is a package for working with persistent data sets.

[JuliaDBMeta.jl](https://github.com/JuliaData/JuliaDBMeta.jl)

JuliaDBMeta is a set of macros to simplify data manipulation with JuliaDB, heavily inspired on DataFramesMeta.

[DBFTables.jl](https://github.com/JuliaData/DBFTables.jl)

Read xBase / dBASE III+ .dbf files in Julia.

[SQLQuery.jl](https://github.com/yeesian/SQLQuery.jl)

A package for representing sql queries, and converting them to valid SQL statements. The generated SQL statements follow the specification in https://www.sqlite.org/lang_select.html, and should conform to http://www.sqlstyle.guide/ as far as possible.

[SQLAlchemy.jl](https://github.com/malmaud/SQLAlchemy.jl)

Wrapper over Python's SQLAlchemy library.

[Octo.jl](https://github.com/wookay/Octo.jl)

Octo.jl is an SQL Query DSL in Julia.

[ZoteroDB.jl](https://github.com/tecosaur/ZoteroDB.jl)

Directly interact with your local Zotero database.

---

### Data types: Tables

[CSV.jl](https://csv.juliadata.org/stable/)

CSV.jl is a pure-Julia package for handling delimited text data, be it comma-delimited (csv), tab-delimited (tsv), or otherwise.

[DataFrames.jl](https://github.com/JuliaData/DataFrames.jl)

DataFrames.jl provides a set of tools for working with tabular data in Julia.

[DataFramesMeta.jl](https://github.com/JuliaData/DataFramesMeta.jl)

Metaprogramming tools for DataFrames.jl objects to provide more convenient syntax.

[DataFrameMacros.jl](https://github.com/jkrumbiegel/DataFrameMacros.jl)

DataFrames.jl has a special mini-language for data transformations, which is powerful but often verbose.

[Tables.jl](https://github.com/JuliaData/Tables.jl)

This guide provides documentation around the powerful tables interfaces in the Tables.jl package.

[TableMetadataTools.jl](https://github.com/JuliaData/TableMetadataTools.jl)

Tools for working with metadata of Tables.jl tables in Julia.

[DataCubes.jl](https://github.com/c-s/DataCubes.jl)

DataCubes provides a framework to handle multidimensional tables.

[PrettyTables.jl](https://ronisbr.github.io/PrettyTables.jl/stable/)

This package has the purpose to print data in matrices in a human-readable format. It was inspired in the functionality provided by ASCII Table Generator.

[TypedTables.jl](https://github.com/JuliaData/TypedTables.jl)

Simple, fast, column-based storage for data analysis in Julia.

[Parsers.jl](https://github.com/JuliaData/Parsers.jl)

A collection of type parsers and utilities for Julia.

[YAML.jl](https://github.com/JuliaData/YAML.jl)

YAML is a flexible data serialization format that is designed to be easily read and written by human beings. 
This library parses YAML documents into native Julia types and dumps them back into YAML documents.

[Query.jl](https://www.queryverse.org/Query.jl/stable/)

Query is a package for querying Julia data sources. It can filter, project, join, sort and group data from any iterable data source.

[JSONTables.jl](https://github.com/JuliaData/JSONTables.jl)

A package that provides a JSON integration with the Tables.jl interface, that is, it provides the `jsontable` function as a way to treat a JSON object of arrays, or a JSON array of objects, as a Tables.jl-compatible source.

[StructTypes.jl](https://github.com/JuliaData/StructTypes.jl)

Package providing the `StructTypes.StructType` trait for Julia types to declare the kind of "struct" they are, providing serialization/deserialization packages patterns and strategies to automatically construct objects.

[DataAPI.jl](https://github.com/JuliaData/DataAPI.jl)

This package provides a namespace for data-related generic function definitions to solve the optional dependency problem; packages wishing to share and/or extend functions can avoid depending directly on each other by moving the function definition to DataAPI.jl and each package taking a dependency on it.

[Strapping.jl](https://juliadata.github.io/Strapping.jl/stable/)

"Strapping" stands for STruct Relational MAPPING, and provides ORM-like functionality for Julia, including:

[TableOperations.jl](https://github.com/JuliaData/TableOperations.jl)

Common table operations on Tables.jl compatible sources.

[SplitApplyCombine.jl](https://github.com/JuliaData/SplitApplyCombine.jl)

Strategies for nested data in Julia.

[MemPool.jl](https://github.com/JuliaData/MemPool.jl)

Simple distributed datastore that supports custom serialization, spilling least recently used data to disk and memory-mapping.

[IndexedTables.jl](https://github.com/JuliaData/IndexedTables.jl)

IndexedTables provides tabular data structures where some of the columns form a sorted index.

[FlatBuffers.jl](https://github.com/JuliaData/FlatBuffers.jl)

FlatBuffers.jl provides native Julia support for reading and writing binary structures following the google flatbuffer schema.

[Missings.jl](https://github.com/JuliaData/Missings.jl)

Convenience functions for working with missing values in Julia.

[Avro.jl](https://juliadata.github.io/Avro.jl/stable/)

The Avro.jl package provides a pure Julia implementation for reading writing data in the avro format.

[Feather.jl](https://github.com/JuliaData/Feather.jl)

Julia library for working with feather-formatted files.

[Arrow.jl](https://github.com/apache/arrow-julia)

This is a pure Julia implementation of the Apache Arrow data standard. This package provides Julia `AbstractVector` objects for referencing data that conforms to the Arrow standard. This allows users to seamlessly interface Arrow formatted data with a great deal of existing Julia code.

[Dataverse.jl](https://gdcc.github.io/Dataverse.jl/dev/)

This package is about interfaces to the Dataverse project APIs, collections, datasets, etc.

[JSON3.jl](https://quinnj.github.io/JSON3.jl/stable/)

Yet another JSON package for Julia; this one is for speed and slick struct mapping.

[Chemfiles.jl](https://github.com/chemfiles/Chemfiles.jl)

This package contains the Julia binding for the chemfiles library. It allow you, as a programmer, to read and write chemistry trajectory files easily, with the same simple interface for all the supported formats. For more information, please read the introduction to chemfiles.

[SumTypes.jl](https://github.com/MasonProtter/SumTypes.jl)

A julian implementation of sum types. Sum types, sometimes called 'tagged unions' are the type system equivalent of the disjoint union operation (which is not a union in the traditional sense). From a category theory perspective, sum types are interesting because they are dual to Tuples (whatever that means).

[MutableNamedTuples.jl](https://github.com/MasonProtter/MutableNamedTuples.jl)

Sometimes you want a named tuple, but mutable.

[KeyedFrames.jl](https://github.com/invenia/KeyedFrames.jl)

A `KeyedFrame` is a `DataFrame` that also stores a vector of column names that together act as a unique key, which can be used to determine which columns to `join`, `unique`, and `sort` on by default.

[LayerDicts.jl](https://github.com/invenia/LayerDicts.jl)

`LayerDict` is an `Associative` type that wraps a series of other associatives (e.g. `Dict`s). When performing a lookup, a `LayerDict` will look through its associatives in the order they were passed to the constructor until it finds a match. `LayerDict`s are immutable—you cannot call `setindex!` on them. However, you can update its wrapped associatives and those changes will be reflected in future lookups.

[MultilineStrings.jl](https://github.com/invenia/MultilineStrings.jl)

Tooling for manipulating multiline strings.

[DateSelectors.jl](https://github.com/invenia/DateSelectors.jl)

DateSelectors.jl simplifies the partitioning of a collection of dates into non-contiguous validation and holdout sets in line with best practices for tuning hyper-parameters, for time-series machine learning.

[Intervals.jl](https://github.com/invenia/Intervals.jl)

Non-iterable ranges.

[AxisSets.jl](https://github.com/invenia/AxisSets.jl)

Consistent operations over a collection of KeyedArrays.

[JLSO.jl](https://github.com/invenia/JLSO.jl)

JLSO is a storage container for serialized Julia objects. Think of it less as a serialization format but as a container, that employs a serializer, and a compressor, handles all the other concerns including metadata and saving. Such that the serializer just needs to determine how to turn a julia object into a `streamVector{UInt8}`, and the compressor just needs to determine how to turn one stream of `UInt8`s into a smaller one (and the reverse).

[ObservationDims.jl](https://github.com/invenia/ObservationDims.jl)

This package defines useful traits and methods for organising data into the format required by some API.

[BlockDiagonals.jl](https://github.com/invenia/BlockDiagonals.jl)

Functionality for working efficiently with block diagonal matrices. Note that non-square blocks are allowed, similarly to scipy.block_diag, but in contrast to the mathematical definition above.

[XLSX.jl](https://github.com/felipenoris/XLSX.jl)

Excel file reader/writer for the Julia language.

[BLPData.jl](https://github.com/felipenoris/BLPData.jl)

Provides a wrapper for BLPAPI Library to the Julia language.

[SplitIterators.jl](https://github.com/felipenoris/SplitIterators.jl)

This package provides a `split` function for iterators.

[STables.jl](https://github.com/felipenoris/STables.jl)

Yet another Table / DataFrame like Julia package.

[LDAPClient.jl](https://github.com/felipenoris/LDAPClient.jl)

A Julia client for LDAP (Lightweight Directory Access Protocol) based on OpenLDAP library.

[BSONSerializer.jl](https://github.com/felipenoris/BSONSerializer.jl)

Encode/Decode your Julia structures to/from BSON.

[Ratios.jl](https://github.com/timholy/Ratios.jl)

This package provides types similar to Julia's `Rational` type, which make some sacrifices but have better computational performance at the risk of greater risk of overflow.

[NamedTupleTools.jl](https://github.com/JeffreySarnoff/NamedTupleTools.jl)

Some NamedTuple utilities.

[CardinalDicts.jl](https://github.com/JeffreySarnoff/CardinalDicts.jl)

Fast fixed-size dicts wth eraseable keyed values where keys are sequential indicies.

[JDF.jl](https://github.com/xiaodaigh/JDF.jl)

JDF is a DataFrames serialization format with the following goals: fast save and load times, compressed storage on disk, enable disk-based data manipulation, supports machine learning workloads, e.g. mini-batch, sampling.

[Douglass.jl](https://github.com/jmboehm/Douglass.jl)

Douglass.jl is a package for manipulating DataFrames in Julia using a syntax that is very similar to Stata.

[JSON2DataFrame.jl](https://github.com/jmboehm/JSON2DataFrame.jl)

This package provides a few functions to conveniently flatten information stored in JSON format and convert it to DataFrames.

### Data types: Numbers

[Infinities.jl](https://github.com/JuliaMath/Infinities.jl)

A Julia package for representing infinity in all its forms.

[FixedPointNumbers.jl](https://github.com/JuliaMath/FixedPointNumbers.jl)

This library implements fixed-point number types. A fixed-point number represents a fractional, or non-integral, number. In contrast with the more widely known floating-point numbers, with fixed-point numbers the decimal point doesn't "float": fixed-point numbers are effectively integers that are interpreted as being scaled by a constant factor. Consequently, they have a fixed number of digits (bits) after the decimal (radix) point.

[DoubleFloats.jl](https://github.com/JuliaMath/DoubleFloats.jl)

Math with 85+ accurate bits.

[FixedPointDecimals.jl](https://github.com/JuliaMath/FixedPointDecimals.jl)

Provides the fixed-point decimal type FixedDecimal allowing for exact representations of decimal numbers. These numbers are useful in financial calculations where interactions between decimal numbers are required to be exact.

[Float8s.jl](https://github.com/JuliaMath/Float8s.jl)

Finally a number type that you can count with your fingers. Super Mario and Zelda would be proud. Comes in two flavours: `Float8` has 3 exponent bits and 4 fraction bits, `Float8_4` has 4 exponent bits and 3 fraction bits. Both rely on conversion to Float32 to perform any arithmetic operation, similar to Float16.

[BFloat16s.jl](https://github.com/JuliaMath/BFloat16s.jl)

This package defines the BFloat16 data type, a floating-point format with 1 sign bit, 8 exponent bits and 7 mantissa bits. Hardware implementation of this datatype is available in Google's Cloud TPUs as well as in a growing number of CPUs, GPUs, and more specialized processors. This package is suitable to evaluate whether using BFloat16 would cause precision problems for any particular algorithm, even without access to supporting hardware. Note that this package is designed for functionality, not performance, so this package should be used for precision experiments only, not performance experiments.

[DecFP.jl](https://github.com/JuliaMath/DecFP.jl)

The DecFP package is a Julia wrapper around the Intel Decimal Floating-Point Math Library, providing a software implementation of the IEEE 754-2008 Decimal Floating-Point Arithmetic specification.

[RoundingIntegers.jl](https://github.com/JuliaMath/RoundingIntegers.jl)

RoundingIntegers defines new integer types for the Julia programming language. Rounding integers act very much like regular integers, except that you can safely assign floating-point values to them. As the name suggests, such assignments cause rounding to the nearest integer.

[Decimals.jl](https://github.com/JuliaMath/Decimals.jl)

The Decimals package provides basic data type and functions for arbitrary precision decimal floating point arithmetic in Julia. It supports addition, subtraction, negation, multiplication, division, and equality operations.

[Quadmath.jl](https://github.com/JuliaMath/Quadmath.jl)

This is a Julia interface to libquadmath, providing a Float128 type corresponding to the IEEE754 binary128 floating point format.

[RomanNumerals.jl](https://github.com/harryscholes/RomanNumerals.jl)

Julia package for Roman numerals.

---

### Data types: Arrays

[PDMats.jl](https://github.com/JuliaStats/PDMats.jl)

Uniform interface for positive definite matrices of various structures.

[PDMatsExtras.jl](https://github.com/invenia/PDMatsExtras.jl)

This is a package for extra Positive (Semi-) Definated Matrix types. It is an extension to PDMats.jl.

[NamedDims.jl](https://github.com/invenia/NamedDims.jl)

NamedDimsArray is a zero-cost abstraction to add names to the dimensions of an array.

[PooledArrays.jl](https://github.com/JuliaData/PooledArrays.jl)

A pooled representation of arrays for purposes of compression when there are few unique elements.

[CategoricalArrays.jl](https://github.com/JuliaData/CategoricalArrays.jl)

This package provides tools for working with categorical variables, both with unordered (nominal variables) and ordered categories (ordinal variables), optionally with missing values.

[InvertedIndices.jl](https://github.com/JuliaData/InvertedIndices.jl)

This very small package just exports one type: the `InvertedIndex`, or `Not` for short. It can wrap any supported index type and may be used as an index into any `AbstractArray` subtype, including OffsetArrays.

[ArrayInterface.jl](https://juliaarrays.github.io/ArrayInterface.jl/stable/)

Designs for new Base array interface primitives, used widely through scientific machine learning (SciML) and other organizations

[OffsetArrays.jl](https://juliaarrays.github.io/OffsetArrays.jl/stable/)

OffsetArrays provides Julia users with arrays that have arbitrary indices, similar to those found in some other programming languages like Fortran.

[StructsOfArrays.jl](https://github.com/JuliaArrays/StructsOfArrays.jl)

StructsOfArrays implements the classic structure of arrays optimization. The contents of a given field for all objects is stored linearly in memory, and different fields are stored in different arrays. This permits SIMD optimizations in more cases and can also save a bit of memory if the object contains padding. It is especially useful for arrays of complex numbers.

[IdentityRanges.jl](https://github.com/JuliaArrays/IdentityRanges.jl)

IdentityRanges are Julia-language a helper type for creating "views" of arrays.

[CatIndices.jl](https://github.com/JuliaArrays/CatIndices.jl)

A Julia package for concatenating, growing, and shrinking arrays in ways that allow control over the resulting axes.

[IndirectArrays.jl](https://github.com/JuliaArrays/IndirectArrays.jl)

An `IndirectArray` is one that encodes data using a combination of an `index` and a `value` table. Each element is assigned its own index, which is used to retrieve the value from the `value` table.  Concretely, if `A` is an `IndirectArray`, then `A[i,j...] = value[index[i,j,...]]`.

[MappedArrays.jl](https://github.com/JuliaArrays/MappedArrays.jl)

This package implements "lazy" in-place elementwise transformations of arrays for the Julia programming language.

[UnsafeArrays.jl](https://github.com/JuliaArrays/UnsafeArrays.jl)

UnsafeArrays provides stack-allocated pointer-based array views for Julia.

[CustomUnitRanges.jl](https://github.com/JuliaArrays/CustomUnitRanges.jl)

This Julia package supports the creation of array types with "unconventional" indices, i.e., when the indices may not start at 1.

[AxisArrays.jl](https://github.com/JuliaArrays/AxisArrays.jl)

This package for the Julia language provides an array type (the `AxisArray`) that knows about its dimension names and axis values. This allows for indexing by name without incurring any runtime overhead. This permits one to implement algorithms that are oblivious to the storage order of the underlying arrays. AxisArrays can also be indexed by the values along their axes, allowing column names or interval selections.

[ArraysOfArrays.jl](https://juliaarrays.github.io/ArraysOfArrays.jl/stable/)

A Julia package for efficient storage and handling of nested arrays. ArraysOfArrays provides two different types of nested arrays: `ArrayOfSimilarArrays` and `VectorOfArrays`.

[ElasticArrays.jl](https://github.com/JuliaArrays/ElasticArrays.jl)

ElasticArrays provides resizeable multidimensional arrays for Julia.

[TiledIteration.jl](https://github.com/JuliaArrays/TiledIteration.jl)

This Julia package handles some of the low-level details for writing cache-efficient, possibly-multithreaded code for multidimensional arrays.

[ShiftedArrays.jl](https://juliaarrays.github.io/ShiftedArrays.jl/stable/)

A `ShiftedArray` is a lazy view of an Array, shifted on some or all of his indexing dimensions by some constant values.

[StaticArrays.jl](https://juliaarrays.github.io/StaticArrays.jl/stable/)

StaticArrays provides a framework for implementing statically sized arrays in Julia, using the abstract type `StaticArray{Size,T,N} <: AbstractArray{T,N}`. 

[MetadataArrays.jl](https://github.com/JuliaArrays/MetadataArrays.jl)

Implementation of arrays with metadata.

[LazyArrays.jl](https://juliaarrays.github.io/LazyArrays.jl/dev/)

Lazy arrays and linear algebra in Julia

[BlockArrays.jl](https://juliaarrays.github.io/BlockArrays.jl/stable/)

A block array is a partition of an array into multiple blocks or subarrays, see wikipedia for a more extensive description.

[StructArrays.jl](https://juliaarrays.github.io/StructArrays.jl/stable/)

This package introduces the type `StructArray` which is an `AbstractArray` whose elements are struct (for example `NamedTuples`, or `ComplexF64`, or a custom user defined struct).

[EndpointRanges.jl](https://github.com/JuliaArrays/EndpointRanges.jl)

This Julia package makes it easier to index "unconventional" arrays (ones for which indexing does not necessarily start at 1), by defining constants `ibegin` and `iend` that stand for the beginning and end, respectively, of the indices range along any given dimension.

[InfiniteArrays.jl](https://github.com/JuliaArrays/InfiniteArrays.jl)

A Julia package for representing arrays with infinite dimension sizes, designed to work with other array types. Infinite arrays are by necessity lazy, and so this package is closely linked to `LazyArrays.jl`.

[FillArrays.jl](https://github.com/JuliaArrays/FillArrays.jl)

Julia package to lazily represent matrices filled with a single entry, as well as identity matrices. This package exports the following types: `Eye`, `Fill`, `Ones`, `Zeros`, `Trues` and `Falses`.

[LazyGrids.jl](https://github.com/JuliaArrays/LazyGrids.jl)

This Julia module exports a method ndgrid for generating lazy versions of grids from a collection of 1D vectors (any `AbstractVector` type).

[HybridArrays.jl](https://github.com/JuliaArrays/HybridArrays.jl)

Arrays with both statically and dynamically sized axes in Julia.

[SentinelArrays.jl](https://github.com/JuliaData/SentinelArrays.jl)

Array types that can use sentinel values of the element type for special values.

[AxisAlgorithms.jl](https://github.com/timholy/AxisAlgorithms.jl)

AxisAlgorithms is a collection of filtering and linear algebra algorithms for multidimensional arrays. For algorithms that would typically apply along the columns of a matrix, you can instead pick an arbitrary axis (dimension).

[WoodburyMatrices.jl](https://github.com/timholy/WoodburyMatrices.jl)

This package provides support for the Woodbury matrix identity for the Julia programming language.

[NamedAxesArrays.jl](https://github.com/timholy/NamedAxesArrays.jl)

This package (not yet functional) for the Julia language will allow you to index arrays using names for the individual axes.

[ArrayIteration.jl](https://github.com/timholy/ArrayIteration.jl)

This repository contains a candidate next-generation interface for handling arrays in the Julia programming language. Its two (interconnected) advantages are: (1) it relaxes many of the assumptions built in to Julia's current array API; (2) it employs greater indirection to create new opportunities for both performance and flexibility.

[RestrictProlong.jl](https://github.com/timholy/RestrictProlong.jl)

This package provides efficient multidimensional implementations of two operators, restrict and prolong, which feature heavily in multigrid methods. In general terms, these operations reduce and increase, respectively, the size of arrays by a factor of 2 along one or more dimensions. The two operators satisfy the "Galerkin condition," meaning that as operators they are transposes of one another.

[ArrayMeta.jl](https://github.com/shashi/ArrayMeta.jl)

This package aims to evolve the means available to express array operations at a higher level than currently possible.

[Strided.jl](https://github.com/Jutho/Strided.jl)

A Julia package for working more efficiently with strided arrays, i.e. dense arrays whose memory layout has a fixed stride along every dimension.

[CatViews.jl](https://github.com/ahwillia/CatViews.jl)

In optimization and machine learning, model parameters can be distributed across multiple arrays and can interact in complex ways. However, it can be useful to abstract away these details (e.g. when computing gradients) and collect all the parameters into a single vector. This is a lightweight package that enables you to switch between these two perspectives seamlessly.

[GroupedArrays.jl](https://github.com/FixedEffects/GroupedArrays.jl)

GroupedArray is an AbstractArray that contains positive integers or missing values.

[IndexFunArrays.jl](https://github.com/bionanoimaging/IndexFunArrays.jl)

This package allows to generate complex array expressions based on the indices.

[TiledViews.jl](https://github.com/bionanoimaging/TiledViews.jl)

Allows an AbstractArray, to look like an AbstractArray with one more dimension and the tiles are represented along this dimension.

[Multivectors.jl](https://github.com/digitaldomain/Multivectors.jl)

The Multivectors Julia package defines the Multivector Type to represent mixed-grade linear combinations of KVectors, which are in turn a vector space of Blades of a given grade. Multivectors is intended to be an implementation of Geometric Algebra, although it is useful for any Clifford algebra.

[MultiPrecisionArrays.jl](https://github.com/ctkelley/MultiPrecisionArrays.jl)

This package provides data structures and solvers for several variants of iterative refinement (IR). It will become much more useful when half precision (aka Float16) is fully supported in LAPACK/BLAS.

---

### Data types: Strings

[WeakRefStrings.jl](https://github.com/JuliaData/WeakRefStrings.jl)

A string type for minimizing data-transfer costs in Julia.

[InlineStrings.jl](https://github.com/JuliaStrings/InlineStrings.jl)

Fixed-width string types for facilitating certain string workflows in Julia.

[StringEncodings.jl](https://github.com/JuliaStrings/StringEncodings.jl)

This Julia package provides support for decoding and encoding texts between multiple character encodings.

[StringViews.jl](https://github.com/JuliaStrings/StringViews.jl)

This Julia package implements a new type of `AbstractString`, a `StringView`, that provides a string representation of any underlying array of bytes (any `AbstractVector{UInt8}`), interpreted as UTF-8 encoded Unicode data.

[NaturalSort.jl](https://github.com/JuliaStrings/NaturalSort.jl)

Natural Sort Order in Julia.

[TinySegmenter.jl](https://github.com/JuliaStrings/TinySegmenter.jl)

TinySegmenter.jl is a Julia version of TinySegmenter, which is an extremely compact Japanese tokenizer originally written in JavaScript by Mr. Taku Kudo.

[StringTemplates.jl](https://github.com/joshday/StringTemplates.jl)

Speedy customizable string interpolation for Julia.

---

### Data types: Structures

[AutoHashEquals.jl](https://github.com/JuliaServices/AutoHashEquals.jl)

A macro to add `isequal`, `==`, and `hash()` to struct types: `@auto_hash_equals`.

[AutoAccessorMethods.jl](https://github.com/harryscholes/AutoAccessorMethods.jl)

Automatically define accessor methods for fields of a type.

---

### Data types: Time series

[TimeSeries.jl](https://github.com/JuliaStats/TimeSeries.jl)

TimeSeries aims to provide a lightweight framework for working with time series data in Julia.

[TimeSeries2.jl](https://github.com/JeffreySarnoff/TimeSeries2.jl)

TimeSeries aims to provide a lightweight framework for working with time series data in Julia.

[GetColumn.jl](https://github.com/JeffreySarnoff/GetColumn.jl)

Get a column from a Vector, Matrix, 3D Array, DataFrame, or TimeSeries.

---

### Data analysis

[Impute.jl](https://github.com/invenia/Impute.jl)

Impute.jl provides various methods for handling missing data in Vectors, Matrices and Tables.

[Interpolations.jl](https://juliamath.github.io/Interpolations.jl/latest/)

This package implements a variety of interpolation schemes for the Julia language. It has the goals of ease-of-use, broad algorithmic support, and exceptional performance.

[ScatteredInterpolation.jl](https://eljungsk.github.io/ScatteredInterpolation.jl/stable/)

Interpolation of scattered data in Julia.

[GridInterpolations.jl](https://github.com/sisl/GridInterpolations.jl)

This package performs multivariate interpolation on a rectilinear grid. At the moment, it provides implementations of multilinear and simplex interpolation.

[Fuzzy.jl](https://github.com/phelipe/Fuzzy.jl)

Mamdani and Sugeno type Fuzzy Inference System in Julia.

[FFTViews.jl](https://github.com/JuliaArrays/FFTViews.jl)

A package for simplifying operations that involve Fourier transforms. An FFTView of an array uses periodic boundary conditions for indexing, and shifts all indices of the array downward by 1.

[PaddedViews.jl](https://github.com/JuliaArrays/PaddedViews.jl)

PaddedViews provides a simple wrapper type, `PaddedView`, to add "virtual" padding to any array without copying data. Edge values not specified by the array are assigned a `fillvalue`. Multiple arrays may be "promoted" to have common indices using the `paddedviews` function.

[SpatioTemporalTraits.jl](https://github.com/JuliaArrays/SpatioTemporalTraits.jl)

SpatioTemporalTraits serves as a relatively low-level source of spatiotemporal traits, allowing other packages the opportunity to use a common interface for their unique types.

[Vol.jl](https://github.com/felipenoris/Vol.jl)

Volatility models for time series.

[BS.jl](https://github.com/felipenoris/BS.jl)

Black-Scholes Option Pricing Formulae.

[InterestRates.jl](https://github.com/felipenoris/InterestRates.jl)

Tools for Term Structure of Interest Rates calculation, aimed at the valuation of financial contracts, specially Fixed Income instruments.

[Tidier.jl](https://github.com/kdpsingh/Tidier.jl)

Tidier.jl is a 100% Julia implementation of the R tidyverse mini-language in Julia. Powered by the DataFrames.jl package and Julia’s extensive meta-programming capabilities, Tidier.jl is an R user’s love letter to data analysis in Julia.

[TidierData.jl](https://github.com/TidierOrg/TidierData.jl)

TidierData.jl is a 100% Julia implementation of the dplyr and tidyr R packages. Powered by the DataFrames.jl package and Julia’s extensive meta-programming capabilities, TidierData.jl is an R user’s love letter to data analysis in Julia.

[TidierCats.jl](https://github.com/TidierOrg/TidierCats.jl)

TidierCats.jl has one main goal: to implement forcats's straightforward syntax and of ease of use while working with categorical variables for Julia users. While this package was develeoped to work seamelessly with Tidier.jl functions and macros, it can also work as a independently as a standalone package. This package is powered by CateogricalArrays.jl.

[TidierFiles.jl](https://github.com/TidierOrg/TidierFiles.jl)

TidierFiles.jl is a 100% Julia implementation of the readr, haven, readxl, and writexl R packages. Powered by the CSV.jl, XLSX.jl, ReadStatTables.jl, Arrow.jl, and Parquet2.jl packages, TidierFiles.jl aims to bring a consistent interface to the reading and writing of tabular data, including a consistent syntax to read files locally versus from the web and consistent keyword arguments across data formats.

[TidierDB.jl](https://github.com/TidierOrg/TidierDB.jl)

TiderDB.jl is a 100% Julia implementation of the dbplyr R package, and similar to Python's ibis package. The main goal of TidierDB.jl is to bring the syntax of Tidier.jl to multiple SQL backends, making it possible to analyze data directly on databases without needing to copy the entire database into memory.

[TidierDates.jl](https://github.com/TidierOrg/TidierDates.jl)

TidierDates.jl is a 100% Julia implementation of the R lubridate package. TidierDates.jl has one main goal: to implement lubridate's straightforward syntax and of ease of use while working with dates for Julia users. While this package was developed to work seamlessly with Tidier.jl functions and macros, it can also work as a independently as a standalone package. This package is powered by Dates.jl.

[TidierStrings.jl](https://github.com/TidierOrg/TidierStrings.jl)

TidierStrings.jl is a 100% Julia implementation of the R stringr package. TidierStrings.jl has one main goal: to implement stringr's straightforward syntax and of ease of use for Julia users. While this package was developed to work seamlessly with TidierData.jl functions and macros, it also works independently as a standalone package.

[TidierText.jl](https://github.com/TidierOrg/TidierText.jl)

TidierText.jl is a 100% Julia implementation of the R tidytext package. The purpose of the package is to make it easy analyze text data using DataFrames.

[TidierVest.jl](https://github.com/TidierOrg/TidierVest.jl)

This library combines HTTP, Gumbo and Cascadia for a more simple way to scrape data.

[ImputationAlgamest.jl](https://github.com/JeffreySarnoff/ImputationAlgamest.jl)

Last observation carry forward.

---

### Graphics: Graphics

[Javis.jl](https://juliaanimators.github.io/Javis.jl/stable/)

Javis.jl is a tool focused on providing an easy to use interface for making animations and developing visualizations quickly - while having fun!

[Luxor.jl](https://juliagraphics.github.io/Luxor.jl/stable/)

Luxor is a Julia package for drawing simple static vector graphics. It provides basic drawing functions and utilities for working with shapes, polygons, clipping masks, PNG and SVG images, turtle graphics, and simple animations.

[Layered.jl](https://github.com/jkrumbiegel/Layered.jl)

A library for creating 2D vector graphics in layers. Check out the documentation!

[Animations.jl](https://github.com/jkrumbiegel/Animations.jl)

Animations.jl offers an easy way to set up simple animations where multiple keyframes are interpolated between in sequence. You can choose different easing functions or create your own. Keyframe values can be anything that can be linearly interpolated, you can also add your own methods for special types. An easing can have repetitions and delays, so that looping animations are simpler to create.

[SignedDistanceFields.jl](https://github.com/JuliaGraphics/SignedDistanceFields.jl)

This package implements an algorithm for quickly computing signed distance fields in 2D.

[Immerse.jl](https://github.com/JuliaGraphics/Immerse.jl)

Immerse is a wrapper that adds graphical interactivity to Julia plots. Currently, Immerse supports Gadfly.

[UnicodeGraphics.jl](https://github.com/JuliaGraphics/UnicodeGraphics.jl)

Convert any matrix into a braille or block Unicode string, real fast and dependency free.

[FreeTypeAbstraction.jl](https://github.com/JuliaGraphics/FreeTypeAbstraction.jl)

Draw text into a Matrix.

[Layout.jl](https://github.com/timholy/Layout.jl)

This is intended for experiments with graphical layout management in Julia.

[VTKView.jl](https://github.com/j-fu/VTKView.jl)

VTKView wraps vtkfig, a C++ graphics library with an API that attempts to be easy to use from C/C++ simulation codes. It uses vtk for fast rendering of data. Based on ccall and the C API of vtkfig, VTKView makes this functionality available in Julia..

[ModelingToolkitDesigner.jl](https://github.com/bradcarman/ModelingToolkitDesigner.jl)

The ModelingToolkitDesigner.jl package is a helper tool for visualizing and editing ModelingToolkit.jl system connections.

[Fable.jl](https://github.com/leios/Fable.jl)

The Fractal Animation Engine, Fae.jl, is an open research project with the goal of creating a new, general purpose rendering engine based on Iterated Function Systems (IFS). The principle is simple: an IFS can draw any object with the right set of equations, so if we can find that set of equations, we can render dynamic scenes.

[CatmullRom.jl](https://github.com/JeffreySarnoff/CatmullRom.jl)

Centripetal parameterization for Catmull-Rom interpoint connectivity.

[SaveFigs.jl](https://github.com/harryscholes/SaveFigs.jl)

Save Plots.jl figures easily in multiple formats and sizes.

---

### Graphics: GUI

[Gtk.jl](https://github.com/JuliaGraphics/Gtk.jl)

GUI building, using the Gtk library: https://www.gtk.org/

[GtkObservables.jl](https://github.com/JuliaGizmos/GtkObservables.jl)

GtkObservables is designed to simplify the creation of graphical user interfaces (GUIs) using Gtk and Julia.

[Gtk4.jl](https://github.com/JuliaGtk/Gtk4.jl)

GUI building using the GTK library, version 4.

[Gtk4Makie.jl](https://github.com/JuliaGtk/Gtk4Makie.jl)

Interactive Makie plots in Gtk4 windows.

[mousetrap.jl](https://github.com/Clemapfel/mousetrap.jl)

Mousetrap is a GUI library for Julia. It, and its stand-alone C++-component of the same name, fully wrap GTK4 (which is written in C), vastly simplifying its interface to improve ease-of-use without sacrificing flexibility.

[GtkUtilities.jl](https://github.com/JuliaGtk/GtkUtilities.jl)

This package is a collection of extensions to Gtk that make interactive graphics easier.

[GtkSourceWidget.jl](https://github.com/JuliaGtk/GtkSourceWidget.jl)

GtkSourceWidget.jl is a Julia wrapper for the Gtk library GtkSourceView that allows showing source code documents.

[GtkMarkdownTextView.jl](https://github.com/JuliaGtk/GtkMarkdownTextView.jl)

A widget to display simple markdown formatted text.

[GtkReactive.jl](https://github.com/JuliaGizmos/GtkReactive.jl)

GtkReactive is designed to simplify the creation of graphical user interfaces (GUIs) using Gtk and Julia. 

[Tk.jl](https://github.com/JuliaGraphics/Tk.jl)

Julia interface to the Tk windowing toolkit.

[QML.jl](https://github.com/JuliaGraphics/QML.jl)

This package is maintained by the JuliaGraphics group and provides an interface to Qt6 QML (and to Qt5 for older versions). It uses the CxxWrap package to expose C++ classes. Current functionality allows interaction between QML and Julia using Observables, ListModels and function calling. There is also a generic Julia display, as well as specialized integration for image drawing, GR plots and Makie.

[Blink.jl](https://github.com/JuliaGizmos/Blink.jl)

Blink.jl is the Julia wrapper around Electron. It can serve HTML content in a local window, and allows for communication between Julia and the web page. In this way, therefore, Blink can be used as a GUI toolkit for building HTML-based applications for the desktop.

[Electron.jl](https://github.com/davidanthoff/Electron.jl)

Electron.jl wraps the cross-platform desktop application framework Electron. You can use it to build GUI applications in Julia.

[Interact.jl](https://juliagizmos.github.io/Interact.jl/latest/)

This package is a collection of web-based widgets. It works in Jupyter notebooks, Atom IDE, or as a plain old web page.

[InteractBase.jl](https://github.com/JuliaGizmos/InteractBase.jl)

This package is a developer target and it includes the implementation of Interact widgets.

[WidgetsBase.jl](https://github.com/JuliaGizmos/WidgetsBase.jl)

Unified interface for widgets.

[Escher.jl](https://github.com/JuliaGizmos/Escher.jl)

Escher has been repurposed to be a metapackage around Interact.jl and other packages for web deployment (so far it includes Mux.jl but more things may be added as they become available).

[Reactive.jl](https://github.com/JuliaGizmos/Reactive.jl)

Reactive.jl is a package for reactive programming in Julia.

[WebToys.jl](https://github.com/JuliaGizmos/WebToys.jl)

Fun toys built on top of JuliaGizmos projects!

[CImGui.jl](https://github.com/Gnimuc/CImGui.jl)

This package provides a Julia language wrapper for cimgui: a thin c-api wrapper programmatically generated for the excellent C++ immediate mode gui Dear ImGui.

---

### Graphics: Plots

[GR.jl](https://github.com/jheinen/GR.jl)

Plotting for Julia based on GR, a framework for visualization applications.

[GRUtils.jl](https://github.com/heliosdrm/GRUtils.jl)

This package is an experimental refactoring of the module jlgr from GR.jl, intended to provide the main utilities of jlgr in a more "Julian" and modular style, easy to read, and facilitate code contributions by others.

[Plots.jl](https://github.com/JuliaPlots/PlotDocs.jl)

Plots - powerful convenience for visualization in Julia.

[ColorSchemes.jl](https://juliagraphics.github.io/ColorSchemes.jl/stable/)

This package provides a collection of colorschemes.

[ColorSchemeTools.jl](https://github.com/JuliaGraphics/ColorSchemeTools.jl)

This package provides tools for working with colorschemes and colormaps. It's a companion to the ColorSchemes.jl package.

[Colors.jl](https://github.com/JuliaGraphics/Colors.jl)

This library provides a wide array of functions for dealing with color. This includes conversion between colorspaces, measuring distance between colors, simulating color blindness, parsing colors, and generating color scales for graphics.

[NamedColors.jl](https://github.com/JuliaGraphics/NamedColors.jl)

Colors.jl supports about 660 colors as named colorants. NamedColors.jl supports about 4,000. Honestly, the named colors in Colors.jl, and/or its capacity to generate good palettes, are far more useful.

[ColorTypes.jl](https://github.com/JuliaGraphics/ColorTypes.jl)

This "minimalistic" package serves as the foundation for working with colors in Julia. It defines basic color types and their constructors, and sets up traits and `show` methods to make them easier to work with.

[ColorVectorSpace.jl](https://github.com/JuliaGraphics/ColorVectorSpace.jl)

This package is an add-on to ColorTypes, and provides fast mathematical operations for objects with types such as `RGB` and `Gray`. Specifically, with this package both grayscale and `RGB` colors are treated as if they are points in a normed vector space.

[PlotThemes.jl](https://github.com/JuliaPlots/PlotThemes.jl)

PlotThemes is a package to spice up the plots made with Plots.jl.

[PlotUtils.jl](https://github.com/JuliaPlots/PlotUtils.jl)

Generic helper algorithms for building plotting components.

[UnicodePlots.jl](https://github.com/JuliaPlots/UnicodePlots.jl)

Advanced Unicode plotting library designed for use in Julia's REPL.

[Makie.jl](https://github.com/MakieOrg/Makie.jl)

Makie is a data visualization ecosystem for the Julia programming language, with high performance and extensibility.

[MakieThemes.jl](https://github.com/MakieOrg/MakieThemes.jl)

The idea of this package is to create a collection of themes for Makie to customize the size and look of plot elements and colors.

[MakieTeX.jl](https://github.com/JuliaPlots/MakieTeX.jl)

MakieTeX allows you to draw and visualize arbitrary TeX documents in Makie! You can insert anything from a single line of math to a large and complex TikZ diagram.

[RPRMakie.jl](https://makie.juliaplots.org/stable/documentation/backends/rprmakie/)

Experimental ray tracing backend using AMDs RadeonProRender. While it's created by AMD and tailored to Radeon GPUs, it still works just as well for NVidia and Intel GPUs using OpenCL. It also works on the CPU and even has a hybrid modus to use GPUs and CPUs in tandem to render images.

[Biplots.jl](https://github.com/MakieOrg/Biplots.jl)

Biplot recipes in 2D and 3D for Makie.jl.

[TopoPlots.jl](https://github.com/MakieOrg/TopoPlots.jl)

A package for creating topoplots from data that were measured on arbitrarily positioned sensors.

[GraphMakie.jl](https://github.com/MakieOrg/GraphMakie.jl)

This Package consists of two parts: a plot recipe for graphs types from Graphs.jl and some helper functions to add interactions to those plots.

[OSMMakie.jl](https://github.com/MakieOrg/OSMMakie.jl)

A Makie recipe for plotting OpenStreetMap data. It makes heavy use of the GraphMakie package and extends it to work with the specific features of an OSMGraph.

[GeoMakie.jl](https://geo.makie.org/stable/)

GeoMakie.jl is a Julia package for plotting geospatial data on a given map projection.

[AlgebraOfGraphics.jl](https://juliaplots.org/AlgebraOfGraphics.jl/stable/)

AlgebraOfGraphics defines a language for data visualization. It is based on a few simple building blocks that can be combined using `+` and `*`.

[Graphics.jl](https://github.com/JuliaGraphics/Graphics.jl)

Graphics.jl is an abstraction layer for graphical operations in Julia.

[Winston.jl](https://winston.readthedocs.io/en/latest/#)

Winston: 2D Plotting for Julia.

[Gaston.jl](https://mbaz.github.io/Gaston.jl/stable/)

Gaston is a Julia package for plotting. It provides an interface to gnuplot, a mature, powerful, and actively developed plotting package available on all major platforms.

[PlotlyJS.jl](https://juliaplots.org/PlotlyJS.jl/stable/)

This package does not interact with the Plotly web API, but rather leverages the underlying javascript library to construct plotly graphics using all local resources. This means you do not need a Plotly account or an internet connection to use this package.

[Gnuplot.jl](https://docs.juliahub.com/Gnuplot/4mcTU/1.3.0/)

The Gnuplot.jl package allows easy and fast use of gnuplot as a data visualization tool in Julia.

[VennEuler.jl](https://github.com/JuliaPlots/VennEuler.jl)

Generate area-proportional Venn/Euler diagrams in Julia. This is based, in part, on algorithms from Leland Wilkinson.

[PairPlots.jl](https://github.com/sefffal/PairPlots.jl)

This package produces corner plots, otherwise known as pair plots or scatter plot matrices: grids of 1D and 2D histograms that allow you to visualize high dimensional data.

[Gadfly.jl](https://gadflyjl.org/stable/)

Gadfly is a system for plotting and visualization written in Julia. It is based largely on Hadley Wickhams's ggplot2 for R and Leland Wilkinson's book The Grammar of Graphics.

[Compose.jl](https://giovineitalia.github.io/Compose.jl/latest/)

Compose is a declarative vector graphics system written in Julia. It's designed to simplify the creation of complex graphics and serves as the basis of the Gadfly data visualization package.

[Compose3D.jl](https://github.com/rohitvarkey/Compose3D.jl)

Compose3D is a Julia package written to try and extend Compose to 3-D.

[PythonPlot.jl](https://github.com/stevengj/PythonPlot.jl)

This module provides a Julia interface to the Matplotlib plotting library from Python, and specifically to the `matplotlib.pyplot` module.

[CMPlot.jl](https://github.com/g-insana/CMPlot.jl)

Cloudy Mountain Plot in Julia. An informative RDI categorical distribution plot inspired by Violin, Bean and Pirate Plots. (RDI = Raw data + Descriptive statistics + Inferential statistics)

[Isoband.jl](https://github.com/jkrumbiegel/Isoband.jl)

Isoband.jl wraps isoband_jll, which gives access to wilkelab's isoband package, which powers contour plots in ggplot.

[PlotShapefiles.jl](https://github.com/Wedg/PlotShapefiles.jl)

The package started by copying the method shared by Keno Fischer [here](http://nbviewer.jupyter.org/github/JuliaX/iap2014/blob/master/GeoData.ipynb) and then extended to catering for each of the different types of shapefile (polygons, polylines, points, multipoints and their variants but does not currently support the Multipatch type) as well as overlaying the image onto a Google map.

[TidierPlots.jl](https://github.com/TidierOrg/TidierPlots.jl)

TidierPlots.jl is a 100% Julia implementation of the R package ggplot in Julia. Powered by the AlgebraOfGraphics.jl, Makie.jl, and Julia’s extensive meta-programming capabilities, TidierPlots.jl is an R user’s love letter to data visualization in Julia.

[ImPlot.jl](https://github.com/wsphillips/ImPlot.jl)

Plotting extension library that can be used in conjunction with CImGui.jl to provide enhanced immediate-mode data visualization.

[InteractiveViz.jl](https://github.com/org-arl/InteractiveViz.jl)

Interactive visualization tools for Julia.

[PlotAxes.jl](https://github.com/haberdashPI/PlotAxes.jl)

PlotAxes is intended to simplify the visualization of medium dimensional data (e.g. 4-5 dimensions max) during an interactive session. It is not intended as a full fledged plotting API for publication quality graphs.

[InspectDR.jl](https://github.com/ma-laforge/InspectDR.jl)

Fast, interactive Julia/GTK+ plots (+Smith charts +Gtk widget +Cairo-only images).

[GracePlot.jl](https://github.com/ma-laforge/GracePlot.jl)

The GracePlot.jl module is a simple control interface for Grace/xmgrace - providing more publication-quality plotting facilities to Julia.

[CMDimData.jl](https://github.com/ma-laforge/CMDimData.jl)

CMDimData.jl provides a high-level abstraction to manipulate multi-dimensional data, and automatically interpolate intermediate values as if it was a continuous function.

[Gattino.jl](https://github.com/ChifiSource/Gattino.jl)

Elegant data visualization for Julia.

[Heatmap.jl](https://github.com/Mattriks/Heatmap.jl)

A Julia package for plotting Heatmaps with marginal dendrograms, in Gadfly.

[PlotlyX.jl](https://github.com/joshday/PlotlyX.jl)

Experimental rewrite of PlotlyLight.jl.

---

### Graphics: Graphs

[Graphs.jl](https://juliagraphs.org/Graphs.jl/dev/)

The goal of Graphs.jl is to offer a performant platform for network and graph analysis in Julia, following the example of libraries such as NetworkX in Python.

[GraphRecipes.jl](https://docs.juliaplots.org/stable/GraphRecipes/introduction/)

GraphRecipes is a collection of recipes for visualizing graphs. Users specify a graph through an adjacency matrix, an adjacency list, or an AbstractGraph via Graphs. GraphRecipes will then use a layout algorithm to produce a visualization of the graph that the user passed.

[NetworkLayout.jl](https://juliagraphs.org/NetworkLayout.jl/stable/)

Layout algorithms for graphs and trees in pure Julia.

[SimpleWeightedGraphs.jl](https://github.com/JuliaGraphs/SimpleWeightedGraphs.jl)

Edge-Weighted Graphs for Graphs.jl.

[MatrixNetworks.jl](https://github.com/JuliaGraphs/MatrixNetworks.jl)

This package consists of a collection of network algorithms. In short, the major difference between MatrixNetworks.jl and packages like LightGraphs.jl or Graphs.jl is the way graphs are treated.

[MultilayerGraphs.jl](https://juliagraphs.org/MultilayerGraphs.jl/stable/)

MultilayerGraphs.jl is a Julia package for the construction, manipulation and analysis of multilayer graphs extending Graphs.jl.

[GraphPlot.jl](https://github.com/JuliaGraphs/GraphPlot.jl)

Graph layout and visualization algorithms based on Compose.jl and inspired by GraphLayout.jl.

[MetaGraphs.jl](https://juliagraphs.org/MetaGraphs.jl/dev/)

MetaGraphs.jl graphs with arbitrary metadata.

[GraphIO.jl](https://github.com/JuliaGraphs/GraphIO.jl)

GraphIO provides support to Graphs.jl for reading/writing graphs in various formats.

[StaticGraphs.jl](https://github.com/JuliaGraphs/StaticGraphs.jl)

Memory-efficient, performant graph structures optimized for large networks.

[GraphViz.jl](https://github.com/JuliaGraphs/GraphViz.jl)

This package provides an interface to the the GraphViz package for graph visualization.

[GraphsFlows.jl](https://juliagraphs.org/GraphsFlows.jl/dev/)

Flow algorithms on top of Graphs.jl, including maximum_flow, multiroute_flow and mincost_flow.

[GraphDataFrameBridge.jl](https://github.com/JuliaGraphs/GraphDataFrameBridge.jl)

Tools for interoperability between DataFrame objects and LightGraphs and MetaGraphs objects.

[CommunityDetection.jl](https://github.com/JuliaGraphs/CommunityDetection.jl)

Implements community detection for Graphs.jl. Both Nonbacktracking and Bethe Hessian detection are supported.

[GraphsMatching.jl](https://github.com/JuliaGraphs/GraphsMatching.jl)

Matching algorithms on top of Graphs.jl.

[GraphsExtras.jl](https://github.com/JuliaGraphs/GraphsExtras.jl)

Extra functionality for Graphs.jl.

[LightGraphs.jl](https://github.com/sbromberger/LightGraphs.jl)

LightGraphs offers both (a) a set of simple, concrete graph implementations -- Graph (for undirected graphs) and DiGraph (for directed graphs), and (b) an API for the development of more sophisticated graph implementations under the AbstractGraph type.

[LightGraphsExtras.jl](https://github.com/JuliaGraphs/LightGraphsExtras.jl)

Extra functionality for Graphs.

[VegaGraphs.jl](https://github.com/JuliaGraphs/VegaGraphs.jl)

VegaGraphs implements graph visualization with Vega-Lite.

[SpecialGraphs.jl](https://github.com/JuliaGraphs/SpecialGraphs.jl)

Encoding special graph structures in types.

[Cliquing.jl](https://github.com/invenia/Cliquing.jl)

Algorithms for finding a non-overlapping set of cliques in a graph represented by an adjacency matrix.

[D3Trees.jl](https://github.com/sisl/D3Trees.jl)

Flexible interactive visualization for large trees using D3.js.

---

### Graphics: TeX

[LaTeXStrings.jl](https://github.com/stevengj/LaTeXStrings.jl)

This is a small package to make it easier to type LaTeX equations in string literals in the Julia language, written by Steven G. Johnson.

[MathTeXEngine.jl](https://github.com/Kolaru/MathTeXEngine.jl)

This is a work in progress package aimed at providing a pure Julia engine for LaTeX math mode. It is composed of two main parts: a LaTeX parser and a LaTeX engine, both only for LaTeX math mode.

[TikzPictures.jl](https://github.com/JuliaTeX/TikzPictures.jl)

This library allows one to create Tikz pictures and save in various formats. It integrates with IJulia, outputting SVG images to the notebook.

[TreeView.jl](https://github.com/JuliaTeX/TreeView.jl)

This is a small package to visualize a graph corresponding to an abstract syntax tree (AST) of a Julia expression. It uses the TikzGraphs.jl package to do the visualization.

[PGFPlots.jl](https://github.com/JuliaTeX/PGFPlots.jl)

This library uses the LaTeX package pgfplots to produce plots. It integrates with IJulia, outputting SVG images to the notebook.

[TikzGraphs.jl](https://github.com/JuliaTeX/TikzGraphs.jl)

This library generates graph layouts using the TikZ graph layout package.

[TikzCDs.jl](https://github.com/JuliaTeX/TikzCDs.jl)

A wrapper around TikzPictures.jl for easier drawing of commutative diagrams using `tikz-cd`.

[BibTeX.jl](https://github.com/JuliaTeX/BibTeX.jl)

Parsing BibTeX files for the Julia language.

[Latexify.jl](https://github.com/korsbo/Latexify.jl)

Convert julia objects to LaTeX equations, arrays or other environments. 

[Convex.jl](https://github.com/jump-dev/Convex.jl)

Convex.jl is a Julia package for Disciplined Convex Programming (DCP). Convex.jl can solve linear programs, mixed-integer linear programs, and DCP-compliant convex programs using a variety of solvers, including Mosek, Gurobi, ECOS, SCS, and GLPK, through MathOptInterface. Convex.jl also supports optimization with complex variables and coefficients.

---

### Mathematics

[Interpolations.jl](https://github.com/JuliaMath/Interpolations.jl)

This package implements a variety of interpolation schemes for the Julia language. It has the goals of ease-of-use, broad algorithmic support, and exceptional performance.

[Polynomials.jl](https://github.com/JuliaMath/Polynomials.jl)

Basic arithmetic, integration, differentiation, evaluation, root finding, and fitting for univariate polynomials in Julia.

[IntervalSets.jl](https://github.com/JuliaMath/IntervalSets.jl)

Interval Sets for Julia.

[Bessels.jl](https://github.com/JuliaMath/Bessels.jl)

Numerical routines for computing Bessel, Airy, and Hankel functions for real arguments. These routines are written in the Julia programming language and are self contained without any external dependencies.

[Roots.jl](https://github.com/JuliaMath/Roots.jl)

This package contains simple routines for finding roots, or zeros, of scalar functions of a single real variable using floating-point math.

[Richardson.jl](https://github.com/JuliaMath/Richardson.jl)

The Richardson package provides a function extrapolate that extrapolates any function f(x) to f(x0), evaluating f only at a geometric sequence of points > x0 (or optionally < x0) or at a given sequence of points. f(x) can return scalars, vectors, or any type implementing a normed vector space.

[Combinatorics.jl](https://github.com/JuliaMath/Combinatorics.jl)

A combinatorics library for Julia, focusing mostly (as of now) on enumerative combinatorics and permutations. As overflows are expected even for low values, most of the functions always return BigInt, and are marked as such below.

[IntegerMathUtils.jl](https://github.com/JuliaMath/IntegerMathUtils.jl)

This library adds several functions useful for doing math on integers. Most of these are GMP wrappers that may have faster implimentations for smaller integer types.

[FastChebInterp.jl](https://github.com/JuliaMath/FastChebInterp.jl)

Fast multidimensional Chebyshev interpolation on a hypercube (Cartesian-product) domain, using a separable (tensor-product) grid of Chebyshev interpolation points, as well as Chebyshev regression (least-square fits) from an arbitrary set of points. In both cases we support arbitrary dimensionality, complex and vector-valued functions, and fast derivative and Jacobian computation.

[HCubature.jl](https://github.com/JuliaMath/HCubature.jl)

The HCubature module is a pure-Julia implementation of multidimensional "h-adaptive" integration.

[HypergeometricFunctions.jl](https://github.com/JuliaMath/HypergeometricFunctions.jl)

This package provides an implementation of the generalized hypergeometric function pFq(α, β, z).

[Tau.jl](https://github.com/JuliaMath/Tau.jl)

This Julia package defines the Tau constant and related functions.

[DensityInterface.jl](https://github.com/JuliaMath/DensityInterface.jl)

This package defines an interface for mathematical/statistical densities and objects associated with a density in Julia.

[TensorCore.jl](https://github.com/JuliaMath/TensorCore.jl)

This package is intended as a lightweight foundation for tensor operations across the Julia ecosystem. 

[ChangesOfVariables.jl](https://github.com/JuliaMath/ChangesOfVariables.jl)

ChangesOfVariables.jl defines functionality to calculate volume element changes for functions that perform a change of variables (like coordinate transformations).

[NaNMath.jl](https://github.com/JuliaMath/NaNMath.jl)

Implementations of basic math functions which return NaN instead of throwing a DomainError.

[Xsum.jl](https://github.com/JuliaMath/Xsum.jl)

The Xsum package is a Julia wrapper around Radford Neal's xsum package for exactly rounded double-precision floating-point summation.

[Hadamard.jl](https://github.com/JuliaMath/Hadamard.jl)

This package provides functions to compute fast Walsh-Hadamard transforms in Julia, for arbitrary dimensions and arbitrary power-of-two transform sizes, with the three standard orderings: natural (Hadamard), dyadic (Paley), and sequency (Walsh) ordering.

[FastPow.jl](https://github.com/JuliaMath/FastPow.jl)

This package provides a macro `@fastpow` that can speed up the computation of integer powers in any Julia expression by transforming them into optimal sequences of multiplications, with a slight sacrifice in accuracy compared to Julia's built-in x^n function. It also optimizes powers of the form `1^p`, `(-1)^p`, `2^p`, and `10^p`.

[GSL.jl](https://github.com/JuliaMath/GSL.jl)

Julia wrapper for the GNU Scientific Library (GSL), for Julia v1.0+. Currently uses GSL v2.7.

[InverseLaplace.jl](https://github.com/JuliaMath/InverseLaplace.jl)

This package implements some numerical methods for computing inverse Laplace transforms in Julia.

[FunctionAccuracyTests.jl](https://github.com/JuliaMath/FunctionAccuracyTests.jl)

ULP testing for Floating Point special functions.

[KahanSummation.jl](https://github.com/JuliaMath/KahanSummation.jl)

This package provides variants of `sum` and `cumsum`, called `sum_kbn` and `cumsum_kbn` respectively, using the Kahan-Babuska-Neumaier (KBN) algorithm for additional precision. These functions are typically slower and less memory efficient than `sum` and `cumsum`.

[LambertW.jl](https://github.com/JuliaMath/LambertW.jl)

Lambert W function and associated omega constant.

[MittagLeffler.jl](https://github.com/JuliaMath/MittagLeffler.jl)

Mittag-Leffler function.

[Sobol.jl](https://github.com/JuliaMath/Sobol.jl)

This module provides a free Julia-language Sobol low-discrepancy-sequence (LDS) implementation. This generates "quasi-random" sequences of points in N dimensions which are equally distributed over an N-dimensional hypercube.

[ChangePrecision.jl](https://github.com/JuliaMath/ChangePrecision.jl)

This package makes it easy to change the "default" precision of a large body of Julia code, simply by prefixing it with the `@changeprecision T expression` macro.

[Cubature.jl](https://github.com/JuliaMath/Cubature.jl)

This module provides one- and multi-dimensional adaptive integration routines for the Julia language, including support for vector-valued integrands and facilitation of parallel evaluation of integrands, based on the Cubature Package by Steven G. Johnson.

[ILog2.jl](https://github.com/JuliaMath/ILog2.jl)

This package provides a fast implementation of the integer-valued, base-2 logarithm. `ilog2` supports other types of numbers, as well. It also provides `checkispow2`.

[CheckedArithmetic.jl](https://github.com/JuliaMath/CheckedArithmetic.jl)

This package aims to make it easier to detect overflow in numeric computations. It exports two macros, `@check` and `@checked`, as well as functions `accumulatortype` and `acc`. Packages can add support for their own types to interact appropriately with these tools.

[AccurateArithmetic.jl](https://github.com/JuliaMath/AccurateArithmetic.jl)

Floating point math with error-free, faithful, and compensated transforms.

[JuMP.jl](https://github.com/jump-dev/JuMP.jl)

JuMP is a domain-specific modeling language for mathematical optimization embedded in Julia.

[Calculus.jl](https://github.com/JuliaMath/Calculus.jl)

The Calculus package provides tools for working with the basic calculus operations of differentiation and integration. You can use the Calculus package to produce approximate derivatives by several forms of finite differencing or to produce exact derivative using symbolic differentiation. You can also compute definite integrals by different numerical methods.

[QuadGK.jl](https://github.com/JuliaMath/QuadGK.jl)

This package provides support for one-dimensional numerical integration in Julia using adaptive Gauss-Kronrod quadrature.

[MeasureBase.jl](https://github.com/JuliaMath/MeasureBase.jl)

This (relatively) light-weight package contains core functionality of MeasureTheory. Many packages using or defining measures do not need the full capabilities of MeasureTheory.jl itself, and can depend on this instead.

[MeasureTheory.jl](https://github.com/JuliaMath/MeasureTheory.jl)

MeasureTheory.jl is a package for building and reasoning about measures.

[InverseFunctions.jl](https://github.com/JuliaMath/InverseFunctions.jl)

[Primes.jl](https://github.com/JuliaMath/Primes.jl)

Julia functions for computing prime numbers.

[AbstractFFTs.jl](https://github.com/JuliaMath/AbstractFFTs.jl)

A general framework for fast Fourier transforms (FFTs) in Julia.

[FFTW.jl](https://github.com/JuliaMath/FFTW.jl)

This package provides Julia bindings to the FFTW library for fast Fourier transforms (FFTs), as well as functionality useful for signal processing. These functions were formerly a part of Base Julia.

[FunctionZeros.jl](https://github.com/JuliaMath/FunctionZeros.jl)

This module provides a function to compute the zeros of the Bessel J and K functions, that is Bessel functions of the first and second kind.

[RandomMatrices.jl](https://github.com/JuliaMath/RandomMatrices.jl)

Random matrix package for Julia. This extends the Distributions package to provide methods for working with matrix-valued random variables, a.k.a. random matrices. State of the art methods for computing random matrix samples and their associated distributions are provided.

[RandomizedLinAlg.jl](https://github.com/JuliaLinearAlgebra/RandomizedLinAlg.jl)

RandomizedLinAlg.jl is a Julia package that provides randomized algorithms for numerical linear algebra.

[IterativeSolvers.jl](https://github.com/JuliaLinearAlgebra/IterativeSolvers.jl)

IterativeSolvers is a Julia package that provides iterative algorithms for solving linear systems, eigensystems, and singular value problems.

[SpecialFunctions.jl](https://github.com/JuliaMath/SpecialFunctions.jl)

Special mathematical functions in Julia, include Bessel, Hankel, Airy, error, Dawson, exponential (or sine and cosine) integrals, eta, zeta, digamma, inverse digamma, trigamma, and polygamma functions. Most of these functions were formerly part of Base in early versions of Julia.

[Combinatorics.jl](https://juliamath.github.io/Combinatorics.jl/dev/)

A combinatorics library for Julia, focusing mostly (as of now) on enumerative combinatorics and permutations. As overflows are expected even for low values, most of the functions always return BigInt, and are marked as such below.

[DataStructures.jl](https://juliacollections.github.io/DataStructures.jl/latest/)

This package implements a variety of data structures.

[SpecialMatrices.jl](https://github.com/JuliaMatrices/SpecialMatrices.jl)

This Julia package extends the LinearAlgebra library with support for special matrices that are used in linear algebra.

[ToeplitzMatrices.jl](https://github.com/JuliaMatrices/ToeplitzMatrices.jl)

Fast matrix multiplication and division for Toeplitz, Hankel and circulant matrices in Julia.

[BlockSparseMatrices.jl](https://github.com/KristofferC/BlockSparseMatrices.jl)

Blocked Sparse Matrices in Julia.

[Nemo.jl](https://nemocas.github.io/Nemo.jl/latest/)

Nemo is a computer algebra package for the Julia programming language.

[FastTransforms.jl](https://juliaapproximation.github.io/FastTransforms.jl/stable/)

FastTransforms.jl allows the user to conveniently work with orthogonal polynomials with degrees well into the millions.

[Polynomials.jl](https://juliamath.github.io/Polynomials.jl/stable/)

Polynomials.jl is a Julia package that provides basic arithmetic, integration, differentiation, evaluation, and root finding for univariate polynomials.

[RealDot.jl](https://github.com/JuliaMath/RealDot.jl)

This package only contains and exports a single function `realdot(x, y)`. It computes `real(LinearAlgebra.dot(x, y))` while avoiding computing the imaginary part of `LinearAlgebra.dot(x, y)` if possible.

[IntelVectorMath.jl](https://github.com/JuliaMath/IntelVectorMath.jl)

This package provides bindings to the Intel MKL Vector Mathematics Functions. This is often substantially faster than broadcasting Julia's built-in functions, especially when applying a transcendental function over a large array.

[IrrationalConstants.jl](https://github.com/JuliaMath/IrrationalConstants.jl)

This package defines irrational constants.

[NFFT.jl](https://github.com/JuliaMath/NFFT.jl)

Julia implementation of the Non-equidistant Fast Fourier Transform (NFFT).

[NFFT3.jl](https://nfft.github.io/NFFT3.jl/stable/)

The nonequispaced fast Fourier transform or NFFT, see [Keiner, Kunis, Potts, 2006] and [Plonka, Potts, Steidl, Tasche, 2018], overcomes one of the main shortcomings of the FFT - the need for an equispaced sampling grid.

[Measurements.jl](https://juliaphysics.github.io/Measurements.jl/stable/)

Measurements.jl relieves you from the hassle of propagating uncertainties coming from physical measurements, when performing mathematical operations involving them. The linear error propagation theory is employed to propagate the errors.

[SingularSpectrumAnalysis.jl](https://github.com/baggepinnen/SingularSpectrumAnalysis.jl)

A package for performing [Singular Spectrum Analysis (SSA)](https://en.wikipedia.org/wiki/Singular_spectrum_analysis).

[Symbolics.jl](https://symbolics.juliasymbolics.org/stable/)

Symbolics.jl is a fast and modern Computer Algebra System (CAS) for a fast and modern programming language (Julia). The goal is to have a high-performance and parallelized symbolic algebra system that is directly extendable in the same language as the users.

[SymbolicUtils.jl](https://github.com/JuliaSymbolics/SymbolicUtils.jl)

SymbolicUtils.jl provides various utilities for symbolic computing. SymbolicUtils.jl is what one would use to build a Computer Algebra System (CAS). If you're looking for a complete CAS, similar to SymPy or Mathematica, see Symbolics.jl. If you want to build a crazy CAS for your weird Octonian algebras, you've come to the right place.

[SymbolicSAT.jl](https://github.com/JuliaSymbolics/SymbolicSAT.jl)

This package extends SymbolicUtils expression simplification with a theorem prover.

[Metatheory.jl](https://github.com/JuliaSymbolics/Metatheory.jl)

Metatheory.jl is a general purpose term rewriting, metaprogramming and algebraic computation library for the Julia programming language, designed to take advantage of the powerful reflection capabilities to bridge the gap between symbolic mathematics, abstract interpretation, equational reasoning, optimization, composable compiler transforms, and advanced homoiconic pattern matching features. The core features of Metatheory.jl are a powerful rewrite rule definition language, a vast library of functional combinators for classical term rewriting and an e-graph rewriting, a fresh approach to term rewriting achieved through an equality saturation algorithm. Metatheory.jl can manipulate any kind of Julia symbolic expression type, as long as it satisfies the TermInterface.jl.

[TermInterface.jl](https://github.com/JuliaSymbolics/TermInterface.jl)

This package contains definitions for common functions that are useful for symbolic expression manipulation. Its purpose is to provide a shared interface between various symbolic programming Julia packages, for example SymbolicUtils.jl, Symbolics.jl and Metatheory.jl.

[Rewriters.jl](https://github.com/JuliaSymbolics/Rewriters.jl)

This package contains definitions for common combinators that are useful for symbolic expression rewriting. Its purpose is to provide a shared library of combinators between various symbolic programming Julia packages, for example SymbolicUtils.jl, Symbolics.jl and Metatheory.jl.

[Maxima.jl](https://github.com/nsmith5/Maxima.jl)

Maxima.jl is a Julia package for performing symbolic computations using Maxima. Maxima is computer algebra software that provides a free and open source alternative to proprietary software such as Mathematica, Maple and others.

[NLsolve.jl](https://github.com/JuliaNLSolvers/NLsolve.jl)

Solving non-linear systems of equations in Julia.

[Roots.jl](https://docs.juliahub.com/Roots/o0Xsi/2.0.0/)

Roots is a Julia package for finding zeros of continuous scalar functions of a single real variable.

[RowEchelon.jl](https://github.com/blegat/RowEchelon.jl)

This small package contains the functions `rref` and `rref!`.

[NumericExtensions.jl](https://github.com/lindahua/NumericExtensions.jl)

Julia extensions to provide high performance computational support.

[HyperCubicRoots.jl](https://github.com/jd-foster/HyperCubicRoots.jl)

Julia implementation of the real cubic root finding method described in [https://www.jstor.org/stable/27821778](https://www.jstor.org/stable/27821778).

[RealPolynomialRoots.jl](https://github.com/jverzani/RealPolynomialRoots.jl)

A package to find isolating intervals for the real roots of a square free polynomial.

[RootSystems.jl](https://github.com/tkluck/RootSystems.jl)

A simple library that contains implementations of the irreducible root systems.

[RootsUFPB.jl](https://github.com/eduardovegas/RootsUFPB.jl)

Package that implements some root finding methods in Julia.

[RootsByDistribution.jl](https://github.com/cbilz/RootsByDistribution.jl)

A Julia package for finding roots of a continuous function when the approximate distribution of the roots is known.

[RootSolvers.jl](https://clima.github.io/RootSolvers.jl/dev/)

A simple GPU-capable root solver package.

[SimpleRoots.jl](https://rafaqz.github.io/SimpleRoots.jl/stable/)

A light-weight collection of simple root-finding algorithms.

[RootsAndPoles.jl](https://github.com/fgasdia/RootsAndPoles.jl)

Global complex Roots and Poles Finding in Julia.

[FastPolynomialRoots.jl](https://github.com/andreasnoack/FastPolynomialRoots.jl)

This package is a Julia wrapper of the Fortran programs accompanying Fast and Backward Stable Computation of Roots of Polynomials by Jared L. Aurentz, Thomas Mach, Raf Vandebril and David S. Watkins.

[RationalRoots.jl](https://github.com/Jutho/RationalRoots.jl)

This package provides a data type `RationalRoot{T<:Integer}` to exactly represent the (positive or negative) square root of a rational number of type `Rational{T}`.

[PolynomialRoots.jl](https://github.com/giordano/PolynomialRoots.jl)

PolynomialRoots.jl is a library for finding roots of complex univariate polynomials, written in Julia.

[GeometryPrimitives.jl](https://github.com/stevengj/GeometryPrimitives.jl)

This package provides a set of geometric primitive types (balls, cuboids, cylinders, and so on) and operations on them designed to enable piecewise definition of functions, especially for finite-difference and finite-element simulations, in the Julia language.

[FastChebInterp.jl](https://github.com/stevengj/FastChebInterp.jl)

Fast multidimensional Chebyshev interpolation on a hypercube (Cartesian-product) domain, using a separable (tensor-product) grid of Chebyshev interpolation points, as well as Chebyshev regression (least-square fits) from an arbitrary set of points. In both cases we support arbitrary dimensionality, complex and vector-valued functions, and fast derivative and Jacobian computation.

[Sobol.jl](https://github.com/stevengj/Sobol.jl)

This module provides a free Julia-language Sobol low-discrepancy-sequence (LDS) implementation. This generates "quasi-random" sequences of points in N dimensions which are equally distributed over an N-dimensional hypercube.

[Simpson.jl](https://codeberg.org/AdamWysokinski/Simpson.jl)

Simpson.jl is a Julia package to integrate y(x) using samples and the composite Simpson's rule.

[Ferrite.jl](https://ferrite-fem.github.io/Ferrite.jl/stable/)

Ferrite is a finite element toolbox that provides functionalities to implement finite element analysis in Julia. The aim is to be general and to keep mathematical abstractions.

[CALFEM.jl](https://github.com/KristofferC/CALFEM.jl)

CALFEM.jl is an API port of the simple Matlab FE toolbox CALFEM written in Julia. The purpose of this package is to ease the transition for people who want to try out Julia for FE-analysis. CALFEM.jl is built on top of Ferrite.

[Completion.jl](https://github.com/HarrisonGrodin/Completion.jl)

Knuth-Bendix completion algorithm.

[SpecialSets.jl](https://github.com/HarrisonGrodin/SpecialSets.jl)

SpecialSets provides implementations of sets commonly used in mathematics, as well as the logic for cleanly combining such sets.

[Simplify.jl](https://github.com/HarrisonGrodin/Simplify.jl)

Simplify.jl implements methods for symbolic algebraic simplification in the Julia language.

[Conjugates.jl](https://github.com/MasonProtter/Conjugates.jl)

Conjugates.jl is a simple little utility for doing algebraic operations between an object and its ‘conjugate’, either Hermitian conjugate or transpose.

[MatrixProductStates.jl](https://github.com/MasonProtter/MatrixProductStates.jl)

This is a package-in-progress in which I am implementing the DMRG algorithm over matrix product states as explained in Schollwöck’s The density-matrix renormalization group in the age of matrix product states. A similar project has been undertaken in LatticeSweeper.jl.

[LatticeSweeper.jl](https://github.com/0/LatticeSweeper.jl)

Simple two-site DMRG. No support for quantum number conservation.

[Symbolics.jl](https://github.com/MasonProtter/Symbolics.jl)

This is a package I'm throwing together after getting inspired by the talk Physics in Clojure which was about porting scmutils to clojure. scmutils is a Scheme package with a very interesting and powerful computer algebra system meant as a companion to the book Structure and Interpretation of Classical Mechanics.

[SymbolicTracing.jl](https://github.com/MasonProtter/SymbolicTracing.jl)

Experimental tracing for SymbolicUtils.jl types. SymbolicTracing.jl will allow you to effectively treat `Symbolic{T}` as being a subtype of `T` for the purposes of dispatch if `T` is an abstract type.

[GeometricMatrixAlgebras.jl](https://github.com/MasonProtter/GeometricMatrixAlgebras.jl)

This package is a playground for learning about Geometric Algebra (GA), and intends to leverage Julia's powerful Linear Algebra ecosystem to compute quantities in GA using matrices as a backend.

[ImplicitEquations.jl](https://github.com/jverzani/ImplicitEquations.jl)

In a paper, Tupper presents a method for graphing two-dimensional implicit equations and inequalities. This package gives an implementation of the paper's basic algorithms to allow the Julia user to naturally represent and easily render graphs of implicit functions and equations.

[Jacobi.jl](https://github.com/pjabardo/Jacobi.jl)

A library that implements Jacobi polynomials and Gauss quadrature related operations.

[QuadDIRECT.jl](https://github.com/timholy/QuadDIRECT.jl)

QuadDIRECT is an algorithm for global optimization without requiring derivatives.

[LinearCovarianceModels.jl](https://github.com/saschatimme/LinearCovarianceModels.jl)

LinearCovarianceModels.jl is a package for computing Maximum Likelihood degrees and MLEs of linear covariance models using numerical nonlinear algebra. In particular HomotopyContinuation.jl.

[HemirealNumbers.jl](https://github.com/timholy/HemirealNumbers.jl)

Implementation of hemireal arithmetic for Julia.

[HemirealFactorizations.jl](https://github.com/timholy/HemirealFactorizations.jl)

Matrix factorizations over the hemireals.

[MultilevelCoordinateSearch.jl](https://github.com/timholy/MultilevelCoordinateSearch.jl)

This is an implementation of Multilevel Coordinate Search (MCS), an algorithm by Waltraud Huyer and Arnold Neumaier for global minimization over a possibly-bounded domain. In a recent comparison of global optimization routines that do not require derivatives, MCS scored highest among non-commercial algorithms.

[OptimizeQCQP.jl](https://github.com/timholy/OptimizeQCQP.jl)

Pure-Julia quadratically-constrained quadratic programming solvers.

[PositiveFactorizations.jl](https://github.com/timholy/PositiveFactorizations.jl)

PositiveFactorizations is a package for computing a positive definite matrix decomposition (factorization) from an arbitrary symmetric input.

[BunchKaufmanUtils.jl](https://github.com/timholy/BunchKaufmanUtils.jl)

Pseudoinverse from a Bunch-Kaufman factorization.

[CoordinateSplittingPTrees.jl](https://github.com/timholy/CoordinateSplittingPTrees.jl)

Accurate and efficient full-degree multidimensional polynomial interpolation.

[RationalSimplex.jl](https://github.com/IainNZ/RationalSimplex.jl)

Julia implementation of the Simplex algorithm for rational numbers. Intended for educational and experimental purposes only. See code for documentation, or tests for examples.

[RandomCorrelationMatrices.jl](https://github.com/IainNZ/RandomCorrelationMatrices.jl)

Generate random correlation matrices, for some definition of random.

[JuMPeR.jl](https://github.com/IainNZ/JuMPeR.jl)

JuMPeR is a modeling language for robust optimization. It is embedded in the Julia programming language, and is an extension to the JuMP modeling language.

[Gridap.jl](https://github.com/gridap/Gridap.jl)

Gridap provides a set of tools for the grid-based approximation of partial differential equations (PDEs) written in the Julia programming language. The library currently supports linear and nonlinear PDE systems for scalar and vector fields, single and multi-field problems, conforming and nonconforming finite element (FE) discretizations, on structured and unstructured meshes of simplices and n-cubes. It also provides methods for time integration. Gridap is extensible and modular. One can implement new FE spaces, new reference elements, use external mesh generators, linear solvers, post-processing tools, etc.

[GridapMakie.jl](https://github.com/gridap/GridapMakie.jl)

The visualization of numerical results is an important part of finite element (FE) computations. However, before the inception of GridapMakie.jl, the only approach available to data visualization of Gridap.jl computations was to write simulation data to data files (e.g., in vtu format) for later visualization with, e.g., Paraview or VisIt.

[GridapGmsh.jl](https://github.com/gridap/GridapGmsh.jl)

Solve a Poisson problem with Gridap on top of a Finite Element mesh generated by GMSH. The mesh includes two physical groups, "`boundary1`" and "`boundary2`", which are used to define boundary conditions. This is just a simple demo. Once the GMSH mesh is read, all the magic of Gridap can be applied to it.

[OMEinsum.jl](https://github.com/under-Peter/OMEinsum.jl)

This is a repository for the Google Summer of Code project on Differentiable Tensor Networks. It implements one function that both computer scientists and physicists love, the Einstein summation.

[TensorOperations.jl](https://github.com/Jutho/TensorOperations.jl)

Fast tensor operations using a convenient Einstein index notation.

[TimeWarp.jl](https://github.com/ahwillia/TimeWarp.jl)

Dynamic Time Warping (DTW) and related algorithms in Julia.

[NonNegLeastSquares.jl](https://github.com/ahwillia/NonNegLeastSquares.jl)

Some nonnegative least squares solvers in Julia.

[Einsum.jl](https://github.com/ahwillia/Einsum.jl)

This package exports a single macro `@einsum`, which implements similar notation to the Einstein summation convention to flexibly specify operations on Julia `Arrays`, similar to numpy's einsum function (but more flexible!).

[Fresa.jl](https://github.com/ericsfraga/Fresa.jl)

The Fresa package implements a plant propagation algorithm for black-box optimization.

[SaferIntegers.jl](https://github.com/JeffreySarnoff/SaferIntegers.jl)

These integer types do not ignore arithmetic overflows and underflows.

[FastRationals.jl](https://github.com/JeffreySarnoff/FastRationals.jl)

Rationals with unreal performance.

[AngleBetweenVectors.jl](https://github.com/JeffreySarnoff/AngleBetweenVectors.jl)

When computing the arc separating two cartesian vectors, this is robustly stable; others are not.

[RemarkableFloats.jl](https://github.com/JeffreySarnoff/RemarkableFloats.jl)

These IEEE Floats may be marked, cleared, remarked with any one of a few colors/tags.

[ArbNumerics.jl](https://github.com/JeffreySarnoff/ArbNumerics.jl)

For multiprecision numerical computing using values with 25..2,500 digits. With arithmetic and higher level mathematics, this package offers you the best balance of performance and accuracy.

[InfinityInts.jl](https://github.com/JeffreySarnoff/InfinityInts.jl)

Arithmetic with the InfInt type saturates ±Infinty, so there is no overflow or underflow.

[ErrorfreeArithmetic.jl](https://github.com/JeffreySarnoff/ErrorfreeArithmetic.jl)

Error-free transformations are used to get results with extra accuracy.

[Double64s.jl](https://github.com/JeffreySarnoff/Double64s.jl)

Error-free transformations enfold accuracy, careful design speeds computation.

[TwoFloats.jl](https://github.com/JeffreySarnoff/TwoFloats.jl)

Create a more performant, less demanding analog of Double64s.

[Robust32s.jl](https://github.com/JeffreySarnoff/Robust32s.jl)

A more robust Float32 that preserves float performance.

[FastRounding.jl](https://github.com/JeffreySarnoff/FastRounding.jl)

Faster directed rounding for inline arithmetic.

[FasterMinMax.jl](https://github.com/JeffreySarnoff/FasterMinMax.jl)

More performant min, max, minmax with simpler native code.

[QNaNs.jl](https://github.com/JeffreySarnoff/QNaNs.jl)

Simplifies the use of quiet NaNs to propagate information from within numerical computations.

[FiniteFloats.jl](https://github.com/JeffreySarnoff/FiniteFloats.jl)

Floats with neither Infinities nor NaNs.

[IEEEFloats.jl](https://github.com/JeffreySarnoff/IEEEFloats.jl)

IEEE754-2019 structural introspection for Float8, Float16, Float32, Float64, Float128.

[SingleFloats.jl](https://github.com/JeffreySarnoff/SingleFloats.jl)

Float32 results are computed using Float64s.

[XFloats.jl](https://github.com/JeffreySarnoff/XFloats.jl)

XFloats exports two these two types: XFloat16, XFloat32. These floating point types correspond to Float16 and Float32. Use them to compute results with greater accuracy at very high speed.

[Readables.jl](https://github.com/JeffreySarnoff/Readables.jl)

Make extended precision numbers readable.

[LowLevelFloatFunctions.jl](https://github.com/JeffreySarnoff/LowLevelFloatFunctions.jl)

Manipulate sign, exponent, significand of Float64, Float32, Float16 values.

[SimpleFloatFunctions.jl](https://github.com/JeffreySarnoff/SimpleFloatFunctions.jl)

Extra floating point functions.

[ChebyshevNodes.jl](https://github.com/JeffreySarnoff/ChebyshevNodes.jl)

Roots and Extrema of the Four Kinds of Chebyshev Polynomials.

[MarkableIntegers.jl](https://github.com/JeffreySarnoff/MarkableIntegers.jl)

Signed and Unsigned Integers, individually [un]markable.

[RandomXorshiro.jl](https://github.com/JeffreySarnoff/RandomXorshiro.jl)

Xirshiro algorithms for pseudorandom UInt32|64, Int32|64.

[MarkableFloats.jl](https://github.com/JeffreySarnoff/MarkableFloats.jl)

Floats that are markable, unmarkable and remarkable.

[NumericPredicates.jl](https://github.com/JeffreySarnoff/NumericPredicates.jl)

More is__(this) and paired not__(this)__

[DirectedRoundings.jl](https://github.com/JeffreySarnoff/DirectedRoundings.jl)

Round floating point arithmetic as you direct.

[AccurateArithmetic0.jl](https://github.com/JeffreySarnoff/AccurateArithmetic0.jl)

These arithmetic functions provide the usual higher order result and additionally provide the lower order value extending the accuracy and precison of the calculation.

[Nothings.jl](https://github.com/JeffreySarnoff/Nothings.jl)

Additional support for Nothing.

[SortingNetworks.jl](https://github.com/JeffreySarnoff/SortingNetworks.jl)

Sort 1..25 values with conditional swaps.

[NonNegInts.jl](https://github.com/JeffreySarnoff/NonNegInts.jl)

Nonnegative integers.

[ThieleFourPointInterpolation.jl](https://github.com/JeffreySarnoff/ThieleFourPointInterpolation.jl)

Rational interpolation using four given points.

[RomuRandoms.jl](https://github.com/JeffreySarnoff/RomuRandoms.jl)

Romu family of pseudo-random number generators.

[Diagonalizations.jl](https://github.com/Marco-Congedo/Diagonalizations.jl)

Diagonalizations.jl is a Julia signal processing package implementing several closed form and iterative diagonalization procedures for both real and complex data input

[RungeKuttaToolKit.jl](https://github.com/dzhang314/RungeKuttaToolKit.jl)

Tools for deriving new Runge–Kutta methods.

[MultiFloats.jl](https://github.com/dzhang314/MultiFloats.jl)

MultiFloats.jl is a Julia package for extended-precision arithmetic using 100 - 400 bits (≈ 30 - 120 digits). In this range, it is the fastest extended-precision library that I am aware of. At 100-bit precision, MultiFloats.jl is roughly 40x faster than BigFloat and 2x faster than DoubleFloats.jl.

[DZOptimization.jl](https://github.com/dzhang314/DZOptimization.jl)

DZOptimization.jl is a Julia package for smooth nonlinear optimization that emphasizes performance, flexibility, and memory efficiency. In basic usage examples (see below), DZOptimization.jl has 6x less overhead and uses 10x less memory than Optim.jl.

[prima.jl](https://github.com/libprima/prima.jl)

This package is a Julia interface to the PRIMA library, a Reference Implementation for Powell's Methods with Modernization and Amelioration, by Zaikun Zhang who re-implemented and improved algorithms originally by M.J.D. Powell for minimizing a multi-variate objective function possibly under constraints and without derivatives.

[NOMAD.jl](https://github.com/bbopt/NOMAD.jl)

This package provides a Julia interface for NOMAD, which is a C++ implementation of the Mesh Adaptive Direct Search algorithm (MADS), designed for difficult blackbox optimization problems. These problems occur when the functions defining the objective and constraints are the result of costly computer simulations.

[BlackBoxOptim.jl](https://github.com/robertfeldt/BlackBoxOptim.jl)

BlackBoxOptim is a global optimization package for Julia (http://julialang.org/). It supports both multi- and single-objective optimization problems and is focused on (meta-)heuristic/stochastic algorithms (DE, NES etc) that do NOT require the function being optimized to be differentiable. This is in contrast to more traditional, deterministic algorithms that are often based on gradients/differentiability. It also supports parallel evaluation to speed up optimization for functions that are slow to evaluate.

[Trixi.jl](https://github.com/trixi-framework/Trixi.jl)

Trixi.jl is a numerical simulation framework for conservation laws written in Julia. A key objective for the framework is to be useful to both scientists and students. Therefore, next to having an extensible design with a fast implementation,

[Yeppp.jl](https://github.com/JuliaMath/Yeppp.jl)

Yeppp! is a high-performance SIMD-optimized mathematical library. This Julia package makes it possible to call Yeppp from Julia.

[FourierTools.jl](https://github.com/bionanoimaging/FourierTools.jl)

This package contains various functions that are useful for working with and in Fourier space.

[SeparableFunctions.jl](https://github.com/bionanoimaging/SeparableFunctions.jl)

Calculates multidimensional functions faster by exploiting their separability.

[LaplaceInterpolation.jl](https://github.com/vishwas1984/LaplaceInterpolation.jl)

Fast Interpolation for Volume Datasets.

[DiscreteDifferentialGeometry.jl](https://github.com/digitaldomain/DiscreteDifferentialGeometry.jl)

The DiscreteDifferentialGeometry Julia package defines Types and methods to implement Discrete Differential Geometry.

[HalfEdges.jl](https://github.com/digitaldomain/HalfEdges.jl)

The HalfEdges Julia package defines the HalfEdges type and methods/types to implement operations on the halfedge data structure.

[Laplacians.jl](https://github.com/danspielman/Laplacians.jl)

Laplacians is a package containing graph algorithms, with an emphasis on tasks related to spectral and algebraic graph theory. It contains (and will contain more) code for solving systems of linear equations in graph Laplacians, low stretch spanning trees, sparsifiation, clustering, local clustering, and optimization on graphs.

[Mosek.jl](https://github.com/MOSEK/Mosek.jl)

Interface to the MOSEK solver in Julia.

[BDDSolver.jl](https://github.com/danspielman/BDDSolver.jl)

Code for solving systems of linear equations in nonsingular, symmetric, block diagonally dominant (BDD) matrices. This family includes Laplacians, SDDM matrices, and Connection Laplacians.

[PosDefManifold.jl](https://github.com/Marco-Congedo/PosDefManifold.jl)

A Julia package for manipulating data in the Riemannian manifold of positive definite matrices.

[PosDefManifoldML.jl](https://github.com/Marco-Congedo/PosDefManifoldML.jl)

A Julia Package for Machine Learning on the Manifold of Positive Definite Matrices.

[Dierckx.jl](https://github.com/getzze/Dierckx.jl)

This is a Julia wrapper for the dierckx Fortran library, the same library underlying the spline classes in scipy.interpolate. Some of the functionality here overlaps with Interpolations.jl, a pure-Julia interpolation package. Take a look at it if you have a use case not covered here.

[SymEngine.jl](https://github.com/symengine/SymEngine.jl)

Julia Wrappers for [SymEngine](https://github.com/symengine/symengine), a fast symbolic manipulation library, written in C++.

[Fresa.jl](https://github.com/ericsfraga/Fresa.jl)

The Fresa package implements a plant propagation algorithm for black-box optimization.

[FrankWolfe.jl](https://github.com/ZIB-IOL/FrankWolfe.jl)

This package is a toolbox for Frank-Wolfe and conditional gradients algorithms.

[GenericSVD.jl](https://github.com/JuliaLinearAlgebra/GenericSVD.jl)

Implements Singular Value Decomposition for generic number types, such as `BigFloat`, `Complex{BigFloat}` or `Quaternion`s. It internally overloads several Base functions such that existing methods (`svd`, `svdfact` and `svdvals`) should work directly.

[ArpackMKL.jl](https://github.com/JuliaLinearAlgebra/ArpackMKL.jl)

This repo contains Arpack built against the MKL binaries. The module presents as ArpackMKL to Julia, but the underlying binary is still called libarpack.

[HierarchicalMatrices.jl](https://github.com/JuliaLinearAlgebra/HierarchicalMatrices.jl)

This package provides a flexible framework for hierarchical data types in Julia.

[BLIS.jl](https://github.com/JuliaLinearAlgebra/BLIS.jl)

This repo plans to provide a low-level Julia wrapper for BLIS typed interface.

[BLISBLAS.jl](https://github.com/JuliaLinearAlgebra/BLISBLAS.jl)

BLISBLAS.jl is a Julia package that allows users to use the BLIS library for Julia's underlying BLAS. Note that BLIS only provides BLAS but not LAPACK (OpenBLAS will still be used for LAPACK functionality).

[OpenBLAS32.jl](https://github.com/JuliaLinearAlgebra/OpenBLAS32.jl)

A simple package that depends on OpenBLAS32_jll, and sets up the forwarding for LP64 BLAS symbols on 64-bit architectures using libblastrampoline that ships with Julia.

[MultiThreadedLU.jl](https://github.com/JuliaLinearAlgebra/MultiThreadedLU.jl)

A multi-threaded LU implementation.

[Preconditioners.jl](https://github.com/JuliaLinearAlgebra/Preconditioners.jl)

A few preconditioners for iterative solvers.

[WoodburyMatrices.jl](https://github.com/JuliaLinearAlgebra/WoodburyMatrices.jl)

This package provides support for the Woodbury matrix identity for the Julia programming language. This is a generalization of the Sherman-Morrison formula. Note that the Woodbury matrix identity is notorious for floating-point roundoff errors, so be prepared for a certain amount of inaccuracy in the result.

[GenericLinearAlgebra.jl](https://github.com/JuliaLinearAlgebra/GenericLinearAlgebra.jl)

The purpose of this package is partly to extend linear algebra functionality in base to cover generic element types, e.g. `BigFloat` and `Quaternion`, and partly to be a place to experiment with fast linear algebra routines written in Julia (except for optimized BLAS). It is my hope that it is possible to have implementations that are generic, fast, and readable.

[Arpack.jl](https://github.com/JuliaLinearAlgebra/Arpack.jl)

Julia wrapper for the arpack library designed to solve large-scale eigenvalue problems.

[IncrementalSVD.jl](https://github.com/JuliaLinearAlgebra/IncrementalSVD.jl)

IncrementalSVD provides incremental (updating) singular value decomposition. This allows you to update an existing SVD with new columns, and even implement online SVD with streaming data.

[ToeplitzMatrices.jl](https://github.com/JuliaLinearAlgebra/ToeplitzMatrices.jl)

Fast matrix multiplication and division for Toeplitz, Hankel and circulant matrices in Julia.

[RectangularFullPacked.jl](https://github.com/JuliaLinearAlgebra/RectangularFullPacked.jl)

A Julia package for the Rectangular Full Packed (RFP) matrix storage format.

[Octavian.jl](https://github.com/JuliaLinearAlgebra/Octavian.jl)

Octavian.jl is a multi-threaded BLAS-like library that provides pure Julia matrix multiplication on the CPU, built on top of LoopVectorization.jl.

[LowRankApprox.jl](https://github.com/JuliaLinearAlgebra/LowRankApprox.jl)

This Julia package provides fast low-rank approximation algorithms for BLAS/LAPACK-compatible matrices based on some of the latest technology in adaptive randomized matrix sketching.

[RecursiveFactorization.jl](https://github.com/JuliaLinearAlgebra/RecursiveFactorization.jl)

RecursiveFactorization.jl is a package that collects various recursive matrix factorization algorithms.

[LowRankMatrices.jl](https://github.com/JuliaLinearAlgebra/LowRankMatrices.jl)

This package is an offshoot of LowRankApprox.jl, and provides the `LowRankMatrix` type that is used in that package. It should be a lightweight dependency if you only need to use a `LowRankMatrix` without the functionality provided by LowRankApprox.jl.

[SkewLinearAlgebra.jl](https://github.com/JuliaLinearAlgebra/SkewLinearAlgebra.jl)

The SkewLinearAlgebra package provides specialized matrix types, optimized methods of LinearAlgebra functions, and a few entirely new functions for dealing with linear algebra on skew-Hermitian matrices, especially for the case of real skew-symmetric matrices.

[TSVD.jl](https://github.com/JuliaLinearAlgebra/TSVD.jl)

Truncated singular value decomposition with partial reorthogonalization.

[ArnoldiMethod.jl](https://github.com/JuliaLinearAlgebra/ArnoldiMethod.jl)

The Arnoldi Method with Krylov-Schur restart, natively in Julia.

[MatrixFactorizations.jl](https://github.com/JuliaLinearAlgebra/MatrixFactorizations.jl)

A Julia package to contain non-standard matrix factorizations. At the moment it implements the QL, RQ, and UL factorizations, a combined Cholesky factorization with inverse, and polar decompositions. In the future it may include other factorizations such as the LQ factorization.

[SpecialMatrices.jl](https://github.com/JuliaLinearAlgebra/SpecialMatrices.jl)

A Julia package for working with special matrix types. This Julia package extends the LinearAlgebra library with support for special matrices that are used in linear algebra. Every special matrix has its own type and is stored efficiently. The full matrix is accessed by the command `Matrix(A)`.

[BlockBandedMatrices.jl](https://github.com/JuliaLinearAlgebra/BlockBandedMatrices.jl)

A Julia package for representing block-banded matrices and banded-block-banded matrices. This package supports representing block-banded and banded-block-banded matrices by only storing the entries in the non-zero bands.

[SemiseparableMatrices.jl](https://github.com/JuliaLinearAlgebra/SemiseparableMatrices.jl)

A Julia package to represent semiseparable and almost banded matrices.

[MatrixDepot.jl](https://github.com/JuliaLinearAlgebra/MatrixDepot.jl)

An extensible test matrix collection for Julia.

[AlgebraicMultigrid.jl](https://github.com/JuliaLinearAlgebra/AlgebraicMultigrid.jl)

This package lets you solve sparse linear systems using Algebraic Multigrid (AMG). This works especially well for symmetric positive definite matrices.

[BandedMatrices.jl](https://github.com/JuliaLinearAlgebra/BandedMatrices.jl)

A Julia package for representing banded matrices.

[LazyBandedMatrices.jl](https://github.com/JuliaLinearAlgebra/LazyBandedMatrices.jl)

This package supports lazy banded and block-banded matrices, for example, a lazy multiplication of banded matrices.

[ArrayLayouts.jl](https://github.com/JuliaLinearAlgebra/ArrayLayouts.jl)

A Julia package for describing array layouts and more general fast linear algebra.

[LinearMaps.jl](https://github.com/JuliaLinearAlgebra/LinearMaps.jl)

A Julia package for defining and working with linear maps, also known as linear transformations or linear operators acting on vectors. The only requirement for a LinearMap is that it can act on a vector (by multiplication) efficiently. 

[InfiniteLinearAlgebra.jl](https://github.com/JuliaLinearAlgebra/InfiniteLinearAlgebra.jl)

A Julia repository for linear algebra with infinite banded and block-banded matrices.

[PermGroups.jl](https://github.com/jmichel7/PermGroups.jl)

This package implements permutations and some functions of them.

[Permutations.jl](https://github.com/scheinerman/Permutations.jl)

This package defines a Permutation type for Julia. We only consider permutations of sets of the form `{1,2,3,...,n}` where n is a positive integer.

[SparseFFT.jl](https://github.com/klho/SparseFFT.jl)

This Julia package provides functions for computing sparse (or "pruned") fast Fourier transforms (FFTs) in one (1D) and two (2D) dimensions.

[ProximalCore.jl](https://github.com/JuliaFirstOrder/ProximalCore.jl)

Core definitions for the ProximalOperators and ProximalAlgorithms ecosystem.

[ProximalAlgorithms.jl](https://github.com/JuliaFirstOrder/ProximalAlgorithms.jl)

A Julia package for non-smooth optimization algorithms. This package provides algorithms for the minimization of objective functions that include non-smooth terms, such as constraints or non-differentiable penalties.

[ProximalOperators.jl](https://github.com/JuliaFirstOrder/ProximalOperators.jl)

Proximal operators for nonsmooth optimization in Julia. This package can be used to easily implement proximal algorithms for convex and nonconvex optimization problems such as ADMM, the alternating direction method of multipliers.

[PiecewiseQuadratics.jl](https://github.com/JuliaFirstOrder/PiecewiseQuadratics.jl)

A Julia package for manipulation of univariate piecewise quadratic functions.

[SeparableOptimization.jl](https://github.com/JuliaFirstOrder/SeparableOptimization.jl)

A Julia package that solves Linearly Constrained Separable Optimization Problems using ADMM. 

[StructuredOptimization.jl](https://github.com/JuliaFirstOrder/StructuredOptimization.jl)

StructuredOptimization.jl is a high-level modeling language that utilizes a syntax that is very close to the mathematical formulation of an optimization problem.

[Tulip.jl](https://github.com/ds4dm/Tulip.jl)

Tulip is an open-source interior-point solver for linear optimization, written in pure Julia. It implements the homogeneous primal-dual interior-point algorithm with multiple centrality corrections, and therefore handles unbounded and infeasible problems. Tulip’s main feature is that its algorithmic framework is disentangled from linear algebra implementations. This allows to seamlessly integrate specialized routines for structured problems.

[Fortuna.jl](https://github.com/AkchurinDA/Fortuna.jl)

Fortuna.jl is a general-purpose Julia package for structural and system reliability analysis.

[AutoCUFSM.jl](https://github.com/AkchurinDA/AutoCUFSM.jl)

This repository contains a port of the CUFSM package, primarily developed by Dr. Benjamin W. Schafer, from MATLAB to Julia. This port enables users to perform automatic differentiation through any part of its code, facilitating the optimization of cold-formed steel section topologies for strength, stiffness, and other desired parameters.

[JMcDM](https://github.com/jbytecode/JMcDM)

A package for Multiple-criteria decision-making techniques in Julia.

[ManifoldsBase.jl](https://github.com/JuliaManifolds/ManifoldsBase.jl)

Basic interface for manifolds in Julia.

[Manifolds.jl](https://github.com/JuliaManifolds/Manifolds.jl)

Manifolds.jl provides a library of manifolds aiming for an easy-to-use and fast implementation.

---

### Artificial intelligence

[MLBase.jl](https://github.com/JuliaStats/MLBase.jl)

Swiss knife for machine learning.

[MLJ.jl](https://github.com/JuliaAI/MLJ.jl)

A Machine Learning Framework for Julia. MLJ (Machine Learning in Julia) is a toolbox written in Julia providing a common interface and meta-algorithms for selecting, tuning, evaluating, composing and comparing about 200 machine learning models written in Julia and other languages.

[MLJBase.jl](https://github.com/JuliaAI/MLJBase.jl)

Repository for developers that provides core functionality for the MLJ machine learning framework. MLJ is a Julia framework for combining and tuning machine learning models. This repository provides core functionality for MLJ.

[MLJModels.jl](https://github.com/JuliaAI/MLJModels.jl)

Repository of the "built-in" models available for use in the MLJ MLJ machine learning framework; and the home of the MLJ model registry.

[MLJFlow.jl](https://github.com/JuliaAI/MLJFlow.jl)

MLJ is a Julia framework for combining and tuning machine learning models. MLJFlow is a package that extends the MLJ capabilities to use MLflow as a backend for model tracking and experiment management. To be specific, MLJFlow provides a close to zero-preparation to use MLflow with MLJ; by the usage of function extensions that automate the MLflow cycle (create experiment, create run, log metrics, log parameters, log artifacts, etc.).

[LearnAPI.jl](https://github.com/JuliaAI/LearnAPI.jl)

A base Julia interface for machine learning and statistics.

[StatisticalTraits.jl](https://github.com/JuliaAI/StatisticalTraits.jl)

A light-weight package defining fall-back implementations for a collection of traits possessed by statistical objects. Here a "trait" is a function with a single argument that is a julia type, which might encode type metadata for inspection, or for use in function dispatch.

[DifferentialEquations.jl](https://github.com/SciML/DifferentialEquations.jl)

Multi-language suite for high-performance solvers of differential equations and scientific machine learning (SciML) components. Ordinary differential equations (ODEs), stochastic differential equations (SDEs), delay differential equations (DDEs), differential-algebraic equations (DAEs), and more in Julia. 

[NonlinearSolve.jl](https://github.com/SciML/NonlinearSolve.jl)

High-performance and differentiation-enabled nonlinear solvers (Newton methods), bracketed rootfinding (bisection, Falsi), with sparsity and Newton-Krylov support.

[ScientificTypesBase.jl](https://github.com/JuliaAI/ScientificTypesBase.jl)

A light-weight, dependency-free, Julia interface defining a collection of types (without instances) for implementing conventions about the scientific interpretation of data.

[ScientificTypes.jl](https://github.com/JuliaAI/ScientificTypes.jl)

This package makes a distinction between machine type and scientific type of a Julia object.

[OpenML.jl](https://github.com/JuliaAI/OpenML.jl)

Partial implementation of the OpenML API for Julia. At present this package allows querying and downloading of OpenML datasets.

[Flux.jl](https://github.com/FluxML/Flux.jl)

Flux is an elegant approach to machine learning. It's a 100% pure-Julia stack, and provides lightweight abstractions on top of Julia's native GPU and AD support. Flux makes the easy things easy while remaining fully hackable.

[Metalhead.jl](https://github.com/FluxML/Metalhead.jl)

Metalhead.jl provides standard machine learning vision models for use with Flux.jl. The architectures in this package make use of pure Flux layers, and they represent the best-practices for creating modules like residual blocks, inception blocks, etc. in Flux. Metalhead also provides some building blocks for more complex models in the Layers module.

[Alice.jl](https://github.com/Wedg/Alice.jl)

A deep learning package for supervised learning built in Julia.

[MLTools.jl](https://github.com/Wedg/MLTools.jl)

Utilities for Machine Learning in Julia (ROC Curve, Confusion Matrix).

[Knet.jl](https://denizyuret.github.io/Knet.jl/stable/)

Knet (pronounced "kay-net") is the Koç University deep learning framework implemented in Julia by Deniz Yuret and collaborators.

[Lux.jl](https://github.com/LuxDL/Lux.jl)

The Deep Learning Framework.

[LuxCUDA.jl](https://github.com/LuxDL/LuxCUDA.jl)

LuxCUDA is meant to be used as a trigger package for all CUDA dependencies in Lux. Users requiring CUDA support should install LuxCUDA and load it alongside Lux.

[LuxAMDGPU.jl](https://github.com/LuxDL/LuxAMDGPU.jl)

LuxAMDGPU is meant to be used as a trigger package for all AMDGPU dependencies in Lux. Users requiring AMDGPU support should install LuxAMDGPU and load it alongside Lux.

[WeightInitializers.jl](https://github.com/LuxDL/WeightInitializers.jl)

This package is a light dependency providing common weight initialization schemes for deep learning models.

[Boltz.jl](https://github.com/LuxDL/Boltz.jl)

Accelerate your ML research using pre-built Deep Learning Models with Lux.

[Avalon.jl](https://github.com/dfdx/Avalon.jl)

Avalon is a deep learning library in Julia with focus on high performance and interoperability with existing DL frameworks.

[Yota.jl](https://github.com/dfdx/Yota.jl)

Yota.jl is a package for reverse-mode automatic differentiation in Julia.

[SimpleChains.jl](https://github.com/PumasAI/SimpleChains.jl)

SimpleChains.jl only supports simple chains, but it intends to be fast for small problems on the CPU. 

[FastAI.jl](https://fluxml.ai/FastAI.jl/dev/README.md.html)

FastAI.jl is inspired by fastai, and is a repository of best practices for deep learning in Julia.

[ModelingToolkit.jl](https://mtk.sciml.ai/stable/)

ModelingToolkit.jl is a modeling language for high-performance symbolic-numeric computation in scientific computing and scientific machine learning.

[GlobalSensitivity.jl](https://gsa.sciml.ai/stable/)

Global Sensitivity Analysis (GSA) methods are used to quantify the uncertainty in output of a model with respect to the parameters.

[DecisionTrees.jl](https://github.com/kafisatz/DecisionTrees.jl)

A pure Julia implementation of Decision Tree Algorithms for Regression.

[NearestNeighbors.jl](https://github.com/KristofferC/NearestNeighbors.jl)

NearestNeighbors.jl is a package written in Julia to perform high performance nearest neighbor searches in arbitrarily high dimensions.

[Lathe.jl](https://github.com/ChifiSource/Lathe.jl)

Lathe.jl is an all-in-one package for predictive modeling in Julia. It comes packaged with a Stats Library, Preprocessing Tools, Distributions, Machine-Learning Models, and Model Validation. Lathe features easy object-oriented programming methodologies using Julia's dispatch.

[Hyperparameters.jl](https://github.com/invenia/Hyperparameters.jl)

A minimal utility for working with AWS Sagemaker hyperparameters. More broadly for dealing with environment variables.

[Keras.jl](https://github.com/invenia/Keras.jl)

Keras.jl uses PyCall.jl to build a julia wrapper around the python neural network library keras.

[ParameterHandling.jl](https://github.com/invenia/ParameterHandling.jl)

ParameterHandling.jl is an experiment in handling constrained tunable parameters of models.

[FeatureTransforms.jl](https://github.com/invenia/FeatureTransforms.jl)

FeatureTransforms.jl provides utilities for performing feature engineering in machine learning pipelines with support for `AbstractArray`s and `Table`s.

[FeatureDescriptors.jl](https://github.com/invenia/FeatureDescriptors.jl)

FeatureDescriptors.jl is an interface package for describing features used in models, feature engineering, and other data-science workflows.

[MaximumLikelihoodUtils.jl](https://github.com/invenia/MaximumLikelihoodUtils.jl)

A package for Maximum Likelihood Estimation in various models using Expectation Maximisation and gradient descent.

[Models.jl](https://github.com/invenia/Models.jl)

An interface package that defines the methods and types for working with models.

[Nabla.jl](https://github.com/invenia/Nabla.jl)

Nabla.jl is a reverse-mode automatic differentiation package targetting machine learning use cases. As such, we have (for example) prioritised support for linear algebra optimisations and higher-order functions over the ability to take higher-order derivatives (Nabla currently only supports first-order derivatives).

[TSNE.jl](https://github.com/Wedg/TSNE.jl)

An implementation of the t-Sne algorithm. The main author (Laurens van der Maaten) has information on t-Sne (including papers and links to other implementations) [here](https://lvdmaaten.github.io/tsne/).

[MilDatasets.jl](https://github.com/marekdedic/MilDatasets.jl)

A set of standard datasets in Multi-instance learning to be used with Flux.jl.

[MilDistances.jl](https://github.com/marekdedic/MilDistances.jl)

CPC, triplet loss and magnet loss for multi-instance learning. For use with Flux.jl and Mill.jl.

[ArrayFlow.jl](https://github.com/malmaud/ArrayFlow.jl)

Wrapper around the Tensorflow C api.

[TensorFlow.jl](https://github.com/malmaud/TensorFlow.jl)

A wrapper around TensorFlow, a popular open source machine learning framework from Google.

[EvoLP.jl](https://github.com/ntnu-ai-lab/EvoLP.jl)

EvoLP is a playground for evolutionary computation in Julia. It provides a set of predefined building blocks that can be coupled together to play around: quickly generate evolutionary computation solvers and compute statistics for a variety of optimisation tasks, including discrete, continuous and combinatorial optimisation.

[MaskRCNN.jl](https://github.com/DhairyaLGandhi/maskrcnn)

Implementation of Mask RCNN in Flux.jl.

[DynamicDiscreteModels.jl](https://github.com/BenConnault/DynamicDiscreteModels.jl)

Back-end support for doing statistical inference with any model that can described as a "partially observed" (discrete) Markov chain, such as a Hidden Markov Model.

[HiddenMarkovModels.jl](https://github.com/BenConnault/HiddenMarkovModels.jl)

Basic simulation / parameter estimation / latent state inference for hidden Markov models. Thin front-end package built on top of DynamicDiscreteModels.jl.

[Strada.jl](https://github.com/pcmoritz/Strada.jl)

Strada is a Deep Learning library for Julia, based on the popular Caffe framework developed by BVLC and the Berkeley computer vision community. It supports convolutional and recurrent neural netwok training, both on the CPU and GPU.

[ArviZ.jl](https://github.com/arviz-devs/ArviZ.jl)

ArviZ.jl (pronounced "AR-vees") is a Julia package for exploratory analysis of Bayesian models. It includes functions for posterior analysis, model checking, comparison and diagnostics.

[MNIST.jl](https://github.com/johnmyleswhite/MNIST.jl)

This package provides access to the classic MNIST data set of handwritten digits that has been used as a testbed for new machine learning methods.

[JMcDM](https://github.com/jbytecode/JMcDM)

A package for Multiple-criteria decision-making techniques in Julia.

[Fastcluster.jl](https://github.com/jmboehm/Fastcluster.jl)

Julia wrapper to Daniel Muellner's fastcluster library for hierarchical clustering.

[EvoTrees.jl](https://github.com/Evovest/EvoTrees.jl)

A Julia implementation of boosted trees with CPU and GPU support. Efficient histogram based algorithms with support for multiple loss functions (notably multi-target objectives such as max likelihood methods).

[NeuroTreeModels.jl](https://github.com/Evovest/NeuroTreeModels.jl)

Differentiable tree-based models for tabular data.

[MLBenchmarks.jl](https://github.com/Evovest/MLBenchmarks.jl)

This repo provides Julia based benchmarks for ML algo on tabular data. It was developed to support both NeuroTreeModels.jl and EvoTrees.jl projects.

[StatisticalMeasuresBase.jl](https://github.com/JuliaAI/StatisticalMeasuresBase.jl)

A Julia package for building production-ready measures (metrics) for statistics and machine learning.

[StatisticalMeasures.jl](https://github.com/JuliaAI/StatisticalMeasures.jl)

Measures (metrics) for statistics and machine learning.

[MLJOpenML.jl](https://github.com/JuliaAI/MLJOpenML.jl)

A package providing integration of OpenML with the MLJ machine learning framework.

[NearestNeighborModels.jl](https://github.com/JuliaAI/NearestNeighborModels.jl)

Package providing K-nearest neighbor regressors and classifiers, for use with the MLJ machine learning framework.

[MLJParticleSwarmOptimization.jl](https://github.com/JuliaAI/MLJParticleSwarmOptimization.jl)

Particle swarm optimization for hyperparameter tuning in MLJ.

[TreeRecipe.jl](https://github.com/JuliaAI/TreeRecipe.jl)

A Plot recipe for plotting (decision) trees.

[EarlyStopping.jl](https://github.com/JuliaAI/EarlyStopping.jl)

A small package for applying early stopping criteria to loss-generating iterative algorithms, with a view to training and optimizing machine learning models.

[IterationControl.jl](https://github.com/JuliaAI/IterationControl.jl)

A lightweight package for controlling iterative algorithms, with a view to training and optimizing machine learning models.

[DecisionTree.jl](https://github.com/JuliaAI/DecisionTree.jl)

Julia implementation of Decision Tree (CART) and Random Forest algorithms.

[CatBoost.jl](https://github.com/JuliaAI/CatBoost.jl)

Julia interface to CatBoost. This library is a wrapper CatBoost's Python package via PythonCall.jl.

[MLJEnsembles.jl](https://github.com/JuliaAI/MLJEnsembles.jl)

A package to create bagged homogeneous ensembles of machine learning models using the MLJ machine learning framework.

[MLJIteration.jl](https://github.com/JuliaAI/MLJIteration.jl)

A package for wrapping iterative models provided by the MLJ machine learning framework in a control strategy.

[MLJTuning.jl](https://github.com/JuliaAI/MLJTuning.jl)

Hyperparameter optimization for MLJ machine learning models.

[MLJBalancing.jl](https://github.com/JuliaAI/MLJBalancing.jl)

A package providing composite models wrapping class imbalance algorithms from Imbalance.jl with classifiers from MLJ.

[FeatureSelection.jl](https://github.com/JuliaAI/FeatureSelection.jl)

Repository housing feature selection algorithms for use with the machine learning toolbox MLJ. 

[MLJTestIntegration.jl](https://github.com/JuliaAI/MLJTestIntegration.jl)

Package for applying integration tests to models implementing the MLJ model interface.

[MLJText.jl](https://github.com/JuliaAI/MLJText.jl)

An MLJ extension providing tools and models for text analysis.

[MLFlowClient.jl](https://github.com/JuliaAI/MLFlowClient.jl)

Julia client for MLFlow.

[MLJModelInterface.jl](https://github.com/JuliaAI/MLJModelInterface.jl)

A light-weight interface for developers wanting to integrate machine learning models into MLJ.

[MLJNaiveBayesInterface.jl](https://github.com/JuliaAI/MLJNaiveBayesInterface.jl)

Repository implementing MLJ interface for NaiveBayes models.

[MLJMultivariateStatsInterface.jl](https://github.com/JuliaAI/MLJMultivariateStatsInterface.jl)

Repository implementing MLJ interface for MultivariateStats models.

[MLJTSVDInterface.jl](https://github.com/JuliaAI/MLJTSVDInterface.jl)

Repository implementing the MLJ model interface for models provided by TSVD.jl.

[MLJLIBSVMInterface.jl](https://github.com/JuliaAI/MLJLIBSVMInterface.jl)

Repository implementing MLJ interface for LIBSVM models.

[MLJTestInterface.jl](https://github.com/JuliaAI/MLJTestInterface.jl)

Package for testing an implementation of the MLJ model interface.

[MLJGLMInterface.jl](https://github.com/JuliaAI/MLJGLMInterface.jl)

Repository implementing MLJ interface for GLM models.

[MLJLinearModels.jl](https://github.com/JuliaAI/MLJLinearModels.jl)

This is a package gathering functionalities to solve a number of generalised linear regression/classification problems.

[MLJXGBoostInterface.jl](https://github.com/JuliaAI/MLJXGBoostInterface.jl)

Repository implementing MLJ interface for XGBoost models.

[MLJDecisionTreeInterface.jl](https://github.com/JuliaAI/MLJDecisionTreeInterface.jl)

Repository implementing the MLJ model interface for DecisionTree models.

[MLJClusteringInterface.jl](https://github.com/JuliaAI/MLJClusteringInterface.jl)

Repository implementing MLJ interface for Clustering.jl models.

[MLJScikitLearnInterface.jl](https://github.com/JuliaAI/MLJScikitLearnInterface.jl)

Repository implementing MLJ interface for scikit-learn models (via PythonCall.jl).

[Imbalance.jl](https://github.com/JuliaAI/Imbalance.jl)

A Julia package with resampling methods to correct for class imbalance in a wide variety of classification settings.

[CategoricalDistributions.jl](https://github.com/JuliaAI/CategoricalDistributions.jl)

Probability distributions and measures for finite sample spaces whose elements are labeled (consist of the class pool of a `CategoricalArray`).

[Spacy.jl](https://github.com/joshday/Spacy.jl)

A lightweight wrapper of the amazing spaCy Python package.

[ProbNumDiffEq.jl](https://github.com/nathanaelbosch/ProbNumDiffEq.jl)

ProbNumDiffEq.jl provides probabilistic numerical ODE solvers to the DifferentialEquations.jl ecosystem. The implemented ODE filters solve differential equations via Bayesian filtering and smoothing. The filters compute not just a single point estimate of the true solution, but a posterior distribution that contains an estimate of its numerical approximation error.

---

### Signal analysis

[FFTA.jl](https://github.com/dannys4/FFTA.jl)

This is a pure Julia implementation of FFTs, with the goal that this could supplant other FFTs for applications that require odd Julia objects. 

[DSP.jl](https://docs.juliadsp.org/stable/contents/)

DSP.jl provides a number of common digital signal processing routines in Julia.

[Deconvolution.jl](https://juliadsp.org/Deconvolution.jl/dev/)

Deconvolution.jl provides a set of functions to deconvolve digital signals, like images or time series.

[Wavelets.jl](https://github.com/JuliaDSP/Wavelets.jl)

A Julia package for fast wavelet transforms (1-D, 2-D, 3-D, by filtering or lifting). The package includes discrete wavelet transforms, column-wise discrete wavelet transforms, and wavelet packet transforms.

[ContinuousWavelets.jl](https://github.com/UCD4IDS/ContinuousWavelets.jl)

This package is an offshoot of Wavelets.jl for the continuous wavelets.

[WaveletsExt.jl](https://ucd4ids.github.io/WaveletsExt.jl/stable/)

This package is a Julia extension package to Wavelets.jl (WaveletsExt is short for Wavelets Extension).

[Estimation.jl](https://github.com/JuliaDSP/Estimation.jl)

A julialang package for DSP related estimation.

[MFCC.jl](https://github.com/JuliaDSP/MFCC.jl)

A package to compute Mel Frequency Cepstral Coefficients.

[FindPeaks1D](https://ymtoo.github.io/FindPeaks1D.jl/stable/)

Finding peaks in a 1-D signal in Julia. The implementation is based on find_peaks in SciPy.

[CubicSplines.jl](https://github.com/sp94/CubicSplines.jl)

A simple package for interpolating 1D data with Akima cubic splines, based on "A New Method of Interpolation and Smooth Curve Fitting Based on Local Parameters", Akima, 1970.

[EntropyHub.jl](https://github.com/MattWillFlood/EntropyHub.jl)

EntropyHub: An open-source toolkit for entropic time series analysis.

[MDCT.jl](https://github.com/stevengj/MDCT.jl)

This module computes the modified discrete cosine transform (MDCT) in the Julia language and the inverse transform (IMDCT), using the fast type-IV discrete cosine tranform (DCT-IV) functions in the FFTW.jl package.

[WorldVocoder.jl](https://github.com/jkrumbiegel/WorldVocoder.jl)

WorldVocoder.jl is a wrapper package for the WORLD vocoder by mmorise https://github.com/mmorise/World. It calls the original WORLD binary via the WORLD_jll.jl package.

[SavitzkyGolay.jl](https://github.com/lnacquaroli/SavitzkyGolay.jl)

Implementation of the 1D Savitzky-Golay filter.

[Kalman.jl](https://github.com/mschauer/Kalman.jl)

Flexible filtering and smoothing in Julia. Kalman uses `DynamicIterators` (an iterator protocol for dynamic data dependent and controlled processes) and `GaussianDistributions` (Gaussian distributions as abstraction for the uncertain state) to implement flexible online Kalman filtering.

[DirectionalStatistics.jl](https://gitlab.com/aplavin/DirectionalStatistics.jl)

Directional statistics package for Julia. Currently includes several circular and spatial descriptive statistics.

[MagneticResonanceSignals.jl](https://github.com/TRIImaging/MagneticResonanceSignals.jl)

File IO and signal processing tools for raw Magnetic Resonance data, with a focus on spectroscopy.

[SignalBase.jl](https://github.com/haberdashPI/SignalBase.jl)

SignalBase defines a basic API to inspect signals that are regularly sampled in time. It consists of the following functions.

[SignalOperators.jl](https://github.com/haberdashPI/SignalOperators.jl)

SignalOperators is a Julia package that aims to provide a clean interface for generating and manipulating signals: typically sounds, but any signal regularly sampled in time can be manipulated.

[SignalAnalysis.jl](https://github.com/org-arl/SignalAnalysis.jl)

While a few great signal processing packages (e.g. DSP.jl, SignalOperators.jl) are available, they have limited time-frequency analysis, sonar analysis, and baseband analysis capabilities. This SignalAnalysis.jl package aims to fill that gap. The package has grown out of my research needs, but I hope to expand it over time to provide a wide variety of time-frequency analysis, and baseband signal processing tools.

[SignalScope.jl](https://github.com/org-arl/SignalScope.jl)

Oscilloscope view from various real-time signal sources.

[UnderwaterAcoustics.jl](https://github.com/org-arl/UnderwaterAcoustics.jl)

Julia toolbox for underwater acoustic modeling.

[AcousticsToolbox.jl](https://github.com/org-arl/AcousticsToolbox.jl)

This package provides a Julia wrapper to the OALIB acoustic propagation modeling toolbox, making it available for use with UnderwaterAcoustics.jl.

[AcousticRayTracers.jl](https://github.com/org-arl/AcousticRayTracers.jl)

RaySolver is a differentiable 2½D Gaussian beam tracer for use with UnderwaterAcoustics.jl. It is similar to Bellhop, but fully written in Julia to be compatible with automatic differentiation (AD) tool such as ForwardDiff (compatibility with other AD packages such as ReverseDiff and Zygote is not fully tested).

[AcousticAnnotations.jl](https://github.com/org-arl/AcousticAnnotations.jl)

Acoustic annotations database.

[DataDrivenAcoustics.jl](https://github.com/org-arl/DataDrivenAcoustics.jl)

This package is built upon the ideas discussed in our journal paper "Data-Aided Underwater Acoustic Ray Propagation Modeling" published on IEEE Journal of Oceanic Engineering (available online: https://ieeexplore.ieee.org/abstract/document/10224658).

[ARLToolkit.jl](https://github.com/org-arl/ARLToolkit.jl)

ARL Julia Toolkit.

[UnetSockets.jl](https://github.com/org-arl/UnetSockets.jl)

Julia UnetSocket API to connect to UnetStack.

[wignerVille.jl](https://github.com/flpf/wignerVille.jl)

An implementation of the Wigner-Ville transform in Julia. 

[RSDeltaSigmaPort.jl](https://github.com/ma-laforge/RSDeltaSigmaPort.jl)

Port of Richard Schreier's Delta Sigma Toolbox (https://www.mathworks.com/matlabcentral/fileexchange/19-delta-sigma-toolbox).

[CMDimCircuits.jl](https://github.com/ma-laforge/CMDimCircuits.jl)

Process measurement/simulation results from parametric analyses.

[EasyFFTs.jl](https://github.com/KronosTheLate/EasyFFTs.jl)

An opinionated layer on top of FFTW.jl to provide simpler FFTs for everyone.

[FourierAnalysis.jl](https://github.com/Marco-Congedo/FourierAnalysis.jl)

FourierAnalysis is a signal-processing Julia package for performing the analysis of real multivariate data (e.g., multivariate time series) in the frequency domain and in the time-frequency domain. It is based upon the DSP.jl, FFTW.jl and AbstractFFTs.jl packages.

[FIRLSFilterDesign.j](https://github.com/ltjkoomen/FIRLSFilterDesign.jl)

This is a package for linear-phase FIR filter design using weighted least-squares, written in Julia.

---

### Image processing and analysis

[JuliaImages.jl](https://juliaimages.org/stable/)

JuliaImages is a collection of packages specifically focused on image processing.

[ImageView.jl](https://github.com/JuliaImages/ImageView.jl)

An image display GUI for Julia.

[MosaicViews.jl](https://github.com/JuliaArrays/MosaicViews.jl)

When visualizing images, it is not uncommon to provide a 2D view of different image sources. For example, comparing multiple images of different sizes, getting a preview of machine learning dataset. This package aims to provide easy-to-use tools for such tasks.

[ImageTransformations.jl](https://juliaimages.org/ImageTransformations.jl/stable/#ImageTransformations.jl)

This package provides support for image resizing, image rotation, and other spatial transformations of arrays.

[ImageFiltering.jl](https://juliaimages.org/ImageFiltering.jl/stable/)

ImageFiltering supports linear and nonlinear filtering operations on arrays, with an emphasis on the kinds of operations used in image processing.

[ImageBinarization.jl](https://juliaimages.org/ImageBinarization.jl/stable/)

A Julia package containing a number of algorithms for analyzing images and automatically binarizing them into background and foreground.

[HistogramThresholding.jl](https://juliaimages.org/HistogramThresholding.jl/stable/)

A Julia package for analyzing a one-dimensional histogram and automatically choosing a threshold which partitions the histogram into two parts.

[ImageContrastAdjustment.jl](https://juliaimages.org/ImageContrastAdjustment.jl/stable/)

A Julia package for enhancing and manipulating image contrast.

[ImageDistances.jl](https://github.com/JuliaImages/ImageDistances.jl)

ImageDistances.jl aims to: follow the same API in Distances.jl, support image types, provide image-specific distances.

[ImageSegmentation.jl](https://juliaimages.org/stable/pkgs/segmentation/)

Image Segmentation is the process of partitioning the image into regions that have similar attributes.

[ImageInpainting.jl](https://github.com/JuliaImages/ImageInpainting.jl)

Image inpainting algorithms in Julia.

[ImageQualityIndexes.jl](https://github.com/JuliaImages/ImageQualityIndexes.jl)

ImageQualityIndexes provides the basic image quality assessment methods. Check the reasoning behind the code design here if you're interested in.

[ImageFeatures.jl](https://juliaimages.org/ImageFeatures.jl/stable/)

ImageFeatures is a package for identifying and characterizing "keypoints" (salient features) in images.

[ImageMorphology.jl](https://juliaimages.org/ImageMorphology.jl/stable/)

This package provides morphology operations for structure analysis and image processing.

[ImageMetadata.jl](https://github.com/JuliaImages/ImageMetadata.jl)

ImageMetadata is a simple package providing utilities for working with images that have metadata attached.

[ImageDraw.jl](https://juliaimages.org/ImageDraw.jl/stable/)

A drawing package for JuliaImages.

[OpenCV.jl](https://github.com/JuliaOpenCV/OpenCV.jl)

The Julia wrapper for Open Source Computer Vision library (OpenCV).

[CVVideo.jl](https://github.com/JuliaOpenCV/CVVideo.jl)

[CVCalib3d.jl](https://github.com/JuliaOpenCV/CVCalib3d.jl)

[CVHighGUI.jl](https://github.com/JuliaOpenCV/CVHighGUI.jl)

High-level GUI.

[CVImgCodecs.jl](https://github.com/JuliaOpenCV/CVImgCodecs.jl)

Image file reading and writing.

[CVImgProc.jl](https://github.com/JuliaOpenCV/CVImgProc.jl)

Image processing.

[CVVideoIO.jl](https://github.com/JuliaOpenCV/CVVideoIO.jl)

Media I/O.

[LibOpenCV.jl](https://github.com/JuliaOpenCV/LibOpenCV.jl)

OpenCV library dependencies.

[CVCore.jl](https://github.com/JuliaOpenCV/CVCore.jl)

OpenCV Core functionality.

[YOLO.jl](https://github.com/IanButterworth/YOLO.jl)

Currently only supports loading YOLOv2-tiny and the VOC-2007 pretrained model (pretrained on Darknet).

[Darknet.jl](https://github.com/IanButterworth/Darknet.jl)

Wrapper for https://github.com/AlexeyAB/darknet based on pre-build binaries.

[ObjectDetector.jl](https://github.com/r3tex/ObjectDetector.jl)

Object detection via YOLO in Julia. YOLO models are loaded directly from Darknet .cfg and .weights files as Flux models. Uses CUDA, if available.

[FaceDetection.jl](https://github.com/jakewilliami/FaceDetection.jl)

FaceDetection using Viola-Jones' Robust Algorithm for Object Detection.

[KernelTools.jl](https://github.com/timholy/KernelTools.jl)

Fast kernel/stencil operations in Julia.

[ANTsRegistration.jl](https://github.com/timholy/ANTsRegistration.jl)

This provides a Julia wrapper around the Advanced Normalization Tools image registration and motion correction suite.

[SpatiallyFilteredViews.jl](https://github.com/timholy/SpatiallyFilteredViews.jl)

Memory-efficient spatial filtering of image time slices.

[JpegGlitcher.jl](https://github.com/JuliaWTF/JpegGlitcher.jl)

JpegGlitcher only exports the `glitch` function. `glitch` takes an image, compresses it using the JPEG encoding, safely modifies some bytes of the compressed version and return the decoded version.

[CoherentTransformations.jl](https://github.com/JuliaWTF/CoherentTransformations.jl)

Combine CoherentNoise.jl and ImageTransformations.jl to produce interesting effects on photos.

[DeepFry.jl](https://github.com/JuliaWTF/DeepFry.jl)

Chaotic image processing tool.

[CoherentNoise.jl](https://github.com/mfiano/CoherentNoise.jl)

A comprehensive suite of coherent noise algorithms and composable tools for manipulating them.

[ImageSharpness.jl](https://github.com/bionanoimaging/ImageSharpness.jl)

Judge the image sharpness of at a certain file path location.

---

### Statistics

[StatsKit.jl](https://github.com/JuliaStats/StatsKit.jl)

This is a convenience meta-package which allows loading essential packages for statistics.

[Bootstrap.jl](https://github.com/juliangehring/Bootstrap.jl)

Statistical bootstrapping library for Julia.

[Clustering.jl](https://github.com/JuliaStats/Clustering.jl)

Clustering.jl is a Julia package for data clustering.

[Distances.jl](https://github.com/JuliaStats/Distances.jl)

A Julia package for evaluating distances(metrics) between vectors.

[HypothesisTests.jl](https://github.com/JuliaStats/HypothesisTests.jl)

This package implements several hypothesis tests in Julia.

[KernelDensity.jl](https://github.com/JuliaStats/KernelDensity.jl)

Kernel density estimators for Julia.

[NMF.jl](https://github.com/JuliaStats/NMF.jl)

A Julia package for non-negative matrix factorization (NMF).

[Loess.jl](https://github.com/JuliaStats/Loess.jl)

This is a pure Julia loess implementation, based on the fast kd-tree based approximation, implemented in the netlib loess C/Fortran code, and used by many, including in R's loess function.

[MultivariateStats.jl](https://github.com/JuliaStats/MultivariateStats.jl)

A Julia package for multivariate statistics and data analysis (e.g. dimensionality reduction).

[MixedModels.jl](https://juliastats.org/MixedModels.jl/stable/)

MixedModels.jl is a Julia package providing capabilities for fitting and examining linear and generalized linear mixed-effect models.

[StatsBase.jl](https://github.com/JuliaStats/StatsBase.jl)

StatsBase.jl is a Julia package that provides basic support for statistics. Particularly, it implements a variety of statistics-related functions, such as scalar statistics, high-order moment computation, counting, ranking, covariances, sampling, and empirical density estimation.

[StatsModels.jl](https://github.com/JuliaStats/StatsModels.jl)

This package provides common abstractions and utilities for specifying, fitting, and evaluating statistical models.

[StatsFuns.jl](https://github.com/JuliaStats/StatsFuns.jl)

Mathematical functions related to statistics.

[GLM.jl](https://github.com/JuliaStats/GLM.jl)

Linear and generalized linear models in Julia.

[Distributions.jl](https://github.com/JuliaStats/Distributions.jl)

The Distributions package provides a large collection of probabilistic distributions and related functions.

[Survival.jl](https://github.com/JuliaStats/Survival.jl)

A Julia package for performing survival analysis.

[MultivariateStats.jl](https://multivariatestatsjl.readthedocs.io/en/stable/index.html#)

MultivariateStats.jl is a Julia package for multivariate statistical analysis. It provides a rich set of useful analysis techniques, such as PCA, CCA, LDA, PLS, etc.

[LowRankModels.jl](https://github.com/madeleineudell/LowRankModels.jl)

LowRankModels.jl is a Julia package for modeling and fitting generalized low rank models (GLRMs). GLRMs model a data array by a low rank matrix, and include many well known models in data analysis, such as principal components analysis (PCA), matrix completion, robust PCA, nonnegative matrix factorization, k-means, and many more.

[Discreet.jl](https://github.com/cynddl/Discreet.jl)

Discreet is a small opinionated toolbox to estimate entropy and mutual information from discrete samples. It contains methods to adjust results and correct over- or under-estimations.

[InformationMeasures.jl](https://github.com/Tchanders/InformationMeasures.jl)

Entropy and mutual information.

[Lasso.jl](https://github.com/JuliaStats/Lasso.jl)

Lasso.jl is a pure Julia implementation of the glmnet coordinate descent algorithm for fitting linear and generalized linear Lasso and Elastic Net models.

[MultipleTesting.jl](https://github.com/juliangehring/MultipleTesting.jl)

The MultipleTesting package offers common algorithms for p-value adjustment and combination as well as the estimation of the proportion π₀ of true null hypotheses.

[ConjugatePriors.jl](https://github.com/JuliaStats/ConjugatePriors.jl)

A Julia package to support conjugate prior distributions.

[PGM.jl](https://github.com/JuliaStats/PGM.jl)

PGM.jl is a Julia framework for probabilistic graphical models.

[QuantEcon.jl](https://github.com/QuantEcon/QuantEcon.jl)

QuantEcon.jl is a Julia package for doing quantitative economics.

[FreqTables.jl](https://github.com/nalimilan/FreqTables.jl)

This package allows computing one- or multi-way frequency tables (a.k.a. contingency or pivot tables) from any type of vector or array.

[OnlineStats.jl](https://github.com/joshday/OnlineStats.jl)

OnlineStats does statistics and data visualization for big/streaming data via online algorithms. Each algorithm: 1) processes data one observation at a time. 2) uses O(1) memory.

[OnlineStatsBase.jl](https://github.com/joshday/OnlineStatsBase.jl)

This package defines the basic types and interface for OnlineStats.

[VariationalOnlineStats.jl](https://github.com/joshday/OnlineStats.jl)

Online estimate of the median by variational/Bayesian methods.

[EmpiricalRisks.jl](https://github.com/lindahua/EmpiricalRisks.jl)

This Julia package provides a collection of predictors and loss functions, mainly to support the implementation of (regularized) empirical risk minimization methods.

[Regression.jl](https://github.com/lindahua/Regression.jl)

This package is based on EmpiricalRisks, and provides a set of algorithms to perform regression analysis. This package supports all regression problems that can be formulated as regularized empirical risk minimization.

[LinearMixingModels.jl](https://github.com/invenia/LinearMixingModels.jl)

A package for implementing Instantaneous Linear Mixing Models (ILMMs) and Orthogonal Instantaneous Linear Mixing Models (OILMMs) using the AbstractGPs interface.

[KeyedDistributions.jl](https://github.com/invenia/KeyedDistributions.jl)

KeyedDistributions.jl provides thin wrappers of Distribution and Sampleable, to store keys and dimnames for the variates.

[TrackedDistributions.jl](https://github.com/invenia/TrackedDistributions.jl)

TrackedDistributions extends Distributions such that it can work with TrackedArrays.

[RobustModels.jl](https://github.com/getzze/RobustModels.jl)

This package defines robust linear models using the interfaces from StatsBase.jl and StatsModels.jl. It defines an `AbstractRobustModel` type as a subtype of `RegressionModel` and it defines the methods from the statistical model API like `fit`/`fit!`.

[RollingFunctions.jl](https://github.com/JeffreySarnoff/RollingFunctions.jl)

Roll a [weighted] function or run a statistic along windowed data.

[Accumulators.jl](https://github.com/JeffreySarnoff/Accumulators.jl)

Support for incremental statistics.

[RollOverData.jl](https://github.com/JeffreySarnoff/RollOverData.jl)

A more capable `RollingFunctions.jl`. 

[WindowedFunctions.jl](https://github.com/JeffreySarnoff/WindowedFunctions.jl)

Successor to RollingFunctions.jl.

[IncrementalAccumulators.jl](https://github.com/JeffreySarnoff/IncrementalAccumulators.jl)

Performant accumulators that accept each [k] successive value[s] from a data stream.

[BitsFields.jl](https://github.com/JeffreySarnoff/BitsFields.jl)

Use adjacent bits as a named bitfield. Use multiple bitfields in distinct roles or as primary elements.

[Stemplot.jl](https://github.com/alexhallam/Stemplot.jl)

Stemplots in Julia.

[OnlineStats.jl](https://github.com/joshday/OnlineStats.jl)

Online Algorithms for Statistics, Models, and Big Data Viz.

[PowerAnalysis.jl](https://github.com/johnmyleswhite/PowerAnalysis.jl)

Tools for analyzing the power of proposed statistical designs.

[RegressionTables.jl](https://github.com/jmboehm/RegressionTables.jl)

This package provides publication-quality regression tables for use with FixedEffectModels.jl and GLM.jl, as well as any package that implements the RegressionModel abstraction.

[Fastcluster.jl](https://github.com/jmboehm/Fastcluster.jl)

Julia wrapper to Daniel Muellner's fastcluster library for hierarchical clustering.

[Alpaca.jl](https://github.com/jmboehm/Alpaca.jl)

Julia wrapper for the alpaca R library to estimate generalized linear model with high-dimensional fixed effects.

[StatsPlots.jl](https://github.com/JuliaPlots/StatsPlots.jl)

This package is a drop-in replacement for Plots.jl that contains many statistical recipes for concepts and types introduced in the JuliaStats organization.

[Coefplots.jl](https://github.com/caibengbu/Coefplots.jl)

Coefplots aims to make available in Julia (part of) the functionalities of the Stata command coefplot. Coefplots is built on PGFPlotsX.

[GLFixedEffectModels.jl](https://github.com/jmboehm/GLFixedEffectModels.jl)

This package estimates generalized linear models with high dimensional categorical variables. It builds on Matthieu Gomez's FixedEffects.jl, Amrei Stammann's Alpaca, and Sergio Correia's ppmlhdfe.

[Vcov.jl](https://github.com/FixedEffects/Vcov.jl)

This package should be used as a backend by package developers. It allows developers to add a `::CovarianceEstimator` argument in the `fit` method defined by their package.

[FixedEffectModels.jl](https://github.com/FixedEffects/FixedEffectModels.jl)

This package estimates linear models with high dimensional categorical variables and/or instrumental variables.

[FixedEffects.jl](https://github.com/FixedEffects/FixedEffects.jl)

This package solves least squares problem with high dimensional fixed effects. For a matrix `D` of high dimensional fixed effects, it finds `b` and `ϵ` such that `y = D'b + ϵ` with `E[Dϵ] = 0`. It is the back end for the package FixedEffectModels.jl, that estimates linear models with high-dimensional fixed effect.

[InteractiveFixedEffectModels.jl](https://github.com/FixedEffects/InteractiveFixedEffectModels.jl)

This package implements a novel, fast and robust algorithm to estimate interactive fixed effect models.

[RDatasets.jl](https://github.com/JuliaStats/RDatasets.jl)

The RDatasets package provides an easy way for Julia users to experiment with most of the standard data sets that are available in the core of R as well as datasets included with many of R's most popular packages. This package is essentially a simplistic port of the Rdatasets repo created by Vincent Arelbundock, who conveniently gathered data sets from many of the standard R packages in one convenient location on GitHub at https://github.com/vincentarelbundock/Rdatasets

[Turing.jl](https://github.com/TuringLang/Turing.jl)

Bayesian inference with probabilistic programming.

[AlphaStableDistributions.jl](https://github.com/org-arl/AlphaStableDistributions.jl)

Alpha stable and sub-Gaussian distributions in Julia.

[StateSpaceModels.jl](https://github.com/LAMPSPUC/StateSpaceModels.jl)

StateSpaceModels.jl is a package for modeling, forecasting, and simulating time series in a state-space framework. Implementations were made based on the book "Time Series Analysis by State Space Methods" (2012) by James Durbin and Siem Jan Koopman. The notation of the variables in the code also follows the book.

[SimpleAnova.jl](https://github.com/JOfTheAncientGermanSpear/SimpleAnova.jl)

Simple ANOVA In Julia.

[ANOVA.jl](https://github.com/grero/ANOVA.jl)

Basic two way ANOVA and repeated measures ANOVA in Julia.

[ANOVA.jl](https://github.com/ZaneMuir/ANOVA.jl)

One-way ANOVA: `ANOVA.anova`, two-way ANOVA: `ANOVA.anova2`, one-way repeated measures ANOVA: `ANOVA.rm_anova`,     two-way repeated measures ANOVA: `ANOVA.rm_anova2`.

[SimpleANOVA.jl](https://github.com/BioTurboNick/SimpleANOVA.jl)

Analysis of Variance for Julia, the old-fashioned way.

[QPIM.jl](https://github.com/yufongpeng/QPIM.jl)

Quality and Process improvement from Minitab.

[AnovaBase.jl](https://github.com/yufongpeng/AnovaBase.jl)

AnovaBase.jl is a Julia package providing a simple framework for Analysis of Variance (ANOVA) on various types of Julia statistical models. It is similar to function ANOVA in R.

[AnovaMixedModels.jl](https://github.com/yufongpeng/AnovaMixedModels.jl)

Conduct one-way and multi-way ANOVA in Julia with MixedModels.jl

[AnovaFixedEffectModels.jl](https://github.com/yufongpeng/AnovaFixedEffectModels.jl)

Conduct one-way and multi-way ANOVA in Julia with FixedEffectModels.jl

[AnovaGLM.jl](https://github.com/yufongpeng/AnovaGLM.jl)

Conduct one-way and multi-way ANOVA in Julia with GLM.jl

[ANOVA.jl](https://github.com/marcpabst/ANOVA.jl)

Calculate ANOVA tables for linear models. If no `anovatype` argument is provided, a type III ANOVA will be calculated. Type I and II are also supported for compatibility. Support for mixed models and a more convenient way to create ANOVAs (similar to the ez package in R) is planned.

[LongMemory.jl](https://github.com/everval/LongMemory.jl)

LongMemory.jl is a package for time series long memory modelling in Julia.

[Diagonalizations.jl](https://github.com/Marco-Congedo/Diagonalizations.jl)

Diagonalization procedures for Julia (PCA, Whitening, MCA, gMCA, CCA, gCCA, CSP, CSTP, AJD, mAJD).

[MeasurementErrorModels.jl](https://github.com/Mattriks/MeasurementErrorModels.jl)

A julia package for working with Measurement Error Models. This package was developed to calibrate Measurement Error Proxy System Models (MEPSMs).

[PredictableComponents.jl](https://github.com/Mattriks/PredictableComponents.jl)

Linear and nonlinear predictable components from multivariate data.

[RobustModels.jl](https://github.com/getzze/RobustModels.jl)

This package defines robust linear models using the interfaces from StatsBase.jl and StatsModels.jl. It defines an AbstractRobustModel type as a subtype of RegressionModel and it defines the methods from the statistical model API like `fit`/`fit!`.

[LinearRegressionKit.jl](https://github.com/ericqu/LinearRegressionKit.jl)

LinearRegressionKit.jl implements linear regression using the least-squares algorithm (relying on the sweep operator). This package is in the beta stage. Hence it is likely that some bugs exist. Furthermore, the API might change in future versions.

[RegressionTables.jl](https://github.com/jmboehm/RegressionTables.jl)

This package provides publication-quality regression tables for use with FixedEffectModels.jl, GLM.jl, GLFixedEffectModels.jl and MixedModels.jl, as well as any package that implements the RegressionModel abstraction.

[Kirstine.jl/](https://sr.ht/~lsandig/Kirstine.jl/)

A Julia package for Bayesian optimal experimental design with nonlinear regression models.

[LSMH.jl](https://github.com/harryscholes/LSMH.jl)

Julia package for similarity detection by Locality-Sensitive MinHash.

[EffectSizes.jl](https://github.com/harryscholes/EffectSizes.jl)

EffectSizes.jl is a Julia package for effect size measures. Confidence intervals are assigned to effect sizes using the Normal distribution or by bootstrap resampling.

[EventStudyInteracts.jl](https://github.com/xiaobaaaa/EventStudyInteracts.jl)

This package is a Julia replication of the Stata package [`eventstudyinteract`](https://github.com/lsun20/EventStudyInteract).

[LinComs.jl](https://github.com/xiaobaaaa/LinComs.jl)

Linear combinations of parameters for FixedEffectModels.jl and EventStudyInteracts.jl like Stata package `lincom`.

[MannKendall.jl](https://github.com/mmhs013/MannKendall.jl)

MannKendall.jl is a pure julia package for Mann-Kendall trend test.

[Pingouin.jl](https://github.com/clementpoiret/Pingouin.jl)

Pingouin is designed for users who want simple yet exhaustive stats functions.

[RobustMeans.jl](https://github.com/dmetivie/RobustMeans.jl)

This package aim is to implement in Julia some robust mean estimators (one-dimensional for now).

[LinRegOutliers](https://github.com/jbytecode/LinRegOutliers)

A Julia package for outlier detection in linear regression.

[RegressionFormulae.jl](https://github.com/kleinschmidt/RegressionFormulae.jl)

Extended StatsModels.jl `@formula` syntax for regression modeling.

[Lathe.jl](https://github.com/smartinsightsfromdata/Lathe.jl)

Lathe.jl is an all-in-one package for predictive modeling in Julia. It comes packaged with a Stats Library, Preprocessing Tools, Distributions, Machine-Learning Models, and Model Validation.

[MixedLRMoE.jl](https://github.com/sparktseung/MixedLRMoE.jl)

MixedLRMoE.jl is an implementation of the Mixed Logit-Reduced Mixture-of-Experts (Mixed LRMoE) model in julia.

[Effects.jl](https://github.com/beacon-biosignals/Effects.jl)

Effects Prediction for Linear and Generalized Linear models.

[GaussianProcesses.jl](https://github.com/STOR-i/GaussianProcesses.jl)

A Gaussian Processes package for Julia.

[StructuralEquationModels.jl](https://github.com/StructuralEquationModels/StructuralEquationModels.jl)

A fast and flexible Structural Equation Modelling Framework.

[AutoDiffSEM.jl](https://github.com/StructuralEquationModels/AutoDiffSEM.jl)

Automatic differentiation support for structural equation models.

[SnllsSEM.jl](https://github.com/StructuralEquationModels/SnllsSEM.jl)

Relies on StructuralEquationModels.jl to implement SNLLS (= separable nonlinear least squares) estimation of structural equation models. For more background, see https://psyarxiv.com/b6uxm/.

[ProximalSEM.jl](https://github.com/StructuralEquationModels/ProximalSEM.jl)

This is a package for regularized structural equation modeling. It connects StructuralEquationModels.jl to ProximalAlgorithms.jl.

[SparseRegressionAlgorithms.jl](https://github.com/joshday/SparseRegressionAlgorithms.jl)

Low-level interface for sparse regression.

[StandardizedMatrices.jl](https://github.com/joshday/StandardizedMatrices.jl)

Statisticians often work with standardized matrices. If x is a data matrix with observations in rows, we want to work with `z = StatsBase.zscore(x, 1)`. This package defines a `StandardizedMatrix` type that treats a matrix as standardized without copying or changing data in place.

[RegularizedGLM.jl](https://github.com/joshday/RegularizedGLM.jl)

Experiments using MM algorithms for regularized generalized linear models.

[PenalizedRegression.jl](https://github.com/joshday/PenalizedRegression.jl)

Experimental changes to SparseRegression.jl.

[CorrelationView.jl](https://github.com/joshday/CorrelationView.jl)

Simple visualizations of correlation matrices.

[DataGenerator.jl](https://github.com/joshday/DataGenerator.jl)

Generate data for generalized linear models and more.

[Reproducible.jl](https://github.com/joshday/Reproducible.jl)

Reproducible.jl is a lightweight package for creating reproducible reports in Julia.

[Telperion.jl](https://github.com/joshday/Telperion.jl)

Parse Statistical Formulas into Feature Columns.

[GTrends.jl](https://github.com/joshday/GTrends.jl)

Julia wrapper for the gtrendsR package.

[SparseRegression.jl](https://github.com/joshday/SparseRegression.jl)

Statistical Models with Regularization in Pure Julia.

[GeneralizedLeastSquares.jl](https://github.com/joshday/GeneralizedLeastSquares.jl)

High-performance Generalized LS solvers.

[NISTStatisticalReferenceData.jl](https://github.com/joshday/NISTStatisticalReferenceData.jl)

This package provides utilities for working with the datasets and certified values provided by NIST's Statistical Reference Datasets (StRD).

[SweepOperator.jl](https://github.com/joshday/SweepOperator.jl)

The symmetric sweep operator is a powerful tool in computational statistics with uses in stepwise regression, conditional multivariate normal distributions, MANOVA, and more.

[AverageShiftedHistograms.jl](https://github.com/joshday/AverageShiftedHistograms.jl)

An Averaged Shifted Histogram (ASH) is essentially Kernel Density Estimation over a fine-partition histogram. ASH uses constant memory, can be constructed on-line via O(nbins) updates, and lets you estimate densities for arbitrarily big data.

[StepwiseRegression.jl](https://codeberg.org/AdamWysokinski/StepwiseRegression.jl)

Perform forward or backward stepwise regression.

[BlindingIndex.jl](https://codeberg.org/AdamWysokinski/BlindingIndex.jl)

Compute James (1996) and Bang (2004) Blinding Indices.

[HighDimMixedModels.jl](https://github.com/solislemuslab/HighDimMixedModels.jl)

HighDimMixedModels.jl is a package for fitting regularized linear mixed-effect models on high-dimensional omics data. These models can be used to analyze hierarchical, high dimensional data, especially useful for situations in which the number of predictors exceeds the number of samples (p > n).

[HurdleDMR.jl](https://github.com/AsafManela/HurdleDMR.jl)

HurdleDMR.jl is a Julia implementation of the Hurdle Distributed Multinomial Regression (HDMR).

---

### Neuroscience

[EDFPlus.jl](https://github.com/wherrera10/EDFPlus.jl)

Julia for handling BDF+ and EDF+ EEG and similar signal data files.

[EEG.jl](https://github.com/JuliaPackageMirrors/EEG.jl)

Process EEG files in Julia.

[Psychophysics.jl](https://github.com/sam81/Psychophysics.jl)

Miscellaneous Julia utilities for psychophysics research.

[Synchrony.jl](https://github.com/simonster/Synchrony.jl)

This package implements efficient multitaper and continuous wavelet transforms, along with the following transform statistics most of which operate on pairs of signals: Power spectral density (PowerSpectrum), Power spectral density variance (PowerSpectrumVariance), Cross spectrum (CrossSpectrum), Coherence (Coherence for the absolute value, Coherency for the complex value), Phase locking value, a.k.a. the mean resultant vector length or R̄ (PLV), Pairwise phase consistency, a.k.a. the unbiased estimator of R̄^2 (PPC), Phase lag index (PLI), Unbiased squared phase lang index (PLI2Unbiased), Weighted phase lag index (WPLI), Debiased squared weighted phase lag index (WPLI2Debiased),    Jammalamadaka circular correlation coefficient (JCircularCorrelation), Jupp-Mardia squared circular correlation coefficient (JMCircularCorrelation), Hurtado et al. modulation index (phase-amplitude coupling) (HurtadoModulationIndex). Additionally, the following point-field measures are implemented: Point-field coherence (pfcoherence), Point-field PLV (pfplv), Point-field PPC, variants 0, 1, and 2 (pfppc0, pfppc1, pfppc2). And the following point-point measures: Point-point cross correlation (pfxcorr).

[Neuroimaging.jl](https://rob-luke.github.io/Neuroimaging.jl/dev/)

Process neuroimaging data using the Julia language.

[NIRX.jl](https://github.com/rob-luke/NIRX.jl)

Read NIRX functional near-infrared spectroscopy files in Julia.

[BDF.jl](https://github.com/sam81/BDF.jl)

BDF.jl is a Julia module to read/write BIOSEMI 24-bit BDF files (used for storing electroencephalographic recordings).

[EDF.jl](https://beacon-biosignals.github.io/EDF.jl/stable/)

Read and write European Data Format (EDF/EDF+) and BioSemi Data Format (BDF) files in Julia.

[AuditoryStimuli.jl](https://github.com/rob-luke/AuditoryStimuli.jl)

Generate auditory stimuli for real-time applications. Specifically, stimuli that are used in auditory research.

[NeuroAnalyzer.jl](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl)

Toolbox for analyzing neurophysiological data with Julia.

[Unfold.jl](https://github.com/unfoldtoolbox/Unfold.jl)

Toolbox to perform linear / GAM / hierarchical / deconvolution regression on biological signals.

[UnfoldSim.jl](https://github.com/unfoldtoolbox/UnfoldSim.jl)

A package to simulate single timeseries model-based ERPs, fMRI activity, pupil dilation etc. If you have one channel, it is a timeseries of (overlapping) event-related activity and some noise - you might have fun here!

[UnfoldMakie.jl](https://github.com/unfoldtoolbox/UnfoldMakie.jl)

UnfoldMakie allows many visualizations for ERP and "Unfolded"-models. Building on the Unfold and Makie, it grants users highly customizable plots.

[ClusterDepth.jl](https://github.com/s-ccs/ClusterDepth.jl)

Fast implementation of the ClusterDepth multiple comparison algorithm from Frossard and Renaud Neuroimage 2022. This is especially interesting to EEG signals. Currently only acts on a single channel/timeseries. Multichannel as discussed in the paper is the next step.

[HeartBeats.jl](https://github.com/cbrnr/HeartBeats.jl)

HeartBeats.jl provides a heartbeat detector based on the approach described by Pan & Tompkins (1985).

[XDF.jl](https://github.com/cbrnr/XDF.jl)

XDF.jl is an importer for XDF files written in Julia.

[LslTools.jl](https://github.com/s-ccs/LslTools.jl)

Tools to import LSL/XDF Data.

[AllenBrain.jl](https://github.com/JuliaNeuroscience/AllenBrain.jl)

Accessing and analyzing data from the Allen Brain Atlas.

[NeuroCore.jl](https://github.com/JuliaNeuroscience/NeuroCore.jl)

Core properties for interacting with neuroscience data in Julia.

[NeuroGraphs.jl](https://github.com/JuliaNeuroscience/NeuroGraphs.jl)

Graph theory for neuroscience data.

[NeuroStudies.jl](https://github.com/JuliaNeuroscience/NeuroStudies.jl)

Package for organizing neuroscience studies. Design is highly motivated by the Brain Imaging Data Structure (BIDS) specification.

[SpikingNeuralNetworks.jl](https://github.com/AStupidBear/SpikingNeuralNetworks.jl)

Julia Spiking Neural Network Simulator.

[SpikingNeuralNetworks_.jl](https://github.com/russelljjarvis/SpikingNeuralNetworks_.jl)

Julia has enough tools to support fitting spiking neural network models to data. Python speed necessitates external simulators to do network simulation. As much as possible it would be nice to do fast, efficient data fitting of spike trains to network models in one language, lets try to do that here.

[SpikeSynchrony.jl](https://github.com/JuliaNeuroscience/SpikeSynchrony.jl)

Julia implementations for measuring distances, synchrony and correlation between spike trains.

[Weber.jl](https://github.com/haberdashPI/Weber.jl)

Weber is a Julia package that can be used to generate simple psychology experiments that present visual and auditory stimuli at precise times.

[WeberDAQmx.jl](https://github.com/haberdashPI/WeberDAQmx.jl)

This Julia package extends Weber, to allow triggers to be sent during EEG recording using the DAQmx api. It is a simple extension which translates the `record` codes, during a call to record, to digital trigger events.

[WeberCedrus.jl](https://github.com/haberdashPI/WeberCedrus.jl)

This Julia package extends Weber, to enable the use of Cedrus response-pad input. It adds a series of new keys, ranging from `key":cedrus0:"` to `key":cedrus19:"`. You can see which key is which by pressing the buttons while running the following code in julia.

[IntrinsicTimescales.jl](https://github.com/duodenum96/IntrinsicTimescales.jl)

IntrinsicTimescales.jl is a software package for estimating Intrinsic Neural Timescales (INTs) from time-series data. It uses model-free methods (ACW-50, ACW-0, fitting an exponential decay function etc.) and simulation-based methods (adaptive approximate Bayesian computation: aABC, automatic differentiation variational inference: ADVI) to estimate INTs.

---

### Web technologies

[WebIO.jl](https://github.com/JuliaGizmos/WebIO.jl)

WebIO provides a simple abstraction for displaying and interacting with web content.

[GenieFramework.jl](https://github.com/GenieFramework/GenieFramework.jl)

Meta package for Genie reactive apps.

[Genie.jl](https://github.com/GenieFramework/Genie.jl)

Genie is a full-stack web framework that provides a streamlined and efficient workflow for developing modern web applications. It builds on Julia's strengths (high-level, high-performance, dynamic, JIT compiled), exposing a rich API and a powerful toolset for productive web development.

[GenieUI.jl](https://github.com/GenieFramework/GenieUI.jl)

Higher level UI elements for Genie apps.

[GenieAutoreload.jl](https://github.com/GenieFramework/GenieAutoreload.jl)

GenieAutoReload monitors the indicated files and folders (recursively) and automatically recompiles the Julia code and reloads the corresponding browser window.

[GeniePackageManager.jl](https://github.com/GenieFramework/GeniePackageManager.jl)

Package Management solution for Genie.jl and GenieBuilder Apps.

[GeniePlugins.jl](https://github.com/GenieFramework/GeniePlugins.jl)

Genie plugin providing functionality for creating and working with Genie plugins.

[GenieAuthentication.jl](https://github.com/GenieFramework/GenieAuthentication.jl)

Authentication plugin for Genie.jl.

[GenieAuthorisation.jl](https://github.com/GenieFramework/GenieAuthorisation.jl)

Role Based Authorisation (RBA) plugin for Genie.jl.

[GenieCache.jl](https://github.com/GenieFramework/GenieCache.jl)

Abstract package for Genie caching.

[GenieSession.jl](https://github.com/GenieFramework/GenieSession.jl)

Sessions support for Genie apps.

[GenieSessionFileSession.jl](https://github.com/GenieFramework/GenieSessionFileSession.jl)

File system based session adapter for GenieSession.jl.

[GenieCacheFileCache.jl](https://github.com/GenieFramework/GenieCacheFileCache.jl)

File system cache adapter for GenieCache.jl.

[GenieDevTools.jl](https://github.com/GenieFramework/GenieDevTools.jl)

GenieDevTools.

[GenieDeploy.jl](https://github.com/GenieFramework/GenieDeploy.jl)

GenieDeploy.

[GenieDeployHeroku.jl](https://github.com/GenieFramework/GenieDeployHeroku.jl)

GenieDeployHeroku.

[GenieDeployJuliaHub.jl](https://github.com/GenieFramework/GenieDeployJuliaHub.jl)

GenieDeployJuliaHub.

[DockerizedGBApp](https://github.com/GenieFramework/DockerizedGBApp)

Genie on Docker.

[SearchLight.jl](https://github.com/GenieFramework/SearchLight.jl)

SearchLight is the ORM layer of Genie.jl, the high-performance high-productivity Julia web framework.

[SearchLightSerializerJSON.jl](https://github.com/GenieFramework/SearchLightSerializerJSON.jl)

JSON data serializer for SearchLight.

[SearchLightSQLite.jl](https://github.com/GenieFramework/SearchLightSQLite.jl)

SQLite adapter for SearchLight.

[SearchLightPostgreSQL.jl](https://github.com/GenieFramework/SearchLightPostgreSQL.jl)

Postgres adapter for SearchLight.

[SearchLightMySQL.jl](https://github.com/GenieFramework/SearchLightMySQL.jl)

MySQL adapter for SearchLight.

[SearchLightOracle.jl](https://github.com/GenieFramework/SearchLightOracle.jl)

The project fullfills the API of SearchLight. The adpater can be used for communicating with Oracle databases.

[Stipple.jl](https://github.com/GenieFramework/Stipple.jl)

Stipple is a reactive UI library for building interactive data applications in pure Julia. It uses Genie.jl (on the server side) and Vue.js (on the client). Stipple uses a high performance MVVM architecture, which automatically synchronizes the state two-way (server -> client and client -> server) sending only JSON data over the wire. The Stipple package provides the fundamental communication layer, extending Genie's HTML API with a reactive component.

[StippleUI.jl](https://github.com/GenieFramework/StippleUI.jl)

StippleUI is a library of reactive UI elements for Stipple.jl.

[StippleCharts.jl](https://github.com/GenieFramework/StippleCharts.jl)

StippleCharts is a library of reactive charts for Stipple.jl.

[StipplePlotly.jl](https://github.com/GenieFramework/StipplePlotly.jl)

Embedding Plotly Charts in Stipple.

[StipplePlotlyExport.jl](https://github.com/GenieFramework/StipplePlotlyExport.jl)

StipplePlotlyExport.

[StippleLatex.jl](https://github.com/GenieFramework/StippleLatex.jl)

StippleLatex.

[SwagUI.jl](https://github.com/GenieFramework/SwagUI.jl)

Want to use Swagger UI in Julia? This package has your back!

[SwaggerMarkdown.jl](https://github.com/GenieFramework/SwaggerMarkdown.jl)

Swagger Markdown allows you to generate swagger.json for API documentation from the julia source code. The package uses marco to process the markdown that contains an API endpoint's documentation. The markdowon needs to follow the paths described by the OpenAPI Specification (v3, v2), and in YAML format.

[Franklin.jl](https://github.com/tlienart/Franklin.jl)

Franklin is a simple static site generator (SSG) oriented towards technical blogging (code, maths, ...), flexibility and extensibility. The base syntax is plain markdown with a few extensions such as the ability to define and use LaTeX-like commands in or outside of maths environments and the possibility to evaluate code blocks on the fly.

[FranklinTemplates.jl](https://github.com/tlienart/FranklinTemplates.jl)

Templates for Franklin, the static-site generator in Julia. Most of these templates are adapted from existing, popular templates with minor modifications to accommodate Franklin's content.

[Mux.jl](https://github.com/JuliaWeb/Mux.jl)

Mux.jl gives your Julia web services some closure. Mux allows you to define servers in terms of highly modular and composable components called middleware, with the aim of making both simple and complex servers as simple as possible to throw together.

[Tachyons.jl](https://github.com/JuliaGizmos/Tachyons.jl)

A wrapper for Tachyons CSS framework.

[Vue.jl](https://github.com/JuliaGizmos/Vue.jl)

A Julia wrapper for Vue.js. It uses WebIO to load JavaScript and to do Julia to JS communication.

[Knockout.jl](https://github.com/JuliaGizmos/Knockout.jl)

A Julia wrapper for Knockout.js. It uses WebIO to load JavaScript and to do Julia to JS communication.

[LiveServer.jl](https://github.com/tlienart/LiveServer.jl)

This is a simple and lightweight development web-server written in Julia, based on HTTP.jl. It has live-reload capability, i.e. when modifying a file, every browser (tab) currently displaying the corresponding page is automatically refreshed.

[LibCURL.jl](https://github.com/JuliaWeb/LibCURL.jl)

Julia wrapper for libCURL.

[Oxygen.jl](https://github.com/OxygenFramework/Oxygen.jl)

Oxygen is a micro-framework built on top of the HTTP.jl library. Breathe easy knowing you can quickly spin up a web server with abstractions you're already familiar with.

[SimpleWebsockets.jl](https://github.com/citkane/SimpleWebsockets.jl)

A flexible, powerful, high level interface for Websockets in Julia. Provides a SERVER and CLIENT.

[WebAssets.jl](https://github.com/joshday/WebAssets.jl)

WebAssets provides a simple API for managing local versions of files based on URLs.

[HTMX.jl](https://github.com/joshday/HTMX.jl)

Using Julia to write https://htmx.org

[TailwindCSS.jl](https://github.com/joshday/TailwindCSS.jl)

Use the TailwindCSS CLI from Julia.

[Webviews.jl](https://github.com/sunoru/Webviews.jl)

Julia implementation of webview, a tiny library for creating web-based desktop GUIs.

---

### Networking

[SSHAgentSetup.jl](https://github.com/felipenoris/SSHAgentSetup.jl)

A tool to setup `ssh-agent`.

[FTPClient.jl](https://github.com/invenia/FTPClient.jl)

A Julia FTP client using LibCURL supporting FTP and FTP over SSL.

[FTPServer.jl](https://github.com/invenia/FTPServer.jl)

A Julia interface for running a test FTP server with pyftpdlib.

[DirectSockets.jl](https://github.com/org-arl/DirectSockets.jl)

A light-weight high-performance UDP sockets library.

[SMTPClient.jl](https://github.com/aviks/SMTPClient.jl)

A CURL based SMTP client with fairly low level API. It is useful for sending emails from within Julia code. Depends on LibCURL.jl.

---

### Audio

[WAV.jl](https://github.com/dancasimiro/WAV.jl)

This is a Julia package to read and write the WAV audio file format.

[PortAudio.jl](https://github.com/JuliaAudio/PortAudio.jl)

PortAudio.jl is a wrapper for libportaudio, which gives cross-platform access to audio devices. It is compatible with the types defined in SampledSignals.jl. It provides a `PortAudioStream` type, which can be read from and written to.

[SampledSignals.jl](https://github.com/JuliaAudio/SampledSignals.jl)

SampledSignals is a collection of types intended to be used on multichannel sampled signals like audio or radio data, EEG signals, etc., to provide better interoperability between packages that read data from files or streams, DSP packages, and output and display packages.

[LibSndFile.jl](https://github.com/JuliaAudio/LibSndFile.jl)

LibSndFile.jl is a wrapper for libsndfile, and supports a wide variety of file and sample formats. The package uses the FileIO `load` and `save` interface to automatically figure out the file type of the file to be opened, and the file contents are represented as a `SampleBuf`. For streaming I/O we support FileIO's `loadstreaming` and `savestreaming` functions as well. The results are represented as `SampleSource` (for reading), or `SampleSink` (for writing) subtypes. These buffer and stream types are defined in the SampledSignals package.

[RingBuffers.jl](https://github.com/JuliaAudio/RingBuffers.jl)

This package provides the `RingBuffer` type, which is a circular, fixed-size multi-channel buffer.

[MIDI.jl](https://github.com/JuliaMusic/MIDI.jl)

MIDI.jl is a complete Julia package for reading and writing MIDI data.

[Mplay.jl](https://github.com/JuliaMusic/Mplay.jl)

Mplay is a full functional MIDI player written in pure Julia. It reads Standard MIDI Files (SMF) and sends them to MIDI devices (or software synthesizers) while giving visual feedback.

[AudioSchedules.jl](https://github.com/JuliaMusic/AudioSchedules.jl)

AudioSchedules allows you to schedule a sequence of overlapping audio synthesizers. Critically, it allows you to do this without lags or gaps.

[MusicProcessing.jl](https://github.com/JuliaMusic/MusicProcessing.jl)

MusicProcessing.jl is a music and audio processing library for Julia, inspired by librosa. It is not feature complete and in a very early stage of development.

[MusicVisualizations.jl](https://github.com/JuliaMusic/MusicVisualizations.jl)

Providing music-related visualization built on top of the packages of JuliaMusic.

[MusicManipulations.jl](https://github.com/JuliaMusic/MusicManipulations.jl)

Manipulate music data (translate/transpose etc). Advanced music data extraction. Quantize/classify notes. Take your music practice to the next level. And more!

[MusicXML.jl](https://github.com/JuliaMusic/MusicXML.jl)

Powerful MusicXML reading and writing package for Julia.

[PianoFingering.jl](https://github.com/JuliaMusic/PianoFingering.jl)

PianoFingering.jl is an automatic fingering generator for piano scores, written in the Julia language.

---

### Geospatial

[GMT.jl](https://github.com/GenericMappingTools/GMT.jl)

GMT is a toolbox for Earth, Ocean, and Planetary Science. Besides its own map projections implementation and mapping capabilities it wraps the GDAL library which gives access to most of of raster and vector formats used in geospatial world.

[Geodesy.jl](https://github.com/JuliaGeo/Geodesy.jl)

Geodesy is a Julia package for working with points in various world and local coordinate systems.

[ASCIIrasters.jl](https://github.com/JuliaGeo/ASCIIrasters.jl)

Simple read and write functions for ASCII raster files.

[Proj.jl](https://github.com/JuliaGeo/Proj.jl)

A simple Julia wrapper around the PROJ cartographic projections library.

[GDAL.jl](https://github.com/JuliaGeo/GDAL.jl)

Julia wrapper for GDAL - Geospatial Data Abstraction Library. This package is a binding to the C API of GDAL/OGR. It provides only a C style usage, where resources must be closed manually, and datasets are pointers.

[GeoJSON.jl](https://juliageo.org/GeoJSON.jl/stable/)

Read GeoJSON files using JSON3.jl, and provide the Tables.jl interface.

[GeoParquet.jl](https://github.com/JuliaGeo/GeoParquet.jl)

Adding `geospatial` data to Parquet. Follows the GeoParquet v0.4 spec.

[GeoInterface.jl](https://juliageo.org/GeoInterface.jl/stable/)

An interface for geospatial vector data in Julia.

[LibGEOS.jl](https://github.com/JuliaGeo/LibGEOS.jl)

LibGEOS is a package for manipulation and analysis of planar geometric objects, based on the libraries GEOS (the engine of PostGIS) and JTS (from which GEOS is ported).

[GeoDatasets.jl](https://juliageo.org/GeoDatasets.jl/stable/)

The aim of this package is to give access to common geographics datasets.

[DimensionalArrayTraits.jl](https://github.com/JuliaGeo/DimensionalArrayTraits.jl)

Abstract base package for dimensional arrays and their specific traits.

[Shapefile.jl](https://github.com/JuliaGeo/Shapefile.jl)

This library supports reading ESRI Shapefiles in pure Julia.

[GADM.jl](https://github.com/JuliaGeo/GADM.jl)

This package provides polygons/multipolygons for all countries and their sub-divisions from the GADM dataset. It fetches the data dynamically from the officially hosted database using DataDeps.jl and provides a minimal wrapper API to get the coordinates of the requested geometries.

[CFTime.jl](https://juliageo.org/CFTime.jl/stable/)

`CFTime` encodes and decodes time units conforming to the Climate and Forecasting (CF) netCDF conventions. 

[Leaflet.jl](https://juliageo.org/Leaflet.jl/stable/)

LeafletJS maps for Julia.

[GeoFormatTypes.jl](https://juliageo.org/GeoFormatTypes.jl/stable/)

GeoFormatTypes defines wrapper types to make it easy to pass and dispatch on geographic formats like Well Known Text or GeoJSON between packages. 

[LibSpatialIndex.jl](https://github.com/JuliaGeo/LibSpatialIndex.jl)

LibSpatialIndex.jl is a julia wrapper around the C API of libspatialindex, for spatially indexing kD bounding box data.

[NetCDF.jl](https://juliageo.org/NetCDF.jl/dev/)

Reading and writing NetCDF files in Julia.

[ArchGDAL.jl](https://github.com/yeesian/ArchGDAL.jl)

GDAL is a translator library for raster and vector geospatial data formats that is released under an X/MIT license by the Open Source Geospatial Foundation. As a library, it presents an abstract data model to drivers for various raster and vector formats.

[OpenStreetMap.jl](https://github.com/tedsteiner/OpenStreetMap.jl)

This package provides basic functionality for parsing, viewing, and working with OpenStreetMap map data. The package is intended mainly for researchers who want to incorporate this rich, global data into their work, and has been designed with both speed and simplicity in mind, especially for those who might be new to Julia.

[Turf.jl](https://github.com/philoez98/Turf.jl)

A spatial analysis library written in Julia, ported from the great Turf.js.

[GeoDataFrames.jl](https://github.com/evetion/GeoDataFrames.jl)

Simple geographical vector interaction built on top of ArchGDAL. Inspiration from geopandas.

[Circuitscape.jl](https://github.com/Circuitscape/Circuitscape.jl)

Circuitscape is an open-source program that uses circuit theory to model connectivity in heterogeneous landscapes. Its most common applications include modeling movement and gene flow of plants and animals, as well as identifying areas important for connectivity conservation.

[GoogleMaps.jl](https://github.com/yeesian/GoogleMaps.jl)

This library brings the Google Maps API Web Services to Julia, and supports the following Google Maps APIs: Directions API, Geocoding API, Places API, Roads API.

[KeplerGLBase.jl](https://github.com/jmboehm/KeplerGLBase.jl)

Julia package to create kepler.gl-compatible maps, to be rendered using one of the following Julia frontends: KeplerGL.jl and StippleKeplerGL.jl.

[KeplerGL.jl](https://github.com/jmboehm/KeplerGL.jl)

Julia package to create, render, and export geospatial maps, using Kepler.gl, via Blink.jl.

[StippleKeplerGL.jl](https://github.com/GenieFramework/StippleKeplerGL.jl)

Julia package to integrate KeplerGL maps into Genie/Stipple applications.

---

### Quantum Computing

[QuantumControl.jl](https://github.com/JuliaQuantumControl/QuantumControl.jl)

A Julia Framework for Quantum Dynamics and Control.

[QuantumControlBase.jl](https://github.com/JuliaQuantumControl/QuantumControlBase.jl)

Common types and methods for the JuliaQuantumControl organization.

[QuantumPropagators.jl](https://github.com/JuliaQuantumControl/QuantumPropagators.jl)

Methods for simulating the time dynamics of a quantum system for packages with the JuliaQuantumControl organization.

[QuantumGradientGenerators.jl](https://github.com/JuliaQuantumControl/QuantumGradientGenerators.jl)

Dynamic Gradients for Quantum Control.

[Krotov.jl](https://github.com/JuliaQuantumControl/Krotov.jl)

Implementation of Krotov's method of optimal control enhanced with automatic differentiation.

[GRAPE.jl](https://github.com/JuliaQuantumControl/GRAPE.jl)

Implementation of (second-order) GRadient Ascent Pulse Engineering (GRAPE) extended with automatic differentiation.

[GRAPELinesearchAnalysis.jl](https://github.com/JuliaQuantumControl/GRAPELinesearchAnalysis.jl)

A package to analyze and debug LBFGS linesearch behavior inside GRAPE.jl.

[QuOptimalControl.jl](https://github.com/JuliaQuantumControl/QuOptimalControl.jl)

Library for solving quantum optimal control problems in Julia. Currently offers support for GRAPE and dCRAB algorithms using piecewise constant controls. 

[TwoQubitWeylChamber.jl](https://github.com/JuliaQuantumControl/TwoQubitWeylChamber.jl)

Julia package for analyzing two-qubit gates in the Weyl chamber.

[QuantumOpticsControlExtensions.jl](https://github.com/JuliaQuantumControl/QuantumOpticsControlExtensions.jl)

This package extends methods from QuantumPropagator.jl and QuantumControl.jl for types defined in QuantumOptics.jl.

[Braket.jl](https://github.com/awslabs/Braket.jl)

This package is a Julia implementation of the Amazon Braket SDK allowing customers to access Quantum Hardware and Simulators.

[Quantica.jl](https://github.com/pablosanjose/Quantica.jl)

The Quantica.jl package provides an expressive API to build arbitrary quantum systems on a discrete lattice, and a collection of algorithms to compute some of their properties.

---

### Dates and Time

[TimeZones.jl](https://github.com/JuliaTime/TimeZones.jl)

IANA time zone database access for the Julia programming language. TimeZones.jl extends the Date/DateTime support for Julia to include a new time zone aware TimeType: ZonedDateTime.

[TZJData.jl](https://github.com/JuliaTime/TZJData.jl)

TZJData provides releases of the IANA tzdata compiled into the tzjfile (time zone julia file) format. The compiled data stored via package artifacts and used by the TimeZones.jl as an the source of pre-computed time zones.

[NanoDates.jl](https://github.com/JuliaTime/NanoDates.jl)

Dates with nanosecond resolved days.

[LeapSeconds.jl](https://github.com/JuliaTime/LeapSeconds.jl)

A new minor version of this package will be published everytime a new leap second is issued be the IERS and dependent packages will need to be updated!

[LaxZonedDateTimes.jl](https://github.com/JuliaTime/LaxZonedDateTimes.jl)

Provides `LaxZonedDateTime`, an alternative to TimeZones.jl's `ZonedDateTime` that does not raise exceptions when a time that is ambiguous or doesn't exist is encountered.

[Holidays.jl](https://github.com/invenia/Holidays.jl)

Julia library for handling holidays.

[UTCDateTimes.jl](https://github.com/invenia/UTCDateTimes.jl)

UTCDateTime is a very simple time zone aware datetime representation that is always in the UTC time zone.

[UniversalTimeLocalTime.jl](https://github.com/JeffreySarnoff/UniversalTimeLocalTime.jl)

Universal Time and Local Time with a great deal of care in how libc is used.

[CompoundPeriods.jl](https://github.com/JeffreySarnoff/CompoundPeriods.jl)

Enhances Dates.CompoundPeriod.

[TimeDates.jl](https://github.com/JeffreySarnoff/TimeDates.jl)

Provides TimeDate, a nanosecond resolved DateTime type.

[DatesWithNanoseconds.jl](https://github.com/JeffreySarnoff/DatesWithNanoseconds.jl)

`TimeDate` is nanosecond resolved, and `DateTime` compatible.

[DateNanoTimes.jl](https://github.com/JeffreySarnoff/DateNanoTimes.jl)

DateNano is a nanosecond resolved DateTime type.

[DatesPlus.jl](https://github.com/JeffreySarnoff/DatesPlus.jl)

Experimental Dates stdlib to support TimesDates.jl v2.

---

### GPU

[CUDA.jl](https://cuda.juliagpu.org/stable/)

The CUDA.jl package is the main entrypoint for programming NVIDIA GPUs in Julia. The package makes it possible to do so at various abstraction levels, from easy-to-use arrays down to hand-written kernels using low-level CUDA APIs.

[AMDGPU.jl](https://amdgpu.juliagpu.org/stable/)

AMD GPU (ROCm) programming in Julia.

---

### Misc packages

[CRC.jl](https://github.com/andrewcooke/CRC.jl)

This is a Julia module for calculating Cyclic Redundancy Checksums (CRCs).

[CRC32.jl](https://github.com/JuliaIO/CRC32.jl)

CRC32 is a Julia package for computing the CRC-32 checksum as defined by the ISO 3309 / ITU-T V.42 / CRC-32-IEEE standards, designed as a drop-in replacement for Julia's CRC32c standard library (which computes the CRC-32c checksum).

[MD5.jl](https://github.com/JuliaCrypto/MD5.jl)

A pure julia MD5 implementation. There is few reasons to create new MD5 checksums, but there are a huge number of existing ones. Honestly, just use SHA-256 for everything you would use MD5 for. MD5 is not secure, and it's not faster, and it doesn't have much going for it.

[SHA.jl](https://github.com/JuliaCrypto/SHA.jl)

A performant, 100% native-julia SHA1, SHA2, and SHA3 implementation.

[Krypto.jl](https://github.com/JuliaCrypto/Krypto.jl)

A futuristic crypto library. In Julia.

[ToyFHE.jl](https://github.com/JuliaCrypto/ToyFHE.jl)

Toy implementation of FHE algorithms.

[OpenSSH.jl](https://github.com/JuliaCrypto/OpenSSH.jl)

Generates the SSH keys that are required for the automatic deployment of documentation with Documenter from a builder to GitHub Pages.

[Ripemd.jl](https://github.com/JuliaCrypto/Ripemd.jl)

Pure Julia implementation of the Ripemd hashing algorithm. Currently only Ripemd160 is implemented and convenience functions are missing.

[SEAL.jl](https://github.com/JuliaCrypto/SEAL.jl)

SEAL.jl is a Julia package that wraps the Microsoft SEAL library for homomorphic encryption. It supports the Brakerski/Fan-Vercauteren (BFV) and Cheon-Kim-Kim-Song (CKKS, also known as HEAAN in literature) schemes and exposes the homomorphic encryption capabilitites of SEAL in a (mostly) intuitive and Julian way. SEAL.jl is published under the same permissive MIT license as the Microsoft SEAL library.

[Nettle.jl](https://github.com/JuliaCrypto/Nettle.jl)

Julia wrapper around nettle cryptographic hashing/encryption library providing MD5, SHA1, SHA2 hashing and HMAC functionality, as well as AES encryption/decryption.

[YearMonths.jl](https://github.com/felipenoris/YearMonths.jl)

Provides `YearMonth` type for the Julia language.

[PiSMC.jl](https://github.com/felipenoris/PiSMC.jl)

Let's calculate Pi using a very inefficient Monte-Carlo simulation.

[SeisNoise.jl](https://tclements.github.io/SeisNoise.jl/latest/)

SeisNoise.jl provides routines for quickly and efficiently implementing seismic interferometry.

[Hexagons.jl](https://github.com/GiovineItalia/Hexagons.jl)

This package provides some basic utilities for working with hexagonal grids.

[Processing.jl](https://github.com/JuliaPackageMirrors/Processing.jl)

A port of the Processing language (https://www.processing.org) to Julia.

[GeoStats.jl](https://juliaearth.github.io/GeoStats.jl/v0.14/)

An extensible framework for high-performance geostatistics in Julia.

[Term.jl](https://fedeclaudi.github.io/Term.jl/dev/)

Term.jl is a Julia library for producing styled, beautiful terminal output.

[TermWin.jl](https://github.com/tonyhffong/TermWin.jl)

TermWin.jl is a tool to help navigate tree-like data structure such as Expr, Dict, Array, Module, and DataFrame It uses a ncurses-based user interface. It also contains a backend framework for composing ncurses user interfaces.

[TextUserInterfaces.jl](https://github.com/ronisbr/TextUserInterfaces.jl/)

This package wraps the C library ncurses and provide a Julia-like API to build text user interfaces. The development was highly based on the package TermWin.jl.

[TerminalUserInterfaces.jl](https://github.com/kdheepak/TerminalUserInterfaces.jl)

Create TerminalUserInterfaces in Julia.

[AnsiColor.jl](https://github.com/Aerlinger/AnsiColor.jl)

Full support for ANSI colored strings in Julia. Allows formatted output in REPL/Shell environment for both Unix and Mac.

[IOIndents.jl](https://github.com/KristofferC/IOIndents.jl)

IOIndents facilitates writing indented and aligned text to buffers (like files or the terminal).

[Crayons.jl](https://github.com/KristofferC/Crayons.jl)

Crayons is a package that makes it simple to write strings in different colors and styles to terminals. It supports the 16 system colors, both the 256 color and 24 bit true color extensions, and the different text styles available to terminals. The package is designed to perform well, have no dependencies and load fast (about 10 ms load time after precompilation).

[SyntheticGrids.jl](https://github.com/invenia/SyntheticGrids.jl)

This module provides an open source suite for generating synthetic grids based on real data openly available to the public.

[PowerSystemsUnits.jl](https://github.com/invenia/PowerSystemsUnits.jl)

A supplemental power systems units package for Unitful 0.15.0 or later.

[OPFSampler.jl](https://github.com/invenia/OPFSampler.jl)

The goal of the package is to provide functions that take a power grid as input, vary its parameters and generate feasible DC- and AC-OPF samples along with the corresponding solutions. This helps the user to explore a variety of distinct active sets of constraints of synthetic cases and mimic the time-varying behaviour of the OPF input parameters.

[Weave.jl](https://weavejl.mpastell.com/stable/)

Weave is a scientific report generator/literate programming tool for Julia. It resembles Pweave, knitr, R Markdown, and Sweave.

[JuliaBerry.jl](https://github.com/JuliaBerry/JuliaBerry.jl)

An omnibus package with a high level API for controlling peripherals on the Raspberry Pi computer. Currently has support for the GPIO pins on the Pi, and the ExplorerHat.

[PiGPIO.jl](https://github.com/JuliaBerry/PiGPIO.jl)

Control GPIO pins on the Raspberry Pi from Julia.

[SenseHat.jl](https://github.com/JuliaBerry/SenseHat.jl)

SenseHat.jl is a Julia library for interacting with the Raspberry Pi Sense HAT.

[DataStructuresAlgorithms.jl](https://github.com/mgcth/DataStructuresAlgorithms.jl)

Implementation of some data structures and algorithms from the Chalmers courses DAT038 and TIN093 in Julia as en exercise and for fun.

[DataStructures.jl](https://github.com/hesseltuinhof/DataStructures.jl)

This repository only serves an educational purpose. It implements a large number of data structures and algorithms from the similarly named book by Clifford A. Schaffer.

[FilesystemDatastructures.jl](https://github.com/staticfloat/FilesystemDatastructures.jl)

This package collects useful filesystem datastructures. Currently, it implements two file caches: SizeConstrainedFileCache and NFileCache.

[FastPriorityQueues.jl](https://gdalle.github.io/FastPriorityQueues.jl/dev/)

Possibly faster alternatives to the priority queue from DataStructures.jl. See the documentation for details.

[TrackRoots.jl](https://github.com/yakir12/TrackRoots.jl)

This is a Julia script for analysing light intensities as a function of time and space in time-lapse image stacks of seedling roots.

[ConstLab.jl](https://github.com/KristofferC/ConstLab.jl)

ConstLab.jl is a small package for Julia to test and experiment with constitutive models. It's main functionality is to call a user given material routine over a time range, aggregate the material response and return it back for analysis. The load applied to the material can be user generated or one of the predefined load cases can be used. To facilitate visualizing the results, some auxiliary plot functions are provided.

[Inflector.jl](https://github.com/GenieFramework/Inflector.jl)

Utility module for working with grammatical rules (singular, plural, underscores, etc).

[MotifSequenceGenerator.jl](https://github.com/JuliaMusic/MotifSequenceGenerator.jl)

A Julia module to generate random sequences of motifs, under the constrain that the sequence has some given total length.

[ThinFilmsTools.jl](https://github.com/lnacquaroli/ThinFilmsTools.jl)

ThinFilmsTools.jl provides tools for the design and characterisation of thin films written in Julia.

[CameraUnits.jl](https://github.com/timholy/CameraUnits.jl)

Registry of gain conversions for scientific cameras.

[Units.jl](https://github.com/timholy/Units.jl)

Infrastructure for handling physical units for the Julia programming language.Registry of gain conversions for scientific cameras.

[VT100.jl](https://github.com/Keno/VT100.jl)

VT100.jl attempts to implement a small and hackable terminal emulator, mostly intended for automatic verification of Terminal based UIs. The current implementation is very simple and ignores most of the more complicated ascepts of terminal emulation, including colors, attributes and Unicode combining characters, but is nevertheless useful for UI validation in regression tests. Support for those features will be added as the need arises.

[OCReract.jl](https://github.com/leferrad/OCReract.jl)

A simple Julia wrapper for Tesseract OCR.

[BioAlignments.jl](https://github.com/BioJulia/BioAlignments.jl)

BioAlignments.jl provides sequence alignment algorithms and data structures.

[MolecularGraph.jl](https://github.com/mojaie/MolecularGraph.jl)

MolecularGraph.jl is a graph-based molecule modeling and chemoinformatics analysis toolkit fully implemented in Julia.

[MzPlots.jl](https://github.com/timholy/MzPlots.jl)

This package supports limited plotting of mass spectrometry data. It is designed to be format-agnostic through its use of the MzCore interface, although currently only MzXML has been implemented.

[MzCore.jl](https://github.com/timholy/MzCore.jl)

MzCore defines traits and utilities for mass spectrometry analyses in Julia. The primary purpose is to allow algorithms to be written in a manner that is independent of the representation used for specific file formats.

[BlossomV.jl](https://github.com/mlewe/BlossomV.jl)

This package provides a julia wrapper to the Blossom V software package which provides an implementation of a minimum cost perfect matching algorithm.

[Humanize.jl](https://github.com/IainNZ/Humanize.jl)

Humanize numbers: data sizes, datetime deltas, digit separators.

[MonkeyString.jl](https://github.com/malmaud/MonkeyString.jl)

Fast string implementation inspired by SpiderMonkey.

[Clockwork.jl](https://github.com/malmaud/Clockwork.jl)

Represent modular arithmetic via clock symbols.

[Jin.jl](https://github.com/malmaud/Jin.jl)

A Julia version of the Python Gin configuration library.

[SFrames.jl](https://github.com/malmaud/SFrames.jl)

Wrapper around the open-source components of Graphlab.

[GraphQL.jl](https://github.com/malmaud/GraphQL.jl)

GraphQL server for Julia.

[WaterLily.jl](https://github.com/weymouth/WaterLily.jl)

WaterLily.jl is a simple and fast fluid simulator written in pure Julia. This is an experimental project to take advantage of the active scientific community in Julia to accelerate and enhance fluid simulations.

[Miner.jl](https://github.com/ashwani-rathee/Miner.jl)

Voxel Simulator written using Makie's GLMakie backend in Julia.

[Raylib.jl](https://github.com/chengchingwen/Raylib.jl)

This project is a wrapping of Raylib, which is a simple and easy-to-use library to enjoy videogames programming.

[Books.jl](https://github.com/JuliaBooks/Books.jl)

In a nutshell, this package is meant to generate books (or reports or dashboards) with embedded Julia output. Via Pandoc, the package can live serve a website and build various outputs including a website and PDF. For many standard output types, such as DataFrames and plots, the package can run your code and will automatically handle proper embedding in the output documents, and also try to guess suitable captions and labels. Also, it is possible to work via the live server, which shows changes within seconds.

[Brainfuck.jl](https://github.com/johnmyleswhite/Brainfuck.jl)

A Brainfuck interpreter written in Julia. Because `<-->`.

[Ueauty.jl](https://gitlab.com/ExpandingMan/Ueauty.jl)

Common unicode aliases for Julia.

[WatchJuliaBurn.jl](https://github.com/JuliaWTF/WatchJuliaBurn.jl)

WatchJuliaBurn aims at destroying the look of your code by adding emojis like 😄 and kaomojis like c╯°□°ↄ╯ instead of your favorite Julia functions. For a serious use of unicode characters see also Ueauty.jl

[Alert.jl](https://github.com/haberdashPI/Alert.jl)

Alert provides a cross-platform means of displaying a notification to the user in Julia. It should work on MacOS, Windows 10 (even under WSL2) and many flavors of Linux.

[AlertPushover.jl](https://github.com/haberdashPI/AlertPushover.jl)

AlertPushover provides a backend for Alert. It uses the Pushover service to send notifications remotely to the Pushover app. This makes alert useful even when working remotely or via an online IDE.

[SphingolipidsID.jl](https://github.com/yufongpeng/SphingolipidsID.jl)

Automatic identify sphingolipids through PreIS and MRM.

[PeptideSeq.jl](https://github.com/yufongpeng/PeptideSeq.jl)

Predict and match digested peptides sequences, their mass m/z and MS/MS spectra with chemical derivatization or post-translational modification.

[BloodVolumePCIIS.jl](https://github.com/yufongpeng/BloodVolumePCIIS.jl)

This is a simple package for processing post-column infusion internal standard(PCIIS) data on dried blood spot(DBS) sample for volume detection.

[TreesHeaps.jl](https://github.com/yufongpeng/TreesHeaps.jl)

Implementation of various tree and heaps structures.

[Toolkits.jl](https://github.com/yufongpeng/Toolkits.jl)

`@pip` and `@pipas` enable R's `%>%` and `%<>%` syntax. Arguments are seperated by space. Broadcasting and anonymous functions are supported; however, `@pipas` can only assign global variable.

[NumericIO.jl](https://github.com/ma-laforge/NumericIO.jl)

Fine control over numeric output: Scientific/Engineering/SI-notation.

[VectorizedRoutines.jl](https://github.com/ChrisRackauckas/VectorizedRoutines.jl)

VectorizedRoutines.jl provides a library of familiar and useful vectorized routines. This package hopes to include high-performance, tested, and documented Julia implementations of routines which MATLAB/Python/R users would be familiar with. We also welcome generally useful routines for operating on vectors/arrays.

[NDTools.jl](https://github.com/bionanoimaging/NDTools.jl)

A lightweight package delivering utility functions for working with multi-dimensional data. It provides efficient versions to deal with sizes, midpoints etc. of multidimensional data.

[SpacetimeFields.jl](https://github.com/Mattriks/SpacetimeFields.jl)

A Julia package for working with spacetime field (stfield) objects. An stfield object is a data field with data, longitude, latitude, time.

[CoupledFields.jl](https://github.com/Mattriks/CoupledFields.jl)

A Julia package for working with coupled fields. This is work in progress. The main function `gradvecfield` calculates the gradient vector or gradient matrix for each instance of the coupled fields.

[ShOpt.jl](https://github.com/EdwardBerman/shopt)

Shear Optimization with ShOpt.jl, a julia library for empirical point spread function characterizations.

[Lints.jl](https://github.com/FermiQC/Lints.jl)

Lints.jl provides an interface to the Libint2 molecular integral generation library.

[Symmetry.jl](https://github.com/FermiQC/Symmetry.jl)

Handling of molecular symmetry, including character table generation and SALCs.

[Molecules.jl](https://github.com/FermiQC/Molecules.jl)

This package offers tools to create and manipulate atomic ensamble for molecular simulations.

[GaussianBasis.jl](https://github.com/FermiQC/GaussianBasis.jl)

GaussianBasis offers high-level utilities for molecular integral computations.

[TBLIS.jl](https://github.com/FermiQC/TBLIS.jl)

Julia wrapper for TBLIS tensor contraction library.

[BliContractor.jl](https://github.com/xrq-phys/BliContractor.jl)

TensorOperations.jl compatible fast contractor for Julia, based on TBLIS, with generic strides and automatic differentiation support.

[Fermi.jl](https://github.com/FermiQC/Fermi.jl)

Fermi.jl is a quantum chemistry framework written in pure Julia. This code is developed at the Center for Computational Quantum Chemistry at the University of Georgia under the supervision of Dr. Justin M. Turney and Prof. Henry F. Schaefer.

[HeroIcons.jl](https://github.com/joshday/HeroIcons.jl)

This package provides access to the https://heroicons.com collection of SVG icons from Julia.

[XKCD.jl](https://github.com/joshday/XKCD.jl)

A Julia package for retrieving data from the XKCD webcomic: xkcd.com.

[SearchSortedNearest.jl](https://github.com/joshday/SearchSortedNearest.jl)

Find the index of (sorted) collection that has the smallest distance to x.

[FUSE.jl](https://github.com/ProjectTorreyPines/FUSE.jl)

FUSE (FUsion Synthesis Engine) is an open-source framework for the integrated design of Fusion Power Plants (FPP). Originally developed by General Atomics, FUSE is now publicly available under the Apache 2.0 license.

[FuseUtils.jl](https://github.com/ProjectTorreyPines/FuseUtils.jl)

The purpose of this package is to hold some low-level utilities (independent of IMASdd.jl and ideally with minimal external dependencies) that are needed across different other packages in the FUSE.jl ecosystem.

[DMRJtensor.jl](https://github.com/bakerte/DMRJtensor.jl)

A general purpose tensor network library in the Julia programming language.

[Glyphy.jl](https://github.com/cormullion/Glyphy.jl)

Glyphy is a small utility package that searches through a list of Unicode glyph names and returns a list of glyphs that match the search term, and any REPL shortcuts, if available.

[TrixiParticles.jl](https://github.com/trixi-framework/TrixiParticles.jl)

TrixiParticles.jl is a high-performance numerical simulation framework for particle-based methods, focused on the simulation of complex multiphysics problems, and written in Julia.

[TerminalSystemMonitor.jl](https://github.com/AtelierArith/TerminalSystemMonitor.jl)

This Julia package displays CPU and RAM usage information on your computer. If necessary, one can show GPU usage.

[MacOSIOReport.jl](https://github.com/AtelierArith/MacOSIOReport.jl)

This Julia package provides functionality to monitor CPU and GPU usage on M-series macOS systems(i.e., Apple Silicon).

[TumorGrowth.jl](https://github.com/ablaom/TumorGrowth.jl)

Predictive models for tumor growth, and tools to apply them to clinical data.

---

## Books

### Julia programming

Bakshi T. Tanmay Teaches Julia for Beginners: A Springboard to Machine Learning for All Ages. ISBN: [9781260456639](https://www.mhebooklibrary.com/doi/book/10.1036/9781260456646)

Balbaert I. Getting Started with Julia. ISBN: [9781783284795](https://www.oreilly.com/library/view/getting-started-with/9781783284795/)

Balbaert I. Julia 1.0 Programming - Second Edition. ISBN: [9781788999090](https://www.oreilly.com/library/view/julia-10-programming/9781788999090/)

Balbaert I, Salceanu A. Julia 1.0 programming complete reference guide: discover Julia, a high-performance language for technical computing. ISBN: [9781838822248](https://www.oreilly.com/library/view/julia-10-programming/9781838822248/)

Dash S. Hands-on Julia Programming. ISBN: [9789391030919](https://bpbonline.com/products/copy-of-learn-ai-with-python?_pos=1&_sid=4d6d1984e&_ss=r&variant=41145722765512)

Engheim E. Julia as a Second Language. ISBN: [1617299715](https://www.oreilly.com/library/view/julia-as-a/9781617299711/)

Engheim E. [Julia for Beginners](https://leanpub.com/julia-for-beginners)

Joshi A, Lakhanpal R. Learning Julia. ISBN: [9781785883279](https://www.oreilly.com/library/view/learning-julia/9781785883279/)

Kalicharan N. Julia - Bit by Bit: Programming for Beginners (Undergraduate Topics in Computer Science) ISBN: [303073935X](https://link.springer.com/book/10.1007/978-3-030-73936-2)

Kamiński B, Szufel P. Julia 1.0 Programming Cookbook. ISBN: [9781788998369](https://www.packtpub.com/product/julia-10-programming-cookbook/9781788998369)

Kerns G. Introduction to Julia.

Kwon C. Julia Programming for Operations Research.

Kwong T. Hands-on design patterns and best practices with Julia: proven solutions to common problems in software design for Julia 1.x. ISBN: 9781838648817

Lauwens B, Downey A. [Think Julia: How to Think Like a Computer Scientist](https://benlauwens.github.io/ThinkJulia.jl/latest/book.html). ISBN: 9781484251898

Lobianco A. Julia Quick Syntax Reference: A Pocket Guide for Data Science Programming. ISBN: 9781484251904

Nagar S. Beginning Julia Programming. ISBN: 9781484231708

Rohit JR. Julia Cookbook: over 40 recipes to get you up and running with programming using Julia. ISBN: 9781785882012

Salceanu A. Julia Programming Projects. ISBN: 9781788292740

Sengupta A, Sherrington M, Balbaert I. Julia: High Performance Programming 2E. ISBN: 9781788298117

Sengupta A, Sherrington M, Balbaert I. Julia: High Performance Programming. ISBN: 9781787125704

Sengupta A. Julia High Performance. ISBN: 9781785880919

Sengupta A. Julia high performance: design and develop high performing programs with Julia. ISBN: 9781785880919

Sengupta A. The Little Book of Julia Algorithms: A workbook to develop fluency in Julia programming. ISBN: 9781838173609

Sherrington M. Mastering Julia: develop your analytical and programming skills further in Julia to solve complex data processing problems. ISBN: 9781783553310

Zea DJ. Interactive Visualization and Plotting with Julia: Create impressive data visualizations through Julia packages. ISBN: 1801810516

Bezanzon J, Chen J, Chung B, Karpinski S, Shah V, Zoubritzky L, Vitek J. Julia: Dynamism and Performance Reconciled by Design. [PDF](http://janvitek.org/pubs/oopsla18b.pdf)

Belyakova J, Chung B, Gelinas J, Nash J, Tate R, Vitek J. World Age in Julia. [PDF](http://janvitek.org/pubs/oopsla20-j.pdf)

Chung B, Nardelli F, Vitek J. Julia's efficient algorithm for subtyping unions and covariant tuples. [PDF](http://janvitek.org/pubs/ecoop19.pdf)

Pelenitsyn A, Belyakova J, Chung B, Tate R, Vitek J. Type Stability in Julia. [PDF](http://janvitek.org/pubs/oopsla21c.pdf)

---

### Mathematics

Kochenderfer M, and Wheeler T. Algorithms for optimization. ISBN: 9780262039420 [PDF](https://algorithmsbook.com/optimization/files/optimization.pdf)

Boyd S, Vandenberghe L. Convex Optimization. ISBN: 0521833787 [PDF](https://web.stanford.edu/~boyd/cvxbook/bv_cvxbook.pdf)

Kwon C. Julia Programming for Operations Research. ISBN: 1798205475 [HTML](https://juliabook.chkwon.net/book)

Lusby R, Stidsen T. Mathematical Programming with Julia. ISBN: 9788793458253 [PDF](https://www.man.dtu.dk/-/media/centre/management-science/mathprogrammingwithjulia/mathematical-programming-with-julia.pdf)

Schoen F. Optimization Models. [HTML](https://webgol.dinfo.unifi.it/OptimizationModels/contents.html)

---

### Data science

Numerical methods for Scientifc Computing. ISBN: 9798985421804

Adams C. Learning Microeconometrics with R. ISBN: 9780367255381

Boyd S, Vandenberghe S. Introduction to Applied Linear Algebra – Vectors, Matrices, and Least Squares. ISBN: 9781316518960 

Chan S. Introduction to Probability for Data Science. ISBN: 978-1-60785-747-1

Joshi A. Julia for data science: explore the world of data science from scratch with Julia by your side. ISBN: 9781785289699

Kamiński B, Prałat P. Train Your Brain - Challenging Yet Elementary Mathematics. ISBN: 9780367564872

Kamiński B. Julia for Data Analysis. ISBN: 9781633439368

Kochenderfer M, Wheeler T, Wray K. Algorithms for Decision Making. ISBN: 9780262047012

Kochenderfer M, Wheeler T. Algorithms for Optimization. ISBN: 9780262039420

McNicholas P, Tait P. Data science with Julia. ISBN: [9781351013673](https://www.taylorfrancis.com/books/mono/10.1201/9781351013673/data-science-julia-peter-tait-paul-mcnicholas)

Orban D, Arioli M. Iterative Solution of Symmetric Quasi-Definite Linear Systems. DOI: [10.1137/1.9781611974737](https://epubs.siam.org/doi/book/10.1137/1.9781611974737)

Storopoli J, Huijzer R, Alonso L. Julia data science. ISBN: [9798489859165](https://juliadatascience.io/juliadatascience.pdf)

Voulgaris Z. [Julia for Data Science](https://technicspub.com/julia-for-data-science).

---

### Statistics

Nazarathy Y, Klok H. Statistics with Julia: Fundamentals for Data Science, Machine Learning and Artificial Intelligence (Springer Series in the Data Sciences). ISBN: [3030709000](https://link.springer.com/book/10.1007/978-3-030-70901-3)

Storopoli J. [Bayesian Statistics with Julia and Turing](https://storopoli.github.io/Bayesian-Julia)

Lukaszuk B. [Romeo and Julia, where Romeo is Basic Statistics](https://b-lukaszuk.github.io/RJ_BS_eng/)

---

### Artificial intelligence

Voulgaris Z. Julia for Machine Learning. ISBN: [9781634628136](https://shop.harvard.com/book/9781634628136)

---

### Signal analysis

---

### Image processing

Cudihins D. Hands-On Computer Vision with Julia. ISBN: [9781788998796](https://www.oreilly.com/library/view/hands-on-computer-vision/9781788998796/)

---

## Misc

[Quantitative Economics with Julia](https://julia.quantecon.org/intro.html)

This website presents a set of lectures on quantitative economic modeling, designed and written by Jesse Perla, Thomas J. Sargent and John Stachurski. The language instruction is Julia.

[JuliaNotes.jl](https://m3g.github.io/JuliaNotes.jl/stable/)

A collection of explanations and tips to make the best use of Julia. Many answers are extracts from solutions provided to user questions in the Julia Discourse. 

Novak K. [Numerical Methods for Scientific Computing](https://www.equalsharepress.com/media/NMFSC.pdf) ISBN: 9798985421804

The book covers the mathematical theory and practical considerations of the essential numerical methods used in scientific computing. Julia is used throughout, with Python and Matlab/Octave included in the back matter. Jupyter notebooks of the code are [available on GitHub](https://nbviewer.org/github/nmfsc/julia/blob/main/julia.ipynb).

[Using Julia on the HPC](https://blogs.nottingham.ac.uk/digitalresearch/2022/09/30/using-julia-on-the-hpc/)

Perkel J. Julia: come for the syntax, stay for the speed. Nature 572, 2019: 141-142. DOI: 10.1038/d41586-019-02310-3

[10 things I love about Julia](https://trang.page/2021/12/28/10-things-i-love-about-julia/)

[Geospatial Data Science with Julia](https://juliaearth.github.io/geospatial-data-science-with-julia/)

Geospatial Data Science with Julia presents a fresh approach to data science with geospatial data and the Julia programming language. It contains best practices for writing clean, readable and performant code in geoscientific applications involving sophisticated representations of the (sub)surface of the Earth such as unstructured meshes made of 2D and 3D geometries.

[The Julia Presentation](https://github.com/jbytecode/julia-presentation) -- presented in 2023.12.01 at Istanbul University, Faculty of Economics

[Julia MixedModels.jl workshop @ MindCORE](https://colab.research.google.com/drive/1eT-cb3_TAczLvs29_XpRH49oaySM4zW0?usp=sharing#scrollTo=puvAqBO28TjS)

[julia-wasm](https://github.com/Keno/julia-wasm)

Running Julia on WASM.

---

## Tutorials

[Julia language: a concise tutorial](https://syl1.gitbook.io/julia-language-a-concise-tutorial/)

[Exploratory PCA in Julia](https://stackoverflow.com/questions/68053860/exploratory-pca-in-julia)

[Julia macros for beginners](https://jkrumbiegel.com/pages/2021-06-07-macros-for-beginners/)

[A really brief introduction to audio signal processing in Julia](https://www.seaandsailor.com/audiosp_julia.html)

[Julia: delete rows and columns from an array or matrix](https://stackoverflow.com/questions/58033504/julia-delete-rows-and-columns-from-an-array-or-matix)

Nybo Nissen J. [What scientists must know about hardware to write fast code](https://biojulia.net/post/hardware/)

The aim of this tutorial is to give non-professional programmers a brief overview of the features of modern hardware that you must understand in order to write fast code.

[JuliaBerry](https://juliaberry.github.io/)

JuliaBerry is an organisation that brings together various resources for using the Julia language for the Raspberry Pi.

[Beautiful Makie](https://beautiful.makie.org/dev/)

A gallery of easy to copy-paste examples. 

[Julia Tutorials Template](https://github.com/rikhuijzer/JuliaTutorialsTemplate)

A template for building websites with Julia. Easily publish Pluto notebooks online.

[Julia by Example](https://juliabyexample.helpmanual.io/)

[The ultimate guide to distributed computing ](https://discourse.julialang.org/t/the-ultimate-guide-to-distributed-computing/41867)

[Denoising diffusion probabilistic models from first principles](https://liorsinai.github.io/coding/2022/12/03/denoising-diffusion-1-spiral.html)

[Guided denoising diffusion](https://liorsinai.github.io/coding/2023/01/04/denoising-diffusion-3-guidance)

[Image generation with MNIST](https://liorsinai.github.io/coding/2022/12/29/denoising-diffusion-2-unet)

[A quick introduction to data parallelism in Julia](https://juliafolds.github.io/data-parallelism/tutorials/quick-introduction/)

[Distributed computing with Julia](https://cecileane.github.io/computingtools/pages/notes1209.html)

[The ultimate guide to distributed computing in Julia](https://github.com/Arpeggeo/julia-distributed-computing)

[A brief tutorial on training a Neural Network with Flux.jl](https://medium.com/coffee-in-a-klein-bottle/deep-learning-with-julia-e7f15ad5080b)

[Introduction in deep learning with Julia](https://www.geeksforgeeks.org/indroduction-in-deep-learning-with-julia/)

[Administrative Scripting with Julia](https://github.com/ninjaaron/administrative-scripting-with-julia)

[Extreme Multi-Threading: C++ and Julia 1.9 Integration](https://scientificcoder.com/extreme-multi-threading-c-and-julia-19-integration)

[JuLox: What I Learned Building a Lox Interpreter in Julia](https://lukemerrick.com/posts/intro_to_julox.html)

[Building GUIs with Julia, Tk, and Cairo, Part I](https://julialang.org/blog/2013/05/graphical-user-interfaces-part1/)

[Building GUIs with Julia, Tk, and Cairo, Part II](https://julialang.org/blog/2013/05/graphical-user-interfaces-part2/)

[Hiccups Writing GTK3 GUI Code in Julia](https://towardsdev.com/hiccups-writing-gtk3-gui-code-in-julia-on-macos-2467483c2e78?gi=32cf37746e98)

[An Introduction to Laplace Approximations for Bayesian Deep Learning in Julia](https://medium.com/@sbratus/an-introduction-to-laplace-approximations-for-bayesian-deep-learning-in-julia-c5a30cfaf7b5)

[Data Science Tutorials in Julia](https://juliaai.github.io/DataScienceTutorials.jl/)

[Introduction to probabilistic programming in Julia](https://medium.com/analytics-vidhya/introduction-to-probabilistic-programming-in-julia-33960c7929f0)

[Interactive contour maps of surface temperature anomalies in 2016](https://etpinard.xyz/#gist=9338947b0f3427e10b3602172b6a59ec&lang=en)

[set up NeoVim + Tmux for a Data Science Workflow with Julia](https://www.indymnv.dev/posts/004_nvim/)

[Using evotrees.jl for time series prediction](https://www.indymnv.dev/posts/003_publish/)

[Creating your own blog with Julia and Franklin](https://www.indymnv.dev/posts/006_build_blog/)

[Julia for Economists](https://www.youtube.com/playlist?list=PLbuwVVKCI3sRW0Y5ehBFwdFVuyuy87ram)

[An Introduction to Structural Econometrics in Julia](https://juliaeconomics.com/)

[Julia for Optimization and Learning](https://juliateachingctu.github.io/Julia-for-Optimization-and-Learning/stable/)

[Modern Julia Workflows](https://modernjuliaworkflows.github.io/)

[Julia for R users](https://vituri.github.io/blog/posts/julia-for-r-users/julia-for-r-users.html)

# Other materials

[What the heck is Julia and why won't Cameron shut up about it](https://newsletter.cameron.pfiffer.org/p/juliacon-2023)

[Julia SOS](https://www.juliasos.com/)

[Why Julia - a Manifesto](https://github.com/Datseris/whyjulia-manifesto)

[Blue: a Style Guide for Julia](https://github.com/JuliaDiff/BlueStyle)

This document specifies style conventions for Julia code. These conventions were created from a variety of sources including Python's PEP8, Julia's [Notes for Contributors](https://github.com/JuliaLang/julia/blob/master/CONTRIBUTING.md), and [Julia's Style Guide](https://docs.julialang.org/en/v1/manual/style-guide/).

[Trustworthy Artificial Intelligence in Julia](https://www.taija.org/)
