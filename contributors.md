---

## Contributors

Created and maintained by [Adam Wysokiński](mailto:adam.wysokinski@umed.lodz.pl).

If you've contributed, add your name below:

Adam Wysokiński, jluis, uncomfyhalomacro, heltonmc

---
