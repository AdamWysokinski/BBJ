## Welcome to the Big Book of Julia!

The idea for Big Book of Julia (BBJ) is based on [Big Book of R](https://www.bigbookofr.com/index.html).

Its objective is to be a set of resources for [Julia](https://julialang.org/) programmers, data scientists and researchers. It contains links to tutorials, courses, blogs, videos, package documentation and books related to Julia programming.

BBJ repository is located at: [https://codeberg.org/AdamWysokinski/BBJ](https://codeberg.org/AdamWysokinski/BBJ).

If you'd like to show your appreciation with a donation you're most welcome to do so [here](https://liberapay.com/AdamWysokinski/donate).

This content is free to use and is licensed under the [CC BY-NC-ND 3.0 License](https://creativecommons.org/licenses/by-nc-nd/3.0/us/) (Creative Commons Attribution-NonCommercial-NoDerivs 3.0).

